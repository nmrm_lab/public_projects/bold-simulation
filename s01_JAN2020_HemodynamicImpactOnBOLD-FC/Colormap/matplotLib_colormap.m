%### COLORMAP Examples ###

%% Plot the scheme's RGB values: 
rgbplot(viridis(256))

%% New colors for the COLORMAP example: 
load spine 
image(X) 
colormap(viridis)

%% New colors for the SURF example: 
[X,Y,Z] = peaks(30); 
surfc(X,Y,Z) 
colormap(viridis) 
axis([-3,3,-3,3,-10,5])

%### COLORORDER Examples ###

%% PLOT using matrices: 
N = 10; 
axes('ColorOrder',vega10(N),'NextPlot','replacechildren') 
X = linspace(0,pi*3,1000); 
Y = bsxfun(@(x,n)n*sin(x+2*n*pi/N), X(:), 1:N); 
plot(X,Y, 'linewidth',4)

%% PLOT in a loop: 
N = 10; 
set(0,'DefaultAxesColorOrder',vega10(N)) 
X = linspace(0,pi*3,1000); 
Y = bsxfun(@(x,n)n*sin(x+2*n*pi/N), X(:), 1:N); 
for n = 1:N 
plot(X(:),Y(:,n), 'linewidth',4); 
hold all 
end

%% LINE using matrices: 
N = 10; 
set(0,'DefaultAxesColorOrder',vega10(N)) 
X = linspace(0,pi*3,1000); 
Y = bsxfun(@(x,n)n*cos(x+2*n*pi/N), X(:), 1:N); 
line(X(:),Y)