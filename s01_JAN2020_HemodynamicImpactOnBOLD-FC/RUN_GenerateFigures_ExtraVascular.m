%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set path where results are stored                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SourcePath = 'C:\Christine Preibisch\04_Calibrated_fMRI\BOLD_Simulations\MATLAB\GIT_BOLD-Simulation\s01_JAN2020_HemodynamicImpactOnBOLD-FC\BACKUP_Results4paper_JAN2020\';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set path where results are stored                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ResultPath = 'C:\Christine Preibisch\04_Calibrated_fMRI\BOLD_Simulations\MATLAB\GIT_BOLD-Simulation\s01_JAN2020_HemodynamicImpactOnBOLD-FC\BACKUP_Results4paper_JAN2020\Figures_200sec_4xSNRMatrix_extravascular';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TR = 2000 ms Load simulation data from disk                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load(fullfile(SourcePath,'PlotResults_200sec_TR2000ms_4xSNRMatrix_extravascular\VARm1_VARf1\TightCoupling\SimulationResults.mat'));
TR2000_TightCoupling_VARm1_VARf1.S_BOLD = S_BOLD_VARm1_VARf1;
TR2000_TightCoupling_VARm1_VARf1.CC = CC_VARm1_VARf1;
TR2000_TightCoupling_VARm1_VARf1.BOLD_Model = BOLD_Model_VARm1_VARf1;
clear S_BOLD_VARm1_VARf1 CC_VARm1_VARf1 BOLD_Model_VARm1_VARf1

load(fullfile(SourcePath,'PlotResults_200sec_TR2000ms_4xSNRMatrix_extravascular\VARdelays\TightCoupling\SimulationResults.mat'));
TR2000_TightCoupling_VARdelays.S_BOLD = S_BOLD_VARdelays;
TR2000_TightCoupling_VARdelays.CC = CC_VARdelays;
TR2000_TightCoupling_VARdelays.BOLD_Model = BOLD_Model_VARdelays;
clear S_BOLD_VARdelays CC_VARdelays BOLD_Model_VARdelays

load(fullfile(SourcePath,'PlotResults_200sec_TR2000ms_4xSNRMatrix_extravascular\VARcbv\TightCoupling\SimulationResults.mat'));
TR2000_TightCoupling_VARcbv.S_BOLD = S_BOLD_VARcbv;
TR2000_TightCoupling_VARcbv.CC = CC_VARcbv;
TR2000_TightCoupling_VARcbv.BOLD_Model = BOLD_Model_VARcbv;
clear S_BOLD_VARcbv CC_VARcbv BOLD_Model_VARcbv

load(fullfile(SourcePath,'PlotResults_200sec_TR2000ms_4xSNRMatrix_extravascular\VARm1_VARf1\SlowerCBV\SimulationResults.mat'));
TR2000_SlowerCBV_VARm1_VARf1.S_BOLD = S_BOLD_VARm1_VARf1;
TR2000_SlowerCBV_VARm1_VARf1.CC = CC_VARm1_VARf1;
TR2000_SlowerCBV_VARm1_VARf1.BOLD_Model = BOLD_Model_VARm1_VARf1;
clear S_BOLD_VARm1_VARf1 CC_VARm1_VARf1 BOLD_Model_VARm1_VARf1

load(fullfile(SourcePath,'PlotResults_200sec_TR2000ms_4xSNRMatrix_extravascular\VARdelays\SlowerCBV\SimulationResults.mat'));
TR2000_SlowerCBV_VARdelays.S_BOLD = S_BOLD_VARdelays;
TR2000_SlowerCBV_VARdelays.CC = CC_VARdelays;
TR2000_SlowerCBV_VARdelays.BOLD_Model = BOLD_Model_VARdelays;
clear S_BOLD_VARdelays CC_VARdelays BOLD_Model_VARdelays

load(fullfile(SourcePath,'PlotResults_200sec_TR2000ms_4xSNRMatrix_extravascular\VARcbv\SlowerCBV\SimulationResults.mat'));
TR2000_SlowerCBV_VARcbv.S_BOLD = S_BOLD_VARcbv;
TR2000_SlowerCBV_VARcbv.CC = CC_VARcbv;
TR2000_SlowerCBV_VARcbv.BOLD_Model = BOLD_Model_VARcbv;
clear S_BOLD_VARcbv CC_VARcbv BOLD_Model_VARcbv

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TR = 1000 ms Load simulation data from disk                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load(fullfile(SourcePath,'PlotResults_200sec_TR1000ms_4xSNRMatrix_extravascular\VARm1_VARf1\TightCoupling\SimulationResults.mat'));
TR1000_TightCoupling_VARm1_VARf1.S_BOLD = S_BOLD_VARm1_VARf1;
TR1000_TightCoupling_VARm1_VARf1.CC = CC_VARm1_VARf1;
TR1000_TightCoupling_VARm1_VARf1.BOLD_Model = BOLD_Model_VARm1_VARf1;
clear S_BOLD_VARm1_VARf1 CC_VARm1_VARf1 BOLD_Model_VARm1_VARf1

load(fullfile(SourcePath,'PlotResults_200sec_TR1000ms_4xSNRMatrix_extravascular\VARdelays\TightCoupling\SimulationResults.mat'));
TR1000_TightCoupling_VARdelays.S_BOLD = S_BOLD_VARdelays;
TR1000_TightCoupling_VARdelays.CC = CC_VARdelays;
TR1000_TightCoupling_VARdelays.BOLD_Model = BOLD_Model_VARdelays;
clear S_BOLD_VARdelays CC_VARdelays BOLD_Model_VARdelays

load(fullfile(SourcePath,'PlotResults_200sec_TR1000ms_4xSNRMatrix_extravascular\VARcbv\TightCoupling\SimulationResults.mat'));
TR1000_TightCoupling_VARcbv.S_BOLD = S_BOLD_VARcbv;
TR1000_TightCoupling_VARcbv.CC = CC_VARcbv;
TR1000_TightCoupling_VARcbv.BOLD_Model = BOLD_Model_VARcbv;
clear S_BOLD_VARcbv CC_VARcbv BOLD_Model_VARcbv

load(fullfile(SourcePath,'PlotResults_200sec_TR1000ms_4xSNRMatrix_extravascular\VARm1_VARf1\SlowerCBV\SimulationResults.mat'));
TR1000_SlowerCBV_VARm1_VARf1.S_BOLD = S_BOLD_VARm1_VARf1;
TR1000_SlowerCBV_VARm1_VARf1.CC = CC_VARm1_VARf1;
TR1000_SlowerCBV_VARm1_VARf1.BOLD_Model = BOLD_Model_VARm1_VARf1;
clear S_BOLD_VARm1_VARf1 CC_VARm1_VARf1 BOLD_Model_VARm1_VARf1

load(fullfile(SourcePath,'PlotResults_200sec_TR1000ms_4xSNRMatrix_extravascular\VARdelays\SlowerCBV\SimulationResults.mat'));
TR1000_SlowerCBV_VARdelays.S_BOLD = S_BOLD_VARdelays;
TR1000_SlowerCBV_VARdelays.CC = CC_VARdelays;
TR1000_SlowerCBV_VARdelays.BOLD_Model = BOLD_Model_VARdelays;
clear S_BOLD_VARdelays CC_VARdelays BOLD_Model_VARdelays

load(fullfile(SourcePath,'PlotResults_200sec_TR1000ms_4xSNRMatrix_extravascular\VARcbv\SlowerCBV\SimulationResults.mat'));
TR1000_SlowerCBV_VARcbv.S_BOLD = S_BOLD_VARcbv;
TR1000_SlowerCBV_VARcbv.CC = CC_VARcbv;
TR1000_SlowerCBV_VARcbv.BOLD_Model = BOLD_Model_VARcbv;
clear S_BOLD_VARcbv CC_VARcbv BOLD_Model_VARcbv
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TR = 100 ms Load simulation data from disk                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load(fullfile(SourcePath,'PlotResults_200sec_TR100ms_4xSNRMatrix_extravascular\VARm1_VARf1\TightCoupling\SimulationResults.mat'));
TR100_TightCoupling_VARm1_VARf1.S_BOLD = S_BOLD_VARm1_VARf1;
TR100_TightCoupling_VARm1_VARf1.CC = CC_VARm1_VARf1;
TR100_TightCoupling_VARm1_VARf1.BOLD_Model = BOLD_Model_VARm1_VARf1;
clear S_BOLD_VARm1_VARf1 CC_VARm1_VARf1 BOLD_Model_VARm1_VARf1

load(fullfile(SourcePath,'PlotResults_200sec_TR100ms_4xSNRMatrix_extravascular\VARdelays\TightCoupling\SimulationResults.mat'));
TR100_TightCoupling_VARdelays.S_BOLD = S_BOLD_VARdelays;
TR100_TightCoupling_VARdelays.CC = CC_VARdelays;
TR100_TightCoupling_VARdelays.BOLD_Model = BOLD_Model_VARdelays;
clear S_BOLD_VARdelays CC_VARdelays BOLD_Model_VARdelays

load(fullfile(SourcePath,'PlotResults_200sec_TR100ms_4xSNRMatrix_extravascular\VARcbv\TightCoupling\SimulationResults.mat'));
TR100_TightCoupling_VARcbv.S_BOLD = S_BOLD_VARcbv;
TR100_TightCoupling_VARcbv.CC = CC_VARcbv;
TR100_TightCoupling_VARcbv.BOLD_Model = BOLD_Model_VARcbv;
clear S_BOLD_VARcbv CC_VARcbv BOLD_Model_VARcbv

load(fullfile(SourcePath,'PlotResults_200sec_TR100ms_4xSNRMatrix_extravascular\VARm1_VARf1\SlowerCBV\SimulationResults.mat'));
TR100_SlowerCBV_VARm1_VARf1.S_BOLD = S_BOLD_VARm1_VARf1;
TR100_SlowerCBV_VARm1_VARf1.CC = CC_VARm1_VARf1;
TR100_SlowerCBV_VARm1_VARf1.BOLD_Model = BOLD_Model_VARm1_VARf1;
clear S_BOLD_VARm1_VARf1 CC_VARm1_VARf1 BOLD_Model_VARm1_VARf1

load(fullfile(SourcePath,'PlotResults_200sec_TR100ms_4xSNRMatrix_extravascular\VARdelays\SlowerCBV\SimulationResults.mat'));
TR100_SlowerCBV_VARdelays.S_BOLD = S_BOLD_VARdelays;
TR100_SlowerCBV_VARdelays.CC = CC_VARdelays;
TR100_SlowerCBV_VARdelays.BOLD_Model = BOLD_Model_VARdelays;
clear S_BOLD_VARdelays CC_VARdelays BOLD_Model_VARdelays

load(fullfile(SourcePath,'PlotResults_200sec_TR100ms_4xSNRMatrix_extravascular\VARcbv\SlowerCBV\SimulationResults.mat'));
TR100_SlowerCBV_VARcbv.S_BOLD = S_BOLD_VARcbv;
TR100_SlowerCBV_VARcbv.CC = CC_VARcbv;
TR100_SlowerCBV_VARcbv.BOLD_Model = BOLD_Model_VARcbv;
clear S_BOLD_VARcbv CC_VARcbv BOLD_Model_VARcbv

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot data to check TRs of full timecourses                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Label = 'VarTR TightCoupling VARm1 VARf1 fullTC';
CheckTC(TR2000_TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1(16, 16).BOLD.bout,...
        TR1000_TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1(16, 16).BOLD.bout,...
        TR100_TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1(16, 16).BOLD.bout,...
        Label, ResultPath)
Label = 'VarTR TightCoupling VARdelays fullTC';
CheckTC(TR2000_TightCoupling_VARdelays.S_BOLD.VARdelays(16, 16).BOLD.bout,...
        TR1000_TightCoupling_VARdelays.S_BOLD.VARdelays(16, 16).BOLD.bout,...
        TR100_TightCoupling_VARdelays.S_BOLD.VARdelays(16, 16).BOLD.bout,...
        Label, ResultPath)
Label = 'VarTR TightCoupling VARcbv fullTC';
CheckTC(TR2000_TightCoupling_VARcbv.S_BOLD.VARcbv(16, 16).BOLD.bout,...
        TR1000_TightCoupling_VARcbv.S_BOLD.VARcbv(16, 16).BOLD.bout,...
        TR100_TightCoupling_VARcbv.S_BOLD.VARcbv(16, 16).BOLD.bout,...
        Label, ResultPath)
Label = 'VarTR SlowerCBV VARm1 VARf1 fullTC';
CheckTC(TR2000_SlowerCBV_VARm1_VARf1.S_BOLD.VARm1_VARf1(16, 16).BOLD.bout,...
        TR1000_SlowerCBV_VARm1_VARf1.S_BOLD.VARm1_VARf1(16, 16).BOLD.bout,...
        TR100_SlowerCBV_VARm1_VARf1.S_BOLD.VARm1_VARf1(16, 16).BOLD.bout,...
        Label, ResultPath)
Label = 'VarTR SlowerCBV VARdelays fullTC';
CheckTC(TR2000_SlowerCBV_VARdelays.S_BOLD.VARdelays(16, 16).BOLD.bout,...
        TR1000_SlowerCBV_VARdelays.S_BOLD.VARdelays(16, 16).BOLD.bout,...
        TR100_SlowerCBV_VARdelays.S_BOLD.VARdelays(16, 16).BOLD.bout,...
        Label, ResultPath)
Label = 'VarTR SlowerCBV VARcbv fullTC';
CheckTC(TR2000_SlowerCBV_VARcbv.S_BOLD.VARcbv(16, 16).BOLD.bout,...
        TR1000_SlowerCBV_VARcbv.S_BOLD.VARcbv(16, 16).BOLD.bout,...
        TR100_SlowerCBV_VARcbv.S_BOLD.VARcbv(16, 16).BOLD.bout,...
        Label, ResultPath)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot data to check TRs of extracted rs-fMRI timecourses                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Label = 'VarTR TightCoupling VARm1 VARf1 rs-TC';
CheckTC(TR2000_TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1(16, 16).BOLD.rs_bout,...
        TR1000_TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1(16, 16).BOLD.rs_bout,...
        TR100_TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1(16, 16).BOLD.rs_bout,...
        Label, ResultPath)
Label = 'VarTR TightCoupling VARdelays rs-TC';
CheckTC(TR2000_TightCoupling_VARdelays.S_BOLD.VARdelays(16, 16).BOLD.rs_bout,...
        TR1000_TightCoupling_VARdelays.S_BOLD.VARdelays(16, 16).BOLD.rs_bout,...
        TR100_TightCoupling_VARdelays.S_BOLD.VARdelays(16, 16).BOLD.rs_bout,...
        Label, ResultPath)
Label = 'VarTR TightCoupling VARcbv rs-TC';
CheckTC(TR2000_TightCoupling_VARcbv.S_BOLD.VARcbv(16, 16).BOLD.rs_bout,...
        TR1000_TightCoupling_VARcbv.S_BOLD.VARcbv(16, 16).BOLD.rs_bout,...
        TR100_TightCoupling_VARcbv.S_BOLD.VARcbv(16, 16).BOLD.rs_bout,...
        Label, ResultPath)
Label = 'VarTR SlowerCBV VARm1 VARf1 rs-TC';
CheckTC(TR2000_SlowerCBV_VARm1_VARf1.S_BOLD.VARm1_VARf1(16, 16).BOLD.rs_bout,...
        TR1000_SlowerCBV_VARm1_VARf1.S_BOLD.VARm1_VARf1(16, 16).BOLD.rs_bout,...
        TR100_SlowerCBV_VARm1_VARf1.S_BOLD.VARm1_VARf1(16, 16).BOLD.rs_bout,...
        Label, ResultPath)
Label = 'VarTR SlowerCBV VARdelays rs-TC';
CheckTC(TR2000_SlowerCBV_VARdelays.S_BOLD.VARdelays(16, 16).BOLD.rs_bout,...
        TR1000_SlowerCBV_VARdelays.S_BOLD.VARdelays(16, 16).BOLD.rs_bout,...
        TR100_SlowerCBV_VARdelays.S_BOLD.VARdelays(16, 16).BOLD.rs_bout,...
        Label, ResultPath)
Label = 'VarTR SlowerCBV VARcbv rs-TC';
CheckTC(TR2000_SlowerCBV_VARcbv.S_BOLD.VARcbv(16, 16).BOLD.rs_bout,...
        TR1000_SlowerCBV_VARcbv.S_BOLD.VARcbv(16, 16).BOLD.rs_bout,...
        TR100_SlowerCBV_VARcbv.S_BOLD.VARcbv(16, 16).BOLD.rs_bout,...
        Label, ResultPath)    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot neuronal inputs & simulated reference curves                       %
% NOTE: For reference curves, all parameters are identical, execept Tv    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Label = 'RefCurves TR100';
plotBOLDSimulationReferenceCurves(TR100_TightCoupling_VARm1_VARf1.S_BOLD,...
                                  TR100_SlowerCBV_VARm1_VARf1.S_BOLD,...
                                                    Label, ResultPath);
Label = 'RefCurves TR1000';
plotBOLDSimulationReferenceCurves(TR1000_TightCoupling_VARm1_VARf1.S_BOLD,...
                                  TR1000_SlowerCBV_VARm1_VARf1.S_BOLD,...
                                                    Label, ResultPath);
Label = 'RefCurves TR2000';
plotBOLDSimulationReferenceCurves(TR2000_TightCoupling_VARm1_VARf1.S_BOLD,...
                                  TR2000_SlowerCBV_VARm1_VARf1.S_BOLD,...
                                                    Label, ResultPath); 
% Plot BOLD time curve for all TRs                                                
Label = 'RefCurves allTRs';
plotBOLDallReferenceCurves(TR100_TightCoupling_VARm1_VARf1.S_BOLD,...
                           TR100_SlowerCBV_VARm1_VARf1.S_BOLD,...
                           TR1000_TightCoupling_VARm1_VARf1.S_BOLD,...
                           TR1000_SlowerCBV_VARm1_VARf1.S_BOLD,...
                           TR2000_TightCoupling_VARm1_VARf1.S_BOLD,...
                           TR2000_SlowerCBV_VARm1_VARf1.S_BOLD,...
                           Label, ResultPath);                                                 
                                                
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot simulated curves for extreme values                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VARm1_VARf1
Label = 'ExtremCurves TR100 TightCoupling VARm1 VARf1';
plotBOLDSimulationInputsResults(TR100_TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1,...
                                                    Label, ResultPath);
Label = 'ExtremCurves TR100 SlowerCBV VARm1 VARf1';
plotBOLDSimulationInputsResults(TR100_SlowerCBV_VARm1_VARf1.S_BOLD.VARm1_VARf1,...
                                                    Label, ResultPath);

Label = 'ExtremCurves TR1000 TightCoupling VARm1 VARf1';
plotBOLDSimulationInputsResults(TR1000_TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1,...
                                                    Label, ResultPath);
Label = 'ExtremCurves TR1000 SlowerCBV VARm1 VARf1';
plotBOLDSimulationInputsResults(TR1000_SlowerCBV_VARm1_VARf1.S_BOLD.VARm1_VARf1,...
                                                    Label, ResultPath);

Label = 'ExtremCurves TR2000 TightCoupling VARm1 VARf1';
plotBOLDSimulationInputsResults(TR2000_TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1,...
                                                    Label, ResultPath);
Label = 'ExtremCurves TR2000 SlowerCBV VARm1 VARf1';
plotBOLDSimulationInputsResults(TR2000_SlowerCBV_VARm1_VARf1.S_BOLD.VARm1_VARf1,...
                                                    Label, ResultPath);
% VARdelays
Label = 'ExtremCurves TR100 TightCoupling VARdelays';
plotBOLDSimulationInputsResults(TR100_TightCoupling_VARdelays.S_BOLD.VARdelays,...
                                                    Label, ResultPath);
Label = 'ExtremCurves TR100 SlowerCBV VARdelays';
plotBOLDSimulationInputsResults(TR100_SlowerCBV_VARdelays.S_BOLD.VARdelays,...
                                                    Label, ResultPath);

Label = 'ExtremCurves TR1000 TightCoupling VARdelays';
plotBOLDSimulationInputsResults(TR1000_TightCoupling_VARdelays.S_BOLD.VARdelays,...
                                                    Label, ResultPath);
Label = 'ExtremCurves TR1000 SlowerCBV VARdelays';
plotBOLDSimulationInputsResults(TR1000_SlowerCBV_VARdelays.S_BOLD.VARdelays,...
                                                    Label, ResultPath);

Label = 'ExtremCurves TR2000 TightCoupling VARdelays';
plotBOLDSimulationInputsResults(TR2000_TightCoupling_VARdelays.S_BOLD.VARdelays,...
                                                    Label, ResultPath);
Label = 'ExtremCurves TR2000 SlowerCBV VARdelays';
plotBOLDSimulationInputsResults(TR2000_SlowerCBV_VARdelays.S_BOLD.VARdelays,...
                                                    Label, ResultPath);
% VARcbv
Label = 'ExtremCurves TR100 TightCoupling VARcbv';
plotBOLDSimulationInputsResults(TR100_TightCoupling_VARcbv.S_BOLD.VARcbv,...
                                                    Label, ResultPath);
Label = 'ExtremCurves TR100 SlowerCBV VARcbv';
plotBOLDSimulationInputsResults(TR100_SlowerCBV_VARcbv.S_BOLD.VARcbv,...
                                                    Label, ResultPath);

Label = 'ExtremCurves TR1000 TightCoupling VARcbv';
plotBOLDSimulationInputsResults(TR1000_TightCoupling_VARcbv.S_BOLD.VARcbv,...
                                                    Label, ResultPath);
Label = 'ExtremCurves TR1000 SlowerCBV VARcbv';
plotBOLDSimulationInputsResults(TR1000_SlowerCBV_VARcbv.S_BOLD.VARcbv,...
                                                    Label, ResultPath);

Label = 'ExtremCurves TR2000 TightCoupling VARcbv';
plotBOLDSimulationInputsResults(TR2000_TightCoupling_VARcbv.S_BOLD.VARcbv,...
                                                    Label, ResultPath);
Label = 'ExtremCurves TR2000 SlowerCBV VARcbv';
plotBOLDSimulationInputsResults(TR2000_SlowerCBV_VARcbv.S_BOLD.VARcbv,...
                                                    Label, ResultPath);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot Summary figure of sumulated BOLD signals without noise             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                                                
Label = 'Summary TR100 TightCoupling VARm1 VARf1';
plotSummaryFigure_VARm1_VARf1(TR100_TightCoupling_VARm1_VARf1,Label, ResultPath)
Label = 'Summary TR100 TightCoupling VARdelays';
plotSummaryFigure_VARdelays(TR100_TightCoupling_VARdelays,Label, ResultPath)
Label = 'Summary TR100 TightCoupling VARcbv';
plotSummaryFigure_VARcbv(TR100_TightCoupling_VARcbv,Label, ResultPath)

Label = 'Summary TR100 SlowerCBV VARm1 VARf1';
plotSummaryFigure_VARm1_VARf1(TR100_SlowerCBV_VARm1_VARf1,Label, ResultPath) 
Label = 'Summary TR100 SlowerCBV VARdelays';
plotSummaryFigure_VARdelays(TR100_SlowerCBV_VARdelays,Label, ResultPath) 
Label = 'Summary TR100 SlowerCBV VARcbv';
plotSummaryFigure_VARcbv(TR100_SlowerCBV_VARcbv,Label, ResultPath) 

Label = 'Summary TR1000 TightCoupling VARm1 VARf1';
plotSummaryFigure_VARm1_VARf1(TR1000_TightCoupling_VARm1_VARf1,Label, ResultPath)
Label = 'Summary TR1000 TightCoupling VARdelays';
plotSummaryFigure_VARdelays(TR1000_TightCoupling_VARdelays,Label, ResultPath)
Label = 'Summary TR1000 TightCoupling VARcbv';
plotSummaryFigure_VARcbv(TR1000_TightCoupling_VARcbv,Label, ResultPath)

Label = 'Summary TR1000 SlowerCBV VARm1 VARf1';
plotSummaryFigure_VARm1_VARf1(TR1000_SlowerCBV_VARm1_VARf1,Label, ResultPath) 
Label = 'Summary TR1000 SlowerCBV VARdelays';
plotSummaryFigure_VARdelays(TR1000_SlowerCBV_VARdelays,Label, ResultPath) 
Label = 'Summary TR1000 SlowerCBV VARcbv';
plotSummaryFigure_VARcbv(TR1000_SlowerCBV_VARcbv,Label, ResultPath) 

Label = 'Summary TR2000 TightCoupling VARm1 VARf1';
plotSummaryFigure_VARm1_VARf1(TR2000_TightCoupling_VARm1_VARf1,Label, ResultPath)
Label = 'Summary TR2000 TightCoupling VARdelays';
plotSummaryFigure_VARdelays(TR2000_TightCoupling_VARdelays,Label, ResultPath)
Label = 'Summary TR2000 TightCoupling VARcbv';
plotSummaryFigure_VARcbv(TR2000_TightCoupling_VARcbv,Label, ResultPath)

Label = 'Summary TR2000 SlowerCBV VARm1 VARf1';
plotSummaryFigure_VARm1_VARf1(TR2000_SlowerCBV_VARm1_VARf1,Label, ResultPath) 
Label = 'Summary TR2000 SlowerCBV VARdelays';
plotSummaryFigure_VARdelays(TR2000_SlowerCBV_VARdelays,Label, ResultPath) 
Label = 'Summary TR2000 SlowerCBV VARcbv';
plotSummaryFigure_VARcbv(TR2000_SlowerCBV_VARcbv,Label, ResultPath) 

% Plot summary of all scenarios
Label = 'Summary TR100 SlowerCBV all Scenarios';
plotSummaryFigure_allScenarios(TR100_SlowerCBV_VARm1_VARf1,... 
                               TR100_SlowerCBV_VARdelays,...
                               TR100_SlowerCBV_VARcbv,Label, ResultPath)                           
Label = 'Summary TR1000 SlowerCBV all Scenarios';
plotSummaryFigure_allScenarios(TR1000_SlowerCBV_VARm1_VARf1,... 
                               TR1000_SlowerCBV_VARdelays,...
                               TR1000_SlowerCBV_VARcbv,Label, ResultPath) 
Label = 'Summary TR2000 SlowerCBV all Scenarios';
plotSummaryFigure_allScenarios(TR2000_SlowerCBV_VARm1_VARf1,... 
                               TR2000_SlowerCBV_VARdelays,...
                               TR2000_SlowerCBV_VARcbv,Label, ResultPath)
                           
Label = 'Summary TR100 TightCoupling all Scenarios';
plotSummaryFigure_allScenarios(TR100_TightCoupling_VARm1_VARf1,... 
                               TR100_TightCoupling_VARdelays,...
                               TR100_TightCoupling_VARcbv,Label, ResultPath)                           
Label = 'Summary TR1000 TightCoupling all Scenarios';
plotSummaryFigure_allScenarios(TR1000_TightCoupling_VARm1_VARf1,... 
                               TR1000_TightCoupling_VARdelays,...
                               TR1000_TightCoupling_VARcbv,Label, ResultPath) 
Label = 'Summary TR2000 TightCoupling all Scenarios';
plotSummaryFigure_allScenarios(TR2000_TightCoupling_VARm1_VARf1,... 
                               TR2000_TightCoupling_VARdelays,...
                               TR2000_TightCoupling_VARcbv,Label, ResultPath)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot CC matrices for different SNR                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%VARm1_VARf1
Label = 'CC TR100 SNRvar TightCoupling VARm1 VARf1';
generateCCfigure_VARm1_VARf1(TR100_TightCoupling_VARm1_VARf1,Label, ResultPath);
Label = 'CC TR100 SNRvar SlowerCBV VARm1 VARf1';
generateCCfigure_VARm1_VARf1(TR100_SlowerCBV_VARm1_VARf1,Label, ResultPath);                                             
Label = 'CC TR1000 SNRvar TightCoupling VARm1 VARf1';
generateCCfigure_VARm1_VARf1(TR1000_TightCoupling_VARm1_VARf1,Label, ResultPath);
Label = 'CC TR1000 SNRvar SlowerCBV VARm1 VARf1';
generateCCfigure_VARm1_VARf1(TR1000_SlowerCBV_VARm1_VARf1,Label, ResultPath);
Label = 'CC TR2000 SNRvar TightCoupling VARm1 VARf1';
generateCCfigure_VARm1_VARf1(TR2000_TightCoupling_VARm1_VARf1,Label, ResultPath);
Label = 'CC TR2000 SNRvar SlowerCBV VARm1 VARf1';
generateCCfigure_VARm1_VARf1(TR2000_SlowerCBV_VARm1_VARf1,Label, ResultPath);

%VARdelays
Label = 'CC TR100 SNRvar TightCoupling VARdelays';
generateCCfigure_VARdelays(TR100_TightCoupling_VARdelays,Label, ResultPath);
Label = 'CC TR100 SNRvar SlowerCBV VARdelays';
generateCCfigure_VARdelays(TR100_SlowerCBV_VARdelays,Label, ResultPath);
Label = 'CC TR1000 SNRvar TightCoupling VARdelays';
generateCCfigure_VARdelays(TR1000_TightCoupling_VARdelays,Label, ResultPath);
Label = 'CC TR1000 SNRvar SlowerCBV VARdelays';
generateCCfigure_VARdelays(TR1000_SlowerCBV_VARdelays,Label, ResultPath);                                                
Label = 'CC TR2000 SNRvar TightCoupling VARdelays';
generateCCfigure_VARdelays(TR2000_TightCoupling_VARdelays,Label, ResultPath);
Label = 'CC TR2000 SNRvar SlowerCBV VARdelays';
generateCCfigure_VARdelays(TR2000_SlowerCBV_VARdelays,Label, ResultPath);     
                                                
%VARcbv                                                
Label = 'CC TR100 SNRvar TightCoupling VARcbv';
generateCCfigure_VARcbv(TR100_TightCoupling_VARcbv,Label, ResultPath);
Label = 'CC TR100 SNRvar SlowerCBV VARcbv';
generateCCfigure_VARcbv(TR100_SlowerCBV_VARcbv,Label, ResultPath);  
Label = 'CC TR1000 SNRvar TightCoupling VARcbv';
generateCCfigure_VARcbv(TR1000_TightCoupling_VARcbv,Label, ResultPath);
Label = 'CC TR1000 SNRvar SlowerCBV VARcbv';
generateCCfigure_VARcbv(TR1000_SlowerCBV_VARcbv,Label, ResultPath);                                                  
Label = 'CC TR2000 SNRvar TightCoupling VARcbv';
generateCCfigure_VARcbv(TR2000_TightCoupling_VARcbv,Label, ResultPath);
Label = 'CC TR2000 SNRvar SlowerCBV VARcbv';
generateCCfigure_VARcbv(TR2000_SlowerCBV_VARcbv,Label, ResultPath);     

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot CC matrices @different SNRs with 16x16 random noise realisations   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
Label = 'CC TR100 SNRvar16x16matrix TightCoupling VARm1_VARf1';
generateCCfigure_VARm1_VARf1_SNRvar16x16matrix(TR100_TightCoupling_VARm1_VARf1,...
                                               Label, ResultPath); 
Label = 'CC TR100 SNRvar16x16matrix SlowerCBV VARm1_VARf1';
generateCCfigure_VARm1_VARf1_SNRvar16x16matrix(TR100_SlowerCBV_VARm1_VARf1,...
                                               Label, ResultPath);                                           
Label = 'CC TR1000 SNRvar16x16matrix TightCoupling VARm1_VARf1';
generateCCfigure_VARm1_VARf1_SNRvar16x16matrix(TR1000_TightCoupling_VARm1_VARf1,...
                                               Label, ResultPath); 
Label = 'CC TR1000 SNRvar16x16matrix SlowerCBV VARm1_VARf1';
generateCCfigure_VARm1_VARf1_SNRvar16x16matrix(TR1000_SlowerCBV_VARm1_VARf1,...
                                               Label, ResultPath); 
Label = 'CC TR2000 SNRvar16x16matrix TightCoupling VARm1_VARf1';
generateCCfigure_VARm1_VARf1_SNRvar16x16matrix(TR2000_TightCoupling_VARm1_VARf1,...
                                               Label, ResultPath); 
Label = 'CC TR2000 SNRvar16x16matrix SlowerCBV VARm1_VARf1';
generateCCfigure_VARm1_VARf1_SNRvar16x16matrix(TR2000_SlowerCBV_VARm1_VARf1,...
                                               Label, ResultPath);                                            

Label = 'CC TR100 SNRvar16x16matrix TightCoupling VARdelays';
generateCCfigure_VARdelays_SNRvar16x16matrix(TR100_TightCoupling_VARdelays,...
                                               Label, ResultPath); 
Label = 'CC TR100 SNRvar16x16matrix SlowerCBV VARdelays';
generateCCfigure_VARdelays_SNRvar16x16matrix(TR100_SlowerCBV_VARdelays,...
                                               Label, ResultPath);                                           
Label = 'CC TR1000 SNRvar16x16matrix TightCoupling VARdelays';
generateCCfigure_VARdelays_SNRvar16x16matrix(TR1000_TightCoupling_VARdelays,...
                                               Label, ResultPath); 
Label = 'CC TR1000 SNRvar16x16matrix SlowerCBV VARdelays';
generateCCfigure_VARdelays_SNRvar16x16matrix(TR1000_SlowerCBV_VARdelays,...
                                               Label, ResultPath); 
Label = 'CC TR2000 SNRvar16x16matrix TightCoupling VARdelays';
generateCCfigure_VARdelays_SNRvar16x16matrix(TR2000_TightCoupling_VARdelays,...
                                               Label, ResultPath); 
Label = 'CC TR2000 SNRvar16x16matrix SlowerCBV VARdelays';
generateCCfigure_VARdelays_SNRvar16x16matrix(TR2000_SlowerCBV_VARdelays,...
                                               Label, ResultPath);  
                                           
Label = 'CC TR100 SNRvar16x16matrix TightCoupling VARcbv';
generateCCfigure_VARcbv_SNRvar16x16matrix(TR100_TightCoupling_VARcbv,...
                                               Label, ResultPath); 
Label = 'CC TR100 SNRvar16x16matrix SlowerCBV VARcbv';
generateCCfigure_VARcbv_SNRvar16x16matrix(TR100_SlowerCBV_VARcbv,...
                                               Label, ResultPath);                                           
Label = 'CC TR1000 SNRvar16x16matrix TightCoupling VARcbv';
generateCCfigure_VARcbv_SNRvar16x16matrix(TR1000_TightCoupling_VARcbv,...
                                               Label, ResultPath); 
Label = 'CC TR1000 SNRvar16x16matrix SlowerCBV VARcbv';
generateCCfigure_VARcbv_SNRvar16x16matrix(TR1000_SlowerCBV_VARcbv,...
                                               Label, ResultPath); 
Label = 'CC TR2000 SNRvar16x16matrix TightCoupling VARcbv';
generateCCfigure_VARcbv_SNRvar16x16matrix(TR2000_TightCoupling_VARcbv,...
                                               Label, ResultPath); 
Label = 'CC TR2000 SNRvar16x16matrix SlowerCBV VARcbv';
generateCCfigure_VARcbv_SNRvar16x16matrix(TR2000_SlowerCBV_VARcbv,...
                                               Label, ResultPath);                                            
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot CC matrices @SNR0 = 250 with 16x16 random noise realisations       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
Label = 'CC TR100 SNR0=250 TightCoupling';
generateCCfigure(TR100_TightCoupling_VARm1_VARf1,...
                 TR100_TightCoupling_VARdelays,...
                 TR100_TightCoupling_VARcbv,Label, ResultPath);             
Label = 'CC TR100 SNR0=250 SlowerCBV';
generateCCfigure(TR100_SlowerCBV_VARm1_VARf1,...
                 TR100_SlowerCBV_VARdelays,...
                 TR100_SlowerCBV_VARcbv,Label, ResultPath);  
             
Label = 'CC TR1000 SNR0=250 TightCoupling';
generateCCfigure(TR1000_TightCoupling_VARm1_VARf1,...
                 TR1000_TightCoupling_VARdelays,...
                 TR1000_TightCoupling_VARcbv,Label, ResultPath); 
Label = 'CC TR1000 SNR0=250 SlowerCBV';
generateCCfigure(TR1000_SlowerCBV_VARm1_VARf1,...
                 TR1000_SlowerCBV_VARdelays,...
                 TR1000_SlowerCBV_VARcbv,Label, ResultPath);  

Label = 'CC TR2000 SNR0=250 TightCoupling';
generateCCfigure(TR2000_TightCoupling_VARm1_VARf1,...
                 TR2000_TightCoupling_VARdelays,...
                 TR2000_TightCoupling_VARcbv,Label, ResultPath);                     
Label = 'CC TR2000 SNR0=250 SlowerCBV';
generateCCfigure(TR2000_SlowerCBV_VARm1_VARf1,...
                 TR2000_SlowerCBV_VARdelays,...
                 TR2000_SlowerCBV_VARcbv,Label, ResultPath);    
             
% Plot combined CC matrices for TightCoupling & SlowerCBV 
Label = 'CC TR100 SNR0=250';
generateCombinedCCfigure(TR100_TightCoupling_VARm1_VARf1,...
                         TR100_TightCoupling_VARdelays,...
                         TR100_TightCoupling_VARcbv,...
                         TR100_SlowerCBV_VARm1_VARf1,...
                         TR100_SlowerCBV_VARdelays,...
                         TR100_SlowerCBV_VARcbv,Label, ResultPath);
                     
Label = 'CC TR1000 SNR0=250';
generateCombinedCCfigure(TR1000_TightCoupling_VARm1_VARf1,...
                         TR1000_TightCoupling_VARdelays,...
                         TR1000_TightCoupling_VARcbv,...
                         TR1000_SlowerCBV_VARm1_VARf1,...
                         TR1000_SlowerCBV_VARdelays,...
                         TR1000_SlowerCBV_VARcbv,Label, ResultPath);
             
Label = 'CC TR2000 SNR0=250';
generateCombinedCCfigure(TR2000_TightCoupling_VARm1_VARf1,...
                         TR2000_TightCoupling_VARdelays,...
                         TR2000_TightCoupling_VARcbv,...
                         TR2000_SlowerCBV_VARm1_VARf1,...
                         TR2000_SlowerCBV_VARdelays,...
                         TR2000_SlowerCBV_VARcbv,Label, ResultPath); 
                     
% Plot CC matrices for different TRs (separately for different scenarios
Label = 'CC VARm1_VARf1 VARTR SNR0=250 SlowerCBV';
generateCCfigure_VARm1_VARf1_VARTR(TR100_SlowerCBV_VARm1_VARf1,...
                                   TR1000_SlowerCBV_VARm1_VARf1,...
                                   TR2000_SlowerCBV_VARm1_VARf1,...
                                   Label, ResultPath); 
Label = 'CC VARm1_VARf1 VARTR SNR0=250 TightCoupling';
generateCCfigure_VARm1_VARf1_VARTR(TR100_TightCoupling_VARm1_VARf1,...
                                   TR1000_TightCoupling_VARm1_VARf1,...
                                   TR2000_TightCoupling_VARm1_VARf1,...
                                   Label, ResultPath); 
                          
Label = 'CC VARdelays VARTR SNR0=250 SlowerCBV';
generateCCfigure_VARdelays_VARTR(TR100_SlowerCBV_VARdelays,...
                                   TR1000_SlowerCBV_VARdelays,...
                                   TR2000_SlowerCBV_VARdelays,...
                                   Label, ResultPath); 
Label = 'CC VARdelays VARTR SNR0=250 TightCoupling';
generateCCfigure_VARdelays_VARTR(TR100_TightCoupling_VARdelays,...
                                   TR1000_TightCoupling_VARdelays,...
                                   TR2000_TightCoupling_VARdelays,...
                                   Label, ResultPath);   
                               
Label = 'CC VARcbv VARTR SNR0=250 SlowerCBV';
generateCCfigure_VARcbv_VARTR(TR100_SlowerCBV_VARcbv,...
                                   TR1000_SlowerCBV_VARcbv,...
                                   TR2000_SlowerCBV_VARcbv,...
                                   Label, ResultPath); 
Label = 'CC VARcbv VARTR SNR0=250 TightCoupling';
generateCCfigure_VARcbv_VARTR(TR100_TightCoupling_VARcbv,...
                                   TR1000_TightCoupling_VARcbv,...
                                   TR2000_TightCoupling_VARcbv,...
                                   Label, ResultPath);                                  
             