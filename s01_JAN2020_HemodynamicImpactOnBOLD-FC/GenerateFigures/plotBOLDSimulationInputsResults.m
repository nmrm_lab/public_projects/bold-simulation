function plotBOLDSimulationInputsResults(BalloonModel, Label, ResultPath)
% m1Val == f1Val == 0 -> use neuronal input '1' as reference

%plot time course variables
totalDur = BalloonModel(1,1).SimulationPars.totalDur;

figure('Units','centimeters','Position',[10 0 40 30]); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot simulated curves for extreme values                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for ncnt = 1:4
    switch ncnt
        case 1
            m1Val = 1;
            f1Val = 1;
            index = 1;
        case 2
            m1Val = 1;
            f1Val = 16;
            index = 2;
        case 3
            m1Val = 16;
            f1Val = 1;
            index = 3;
        case 4
            m1Val = 16;
            f1Val = 16;
            index = 4;
    end
    subplot(2,4,index);
    plot(100*(BalloonModel(m1Val,f1Val).CBF.fin-1),'b-','LineWidth',2)
    hold on
    plot(100*(BalloonModel(m1Val,f1Val).CBF.fout-1),'c-','LineWidth',2)
    plot(100*(BalloonModel(m1Val,f1Val).CMRO2.min-1),'r-','LineWidth',2)
    plot(100*(BalloonModel(m1Val,f1Val).CBV.vout-1),'g','LineWidth',2)

    legend('CBF_{in}','CBF_{out}','CMRO2','CBV_v','FontSize',10)
    axis([0 totalDur -5 65])
    if index == 1
        title(Label)
    else
        title('')
    end
    legend1 = sprintf('= %1.2f',BalloonModel(m1Val,f1Val).CBF.amplitude);
    legend2 = sprintf('= %1.1f',BalloonModel(m1Val,f1Val).CBF.tau);
    legend3 = sprintf('= %1.2f',BalloonModel(m1Val,f1Val).CMRO2.amplitude);
    legend4 = sprintf('= %1.1f',BalloonModel(m1Val,f1Val).CMRO2.tau);
    legend5 = sprintf('= %1.1f',...
            (max(BalloonModel(m1Val,f1Val).CBF.fin)-1)/...
            (max(BalloonModel(m1Val,f1Val).CMRO2.min)-1));
    legend6 = sprintf('= %1.3f',BalloonModel(m1Val,f1Val).CBV.alpha);
    legend7 = sprintf('= %1.1f',BalloonModel(m1Val,f1Val).CBV.tau);

    text(50,60,'f1','Fontsize',10);
    text(80,60,legend1,'Fontsize',10);
    text(50,56,'\tau_f','Fontsize',10);
    text(80,56,legend2,'Fontsize',10);
    text(50,50,'m1','Fontsize',10);
    text(80,50,legend3,'Fontsize',10);
    text(50,46,'\tau_m','Fontsize',10);
    text(80,46,legend4,'Fontsize',10);
    text(50,42,'n','Fontsize',10);
    text(80,42,legend5,'Fontsize',10);
    text(50,36,'\alpha','Fontsize',10);
    text(80,36,legend6,'Fontsize',10);
    text(50,32,'\tau_v','Fontsize',10);
    text(80,32,legend7,'Fontsize',10);

    % yticks([1.0 1.2 1.4 1.6 1.8 2.0])
    yticks([0 50])
    ylabel('\Delta f / f_0 [%]')
    % xticks(0:20:20*floor(totalDur/20))
    xlabel('time [sec]')
    ax = gca;
    ax.FontSize = 11;
    hold off

    subplot(2,4,index+4);

    plot(100*(BalloonModel(m1Val,f1Val).BOLD.boutNoise-1),...
                                                        'r','LineWidth',1)
    hold on
    plot(100*(BalloonModel(m1Val,f1Val).BOLD.bout-1),...
                                                        'b','LineWidth',2)
    plot([0 totalDur], [0 0],'k--','LineWidth',1)
    legend('noisy BOLD','model BOLD','FontSize',10)
    axis([0 totalDur -3 4])
    title('')
    % yticks([0.99 1.00 1.01 1.02 1.03 1.04])
    yticks([-3 -2 -1 0 1 2 3 4])
    text_str = sprintf('SNR_0 = %3.0f',...
        BalloonModel(m1Val,f1Val).SimulationPars.SNR0);
    text(30,3.2,text_str)
    ylabel('\Delta S_{BOLD} / S_0 [%]')
    xlabel('time [sec]')                
    ax = gca;
    ax.FontSize = 11;
    hold off
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% save plot to file                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin > 1
    FNout = fullfile(ResultPath,sprintf('%s.tif',Label));
    saveas(gcf,FNout);
end
end

