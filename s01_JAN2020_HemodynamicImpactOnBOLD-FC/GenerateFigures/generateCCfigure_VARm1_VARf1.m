function generateCCfigure_VARm1_VARf1(VARm1_VARf1, Label, ResultPath)
S_BOLD = VARm1_VARf1.S_BOLD;
CC = VARm1_VARf1.CC;
% CP 29MAR19 Plot CC arrays
% CP 01OCT19 generate figures for paper
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% convenience definitions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% temporal resolution of simulated BOLD signal curves [sec]
SampleTime = S_BOLD.VARm1_VARf1(1,1).SimulationPars.SampleTime; 
% simulated time period
totalDur = S_BOLD.VARm1_VARf1(1,1).SimulationPars.totalDur; 
% total duration of initial block stimulus
blockDur = S_BOLD.VARm1_VARf1(1,1).SimulationPars.blockDur;
% number of prescribed CMRO2 values
m1Nsteps = S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Nsteps;
% minimum of prescribed CMRO2 values
m1Min = S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Min;
% step size for CMRO2 changes
m1StepSize = S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1StepSize;
% max CBF value
m1Max = m1Min + (m1Nsteps-1) * m1StepSize;
% minimum of prescribed CBF values
f1Min = S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1Min;
% number of prescribed CBF values
f1NSteps = S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1NSteps; 
% step size for CBF changes
f1StepSize = S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1StepSize; 
% max CBF value
f1Max = f1Min + (f1NSteps-1) * f1StepSize;

% allow for (ninterval+1) x & y AxisTicks
ninterval = 3;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%   Plot CC arrays created from BOLD TCs with different SNRs    %%%%%%
%%%%%%  (SNR array size = 1)                                         %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure('Units','centimeters','Position',[10 5 40 16]); 
    % Global settings
    FigureProps.GlobalTitleStr = sprintf('%s \n',Label); 
    FigureProps.cFsize = 1.1; % Fontsize 12 pt 
    FigureProps.cLabel = '';
    FigureProps.minVal = m1Min;
    FigureProps.maxVal = m1Max;    
%     FigureProps.NewColorMap = nawhimar;
    FigureProps.NewColorMap = gray;
    FigureProps.TitleStr = ''; % title for subplots
    FigureProps.xAxisLabel = 'm1';
    FigureProps.xAxisTick = m1Min:round(m1Nsteps/ninterval):m1Nsteps;
    FigureProps.yAxisLabel = 'f1'; 
    FigureProps.yAxisTick = f1Min:round(f1NSteps/ninterval):f1NSteps;     
    for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
            sprintf('%1.1f',m1Min + (count-1)*((m1Max-m1Min)/ninterval));
    end
    yAxTicks = f1NSteps:-round(f1NSteps/ninterval):f1Min; % axis is inverted!
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.2f',f1Max - (count-1)*((f1Max-f1Min)/ninterval));
    end
       
subplot(2,4,1)
    FigureProps.cLabel = ''; 
%     FigureProps.minVal = -0.5;
%     FigureProps.maxVal = 0.5;
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_1000(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_1000(:));
    FigureProps.TitleStr = 'SNR_0 = 1000';      
    newdisp_FC(CC.varSNR.RHO_SNR0_1000, FigureProps)    
subplot(2,4,2)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_500(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_500(:));
    FigureProps.TitleStr = 'SNR_0 = 500'; 
    newdisp_FC(CC.varSNR.RHO_SNR0_500, FigureProps)
subplot(2,4,3)
    FigureProps.cLabel = ''; 
     FigureProps.minVal = min(CC.varSNR.RHO_SNR0_250(:));
     FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_250(:));
    FigureProps.TitleStr = 'SNR_0 = 250'; 
    newdisp_FC(CC.varSNR.RHO_SNR0_250, FigureProps)
subplot(2,4,4)
    FigureProps.cLabel = 'CC'; 
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_125(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_125(:));
    FigureProps.TitleStr = 'SNR_0 = 125';     
    newdisp_FC(CC.varSNR.RHO_SNR0_125, FigureProps)    
subplot(2,4,5)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;    
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_1000(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_1000(:));
    FigureProps.TitleStr = '';      
    newdisp_FC(CC.varSNR.PVAL_SNR0_1000, FigureProps)    
subplot(2,4,6)
    FigureProps.cLabel = ''; 
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_500(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_500(:));
    FigureProps.TitleStr = ''; 
    newdisp_FC(CC.varSNR.PVAL_SNR0_500, FigureProps)
subplot(2,4,7)
    FigureProps.cLabel = ''; 
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_250(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_250(:));
    FigureProps.TitleStr = ''; 
    newdisp_FC(CC.varSNR.PVAL_SNR0_250, FigureProps)  
subplot(2,4,8)
    FigureProps.cLabel = 'p value'; 
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_125(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_125(:));
    FigureProps.TitleStr = '';     
    newdisp_FC(CC.varSNR.PVAL_SNR0_125, FigureProps) 
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% save plot to file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FNout = fullfile(ResultPath,sprintf('%s_varSNR.tif',Label));
saveas(gcf,FNout);    

end

