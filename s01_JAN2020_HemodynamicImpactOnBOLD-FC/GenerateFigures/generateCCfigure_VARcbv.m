function generateCCfigure_VARcbv(VARdelays, Label, ResultPath)
S_BOLD = VARdelays.S_BOLD;
CC = VARdelays.CC;
% CP 29MAR19 Plot CC arrays
% CP 01OCT19 generate figures for paper
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% convenience definitions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% temporal resolution of simulated BOLD signal curves [sec]
SampleTime = S_BOLD.VARcbv(1,1).SimulationPars.SampleTime; 
% simulated time period
totalDur = S_BOLD.VARcbv(1,1).SimulationPars.totalDur; 
% total duration of initial block stimulus
blockDur = S_BOLD.VARcbv(1,1).SimulationPars.blockDur;
% number of prescribed CBV temporal delay values
cbvTauNsteps = S_BOLD.VARcbv(1,1).SimulationPars.cbvTauNsteps;
% minimum of prescribed CBV temporal delay values
cbvTauMin = S_BOLD.VARcbv(1,1).SimulationPars.cbvTauMin;
% step size for CBV temporal delay changes
cbvTauStepSize = S_BOLD.VARcbv(1,1).SimulationPars.cbvTauStepSize;
% max CBV temporal delay value
cbvTauMax = cbvTauMin + (cbvTauNsteps-1) * cbvTauStepSize;
% minimum of prescribed alpha values
cbvAlphaMin = S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaMin;
% number of prescribed alpha values
cbvAlphaNsteps = S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaNsteps; 
% step size for alpha changes
cbvAlphaStepSize = S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaStepSize; 
% max alpha value
cbvAlphaMax = cbvAlphaMin + (cbvAlphaNsteps-1) * cbvAlphaStepSize;

% allow for (ninterval+1) x & y AxisTicks
ninterval = 3;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%   Plot CC arrays created from BOLD TCs with different SNRs    %%%%%%
%%%%%%  (SNR array size = 1)                                         %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure('Units','centimeters','Position',[10 5 40 16]); 
    % Global settings
    FigureProps.GlobalTitleStr = sprintf('%s \n',Label); 
    FigureProps.cFsize = 1.1; % Fontsize 12 pt 
    FigureProps.cLabel = ''; 
    FigureProps.minVal = cbvTauMin;
    FigureProps.maxVal = cbvTauMax;   
%     FigureProps.NewColorMap = nawhimar;
    FigureProps.NewColorMap = gray;
    FigureProps.TitleStr = ''; % title for subplots
    FigureProps.xAxisLabel = '\tau_v';   
    FigureProps.xAxisTick = 1:round(cbvTauNsteps/ninterval):cbvTauNsteps;    
    for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
            sprintf('%1.1f',cbvTauMin + (count-1)*((cbvTauMax-cbvTauMin)/ninterval));
    end
    FigureProps.yAxisLabel = '\alpha'; 
    FigureProps.yAxisTick = 1:round(cbvAlphaNsteps/ninterval):cbvAlphaNsteps;    
    yAxTicks = cbvAlphaMax:-round(cbvAlphaNsteps/ninterval):cbvAlphaMin; % axis is inverted!
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.3f',cbvAlphaMax - (count-1)*((cbvAlphaMax-cbvAlphaMin)/ninterval));
    end   
    
subplot(2,4,1)
    FigureProps.cLabel = ''; 
%     FigureProps.minVal = 0;
%     FigureProps.maxVal = 0.5;
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_1000(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_1000(:));
    FigureProps.TitleStr = 'SNR_0 = 1000';      
    newdisp_FC(CC.varSNR.RHO_SNR0_1000, FigureProps)    
subplot(2,4,2)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_500(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_500(:));
    FigureProps.TitleStr = 'SNR_0 = 500'; 
    newdisp_FC(CC.varSNR.RHO_SNR0_500, FigureProps)
subplot(2,4,3)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_250(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_250(:));
    FigureProps.TitleStr = 'SNR_0 = 250'; 
    newdisp_FC(CC.varSNR.RHO_SNR0_250, FigureProps)
subplot(2,4,4)
    FigureProps.cLabel = 'CC'; 
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_125(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_125(:));
    FigureProps.TitleStr = 'SNR_0 = 125';     
    newdisp_FC(CC.varSNR.RHO_SNR0_125, FigureProps)
subplot(2,4,5)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;    
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_1000(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_1000(:));
    FigureProps.TitleStr = 'SNR_0 = 1000';      
    newdisp_FC(CC.varSNR.PVAL_SNR0_1000, FigureProps)    
subplot(2,4,6)
    FigureProps.cLabel = ''; 
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_500(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_500(:));
    FigureProps.TitleStr = 'SNR_0 = 500'; 
    newdisp_FC(CC.varSNR.PVAL_SNR0_500, FigureProps)
subplot(2,4,7)
    FigureProps.cLabel = ''; 
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_250(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_250(:));
    FigureProps.TitleStr = 'SNR_0 = 250'; 
    newdisp_FC(CC.varSNR.PVAL_SNR0_250, FigureProps)    
subplot(2,4,8)
    FigureProps.cLabel = 'p value'; 
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_125(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_125(:));
    FigureProps.TitleStr = 'SNR_0 = 125';     
    newdisp_FC(CC.varSNR.PVAL_SNR0_125, FigureProps)
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% save plot to file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FNout = fullfile(ResultPath,sprintf('%s_varSNR.tif',Label));
saveas(gcf,FNout);      
 
end

