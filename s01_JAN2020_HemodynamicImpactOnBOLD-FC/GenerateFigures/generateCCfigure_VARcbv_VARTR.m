function generateCCfigure_VARcbv_VARTR(TR100, TR1000, TR2000, ...
                                            Label, ResultPath)
% CP 29MAR19 Plot CC arrays
% CP 01OCT19 generate figures for paper
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%   Plot CC array created from BOLD TCs with same SNRs          %%%%%%
%%%%%%  (SNR array size = BOLD_Model.SimulationPars.SNRmatrixSize)   %%%%%%
%%%%%%  but different random number realizations                     %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SNR matrix size --> sqrt(number of different random noise realizations)
% --> create SmS x SmS patches in CC maps with same SNR but different noise
%     realizations
SmS = TR100.S_BOLD.VARcbv(1,1).SimulationPars.SNRmatrixSize;
% Border width between SmS x SmS patches with same SNR
% gap = VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNRgapSize;
gap = TR100.S_BOLD.VARcbv(1,1).SimulationPars.SNRgapSize;

figure('Units','centimeters','Position',[10 5 40 20]); 
    % Global settings %%%%% 
    FigureProps.GlobalTitleStr = '';%sprintf('%s\n\n',Label);
    FigureProps.cFsize = 1.1; % Fontsize 12 pt 
    FigureProps.cLabel = ''; 
%     FigureProps.NewColorMap = nawhimar;
    FigureProps.NewColorMap = gray;
    FigureProps.TitleStr = ''; % title for subplots

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings and plots for VARcbv scenario, TR100 ms                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% number of prescribed CBV temporal delay values
cbvTauNsteps = TR100.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauNsteps;
% minimum of prescribed CBV temporal delay values
cbvTauMin = TR100.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauMin;
% step size for CBV temporal delay changes
cbvTauStepSize = TR100.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauStepSize;
% max CBV temporal delay value
cbvTauMax = cbvTauMin + (cbvTauNsteps-1) * cbvTauStepSize;
% minimum of prescribed alpha values
cbvAlphaMin = TR100.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaMin;
% number of prescribed alpha values
cbvAlphaNsteps = TR100.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaNsteps; 
% step size for alpha changes
cbvAlphaStepSize = TR100.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaStepSize; 
% max alpha value
cbvAlphaMax = cbvAlphaMin + (cbvAlphaNsteps-1) * cbvAlphaStepSize;
% allow for (ninterval+1) x & y AxisTicks
ninterval = 3;

    FigureProps.minVal = cbvTauMin;
    FigureProps.maxVal = cbvTauMax;   
    FigureProps.xAxisTick = 1:round(cbvTauNsteps/ninterval):cbvTauNsteps; 
    % label xAxisTicks with Tv values
    FigureProps.xAxisTick = cbvTauMin+(SmS+gap)/2:...
           	(SmS+gap)*round(cbvTauNsteps/ninterval):cbvTauNsteps*(SmS+gap);   
    % label xAxisTicks with Tv values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',cbvTauMin + (count-1)*((cbvTauMax-cbvTauMin)/ninterval))
	end
    FigureProps.xAxisLabel = '\tau_v'; 
    FigureProps.yAxisTick = cbvAlphaMin+(SmS+gap)/2:...
    	(SmS+gap)*round(cbvAlphaNsteps/ninterval):cbvAlphaNsteps*(SmS+gap); 
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.3f',cbvAlphaMax - (count-1)*((cbvAlphaMax-cbvAlphaMin)/ninterval))
    end
    FigureProps.yAxisLabel = '\alpha_v'; 

subplot(2,3,1)
    FigureProps.cLabel = 'CC (\tau_v, \alpha_v)';
	FigureProps.minVal = -0.40;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(TR100.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.maxVal = max(TR100.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%                      TR100.S_BOLD.VARcbv(1,1).SimulationPars.SNR0);
    FigureProps.TitleStr = sprintf('(a)                       CC = [%3.2f...%3.2f]                  	          ',...
        min(TR100.CC.SNRmatrix.RHO_SNR(:)),max(TR100.CC.SNRmatrix.RHO_SNR(:)));
    newdisp_FC(TR100.CC.SNRmatrix.RHO_SNR, FigureProps)
    
subplot(2,3,4)
    FigureProps.cLabel = 'p value (\tau_v, \alpha_v)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(TR100.CC.SNRmatrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(TR100.CC.SNRmatrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(d)                                                                               ');   
    newdisp_FC(TR100.CC.SNRmatrix.PVAL_SNR, FigureProps)
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings and plots for VARcbv scenario, TR1000 ms                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% number of prescribed CBV temporal delay values
cbvTauNsteps = TR1000.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauNsteps;
% minimum of prescribed CBV temporal delay values
cbvTauMin = TR1000.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauMin;
% step size for CBV temporal delay changes
cbvTauStepSize = TR1000.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauStepSize;
% max CBV temporal delay value
cbvTauMax = cbvTauMin + (cbvTauNsteps-1) * cbvTauStepSize;
% minimum of prescribed alpha values
cbvAlphaMin = TR1000.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaMin;
% number of prescribed alpha values
cbvAlphaNsteps = TR1000.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaNsteps; 
% step size for alpha changes
cbvAlphaStepSize = TR1000.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaStepSize; 
% max alpha value
cbvAlphaMax = cbvAlphaMin + (cbvAlphaNsteps-1) * cbvAlphaStepSize;
% allow for (ninterval+1) x & y AxisTicks
ninterval = 3;

    FigureProps.minVal = cbvTauMin;
    FigureProps.maxVal = cbvTauMax;   
    FigureProps.xAxisTick = 1:round(cbvTauNsteps/ninterval):cbvTauNsteps; 
    % label xAxisTicks with Tv values
    FigureProps.xAxisTick = cbvTauMin+(SmS+gap)/2:...
           	(SmS+gap)*round(cbvTauNsteps/ninterval):cbvTauNsteps*(SmS+gap);   
    % label xAxisTicks with Tv values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',cbvTauMin + (count-1)*((cbvTauMax-cbvTauMin)/ninterval))
	end
    FigureProps.xAxisLabel = '\tau_v'; 
    FigureProps.yAxisTick = cbvAlphaMin+(SmS+gap)/2:...
    	(SmS+gap)*round(cbvAlphaNsteps/ninterval):cbvAlphaNsteps*(SmS+gap); 
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.3f',cbvAlphaMax - (count-1)*((cbvAlphaMax-cbvAlphaMin)/ninterval))
    end
    FigureProps.yAxisLabel = '\alpha_v'; 

subplot(2,3,2)
    FigureProps.cLabel = 'CC (\tau_v, \alpha_v)';
	FigureProps.minVal = -0.40;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(TR1000.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.maxVal = max(TR1000.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%                      TR1000.S_BOLD.VARcbv(1,1).SimulationPars.SNR0);
    FigureProps.TitleStr = sprintf('(a)                       CC = [%3.2f...%3.2f]                  	          ',...
        min(TR1000.CC.SNRmatrix.RHO_SNR(:)),max(TR1000.CC.SNRmatrix.RHO_SNR(:)));
    newdisp_FC(TR1000.CC.SNRmatrix.RHO_SNR, FigureProps)
    
subplot(2,3,5)
    FigureProps.cLabel = 'p value (\tau_v, \alpha_v)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(TR1000.CC.SNRmatrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(TR1000.CC.SNRmatrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(d)                                                                               ');   
    newdisp_FC(TR1000.CC.SNRmatrix.PVAL_SNR, FigureProps)    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings and plots for VARcbv scenario, TR2000 ms                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% number of prescribed CBV temporal delay values
cbvTauNsteps = TR2000.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauNsteps;
% minimum of prescribed CBV temporal delay values
cbvTauMin = TR2000.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauMin;
% step size for CBV temporal delay changes
cbvTauStepSize = TR2000.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauStepSize;
% max CBV temporal delay value
cbvTauMax = cbvTauMin + (cbvTauNsteps-1) * cbvTauStepSize;
% minimum of prescribed alpha values
cbvAlphaMin = TR2000.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaMin;
% number of prescribed alpha values
cbvAlphaNsteps = TR2000.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaNsteps; 
% step size for alpha changes
cbvAlphaStepSize = TR2000.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaStepSize; 
% max alpha value
cbvAlphaMax = cbvAlphaMin + (cbvAlphaNsteps-1) * cbvAlphaStepSize;
% allow for (ninterval+1) x & y AxisTicks
ninterval = 3;

    FigureProps.minVal = cbvTauMin;
    FigureProps.maxVal = cbvTauMax;   
    FigureProps.xAxisTick = 1:round(cbvTauNsteps/ninterval):cbvTauNsteps; 
    % label xAxisTicks with Tv values
    FigureProps.xAxisTick = cbvTauMin+(SmS+gap)/2:...
           	(SmS+gap)*round(cbvTauNsteps/ninterval):cbvTauNsteps*(SmS+gap);   
    % label xAxisTicks with Tv values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',cbvTauMin + (count-1)*((cbvTauMax-cbvTauMin)/ninterval))
	end
    FigureProps.xAxisLabel = '\tau_v'; 
    FigureProps.yAxisTick = cbvAlphaMin+(SmS+gap)/2:...
    	(SmS+gap)*round(cbvAlphaNsteps/ninterval):cbvAlphaNsteps*(SmS+gap); 
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.3f',cbvAlphaMax - (count-1)*((cbvAlphaMax-cbvAlphaMin)/ninterval))
    end
    FigureProps.yAxisLabel = '\alpha_v'; 

subplot(2,3,3)
    FigureProps.cLabel = 'CC (\tau_v, \alpha_v)';
	FigureProps.minVal = -0.40;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(TR2000.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.maxVal = max(TR2000.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%                      TR2000.S_BOLD.VARcbv(1,1).SimulationPars.SNR0);
    FigureProps.TitleStr = sprintf('(a)                       CC = [%3.2f...%3.2f]                  	          ',...
        min(TR2000.CC.SNRmatrix.RHO_SNR(:)),max(TR2000.CC.SNRmatrix.RHO_SNR(:)));
    newdisp_FC(TR2000.CC.SNRmatrix.RHO_SNR, FigureProps)
    
subplot(2,3,6)
    FigureProps.cLabel = 'p value (\tau_v, \alpha_v)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(TR2000.CC.SNRmatrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(TR2000.CC.SNRmatrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(d)                                                                               ');   
    newdisp_FC(TR2000.CC.SNRmatrix.PVAL_SNR, FigureProps)    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% save plot to file                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FNout = fullfile(ResultPath,sprintf('%s.tif', Label));
saveas(gcf,FNout);     
end

