function generateCombinedCCfigure(TightCoupling_VARm1_VARf1, ...
                                  TightCoupling_VARdelays, ...
                                  TightCoupling_VARcbv, ...
                                  SlowerCBV_VARm1_VARf1, ...
                                  SlowerCBV_VARdelays, ...
                                  SlowerCBV_VARcbv, Label, ResultPath)
% CP 29MAR19 Plot CC arrays
% CP 01OCT19 generate figures for paper
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%   Plot CC array created from BOLD TCs with same SNRs          %%%%%%
%%%%%%  (SNR array size = BOLD_Model.SimulationPars.SNRmatrixSize)   %%%%%%
%%%%%%  but different random number realizations                     %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SNR matrix size --> sqrt(number of different random noise realizations)
% --> create SmS x SmS patches in CC maps with same SNR but different noise
%     realizations
SmS = TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNRmatrixSize;
% Border width between SmS x SmS patches with same SNR
% gap = TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNRgapSize;
gap = TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNRgapSize;

figure('Units','centimeters','Position',[20 0 28 35]); 
    % Global settings %%%%% 
    FigureProps.GlobalTitleStr = '';%sprintf('%s\n\n',Label);
    FigureProps.cFsize = 1.0; % 1.1 = Fontsize 12 pt 
    FigureProps.cLabel = ''; 
    FigureProps.NewColorMap = nawhimar;
%     FigureProps.NewColorMap = gray;
    FigureProps.TitleStr = ''; % title for subplots
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings and plots for TightCoupling_VARm1_VARf1 scenario               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% number of prescribed CMRO2 values
m1Nsteps = TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Nsteps;
% minimum of prescribed CMRO2 values
m1Min = TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Min;
% step size for CMRO2 changes
m1StepSize = TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1StepSize;
% max CBF value
m1Max = m1Min + (m1Nsteps-1) * m1StepSize;
% minimum of prescribed CBF values
f1Min = TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1Min;
% number of prescribed CBF values
f1NSteps = TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1NSteps; 
% step size for CBF changes
f1StepSize = TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1StepSize; 
% max CBF value
f1Max = f1Min + (f1NSteps-1) * f1StepSize;
% allow for (ninterval+1) x & y AxisTicks
ninterval = 3;
    FigureProps.minVal = m1Min;
    FigureProps.maxVal = m1Max;
    FigureProps.xAxisTick = m1Min+(SmS+gap)/2:...
                    (SmS+gap)*round(m1Nsteps/ninterval):m1Nsteps*(SmS+gap);
    % label xAxisTicks with M1 values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',m1Min + (count-1)*((m1Max-m1Min)/ninterval));
	end
    FigureProps.xAxisLabel = 'm1';
    FigureProps.yAxisTick = f1Min+(SmS+gap)/2:...
                    (SmS+gap)*round(m1Nsteps/ninterval):f1NSteps*(SmS+gap); 
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.1f',f1Max - (count-1)*((f1Max-1)/ninterval));
    end
    FigureProps.yAxisLabel = 'f1';        
   
subplot(4,3,1)
    FigureProps.cLabel = 'CC (m1, f1)';
	FigureProps.minVal = -0.4;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(TightCoupling_VARm1_VARf1.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.maxVal = max(TightCoupling_VARm1_VARf1.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%     	TightCoupling_VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNR0); 
    FigureProps.TitleStr = sprintf('(a)         CC = [%3.2f...%3.2f]            ',...
        min(TightCoupling_VARm1_VARf1.CC.SNRmatrix.RHO_SNR(:)),...
        max(TightCoupling_VARm1_VARf1.CC.SNRmatrix.RHO_SNR(:)));
    newdisp_FC(TightCoupling_VARm1_VARf1.CC.SNRmatrix.RHO_SNR, FigureProps)
    
subplot(4,3,4)
    FigureProps.cLabel = 'p value (m1, f1)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(TightCoupling_VARm1_VARf1.CC.SNRmatrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(TightCoupling_VARm1_VARf1.CC.SNRmatrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(d)                                                    ');    
    newdisp_FC(TightCoupling_VARm1_VARf1.CC.SNRmatrix.PVAL_SNR, FigureProps)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings and plots for TightCoupling_VARdelays scenario                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% number of prescribed CMRO2 values
m1TauNsteps = TightCoupling_VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.m1TauNsteps;
% minimum of prescribed CMRO2 values
m1TauMin = TightCoupling_VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.m1TauMin;
% step size for CMRO2 changes
m1TauStepSize = TightCoupling_VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.m1TauStepSize;
% max CBF value
m1TauMax = m1TauMin + (m1TauNsteps-1) * m1TauStepSize;
% minimum of prescribed CBF values
f1TauMin = TightCoupling_VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.f1TauMin;
% number of prescribed CBF values
f1TauNsteps = TightCoupling_VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.f1TauNsteps; 
% step size for CBF changes
f1TauStepSize = TightCoupling_VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.f1TauStepSize; 
% max CBF value
f1TauMax = f1TauMin + (f1TauNsteps-1) * f1TauStepSize;
% allow for (ninterval+1) x & y AxisTicks
ninterval = 5;

    FigureProps.minVal = m1TauMin;
    FigureProps.maxVal = m1TauMax;
    FigureProps.xAxisTick = m1TauMin+(SmS+gap)/2:...
           	(SmS+gap)*round(m1TauNsteps/ninterval):m1TauNsteps*(SmS+gap);
    % label xAxisTicks with Tm values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',m1TauMin + (count-1)*((m1TauMax-m1TauMin)/ninterval));
	end
    FigureProps.xAxisLabel = '\tau_m';
    FigureProps.yAxisTick = f1TauMin+(SmS+gap)/2:...
           	(SmS+gap)*round(m1TauNsteps/ninterval):f1TauNsteps*(SmS+gap); 
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.1f',f1TauMax - (count-1)*((f1TauMax-m1TauMin)/ninterval));
    end
    FigureProps.yAxisLabel = '\tau_f';       
    
subplot(4,3,2)
    FigureProps.cLabel = 'CC (\tau_m, \tau_f)';
	FigureProps.minVal = -0.4;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(TightCoupling_VARdelays.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.maxVal = max(TightCoupling_VARdelays.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%         TightCoupling_VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.SNR0);
    FigureProps.TitleStr = sprintf('(b)         CC = [%3.2f...%3.2f]            ',...
        min(TightCoupling_VARdelays.CC.SNRmatrix.RHO_SNR(:)),...
        max(TightCoupling_VARdelays.CC.SNRmatrix.RHO_SNR(:)));
    newdisp_FC(TightCoupling_VARdelays.CC.SNRmatrix.RHO_SNR, FigureProps)
    
subplot(4,3,5)
    FigureProps.cLabel = 'p value (\tau_m, \tau_f)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(TightCoupling_VARdelays.CC.SNRmatrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(TightCoupling_VARdelays.CC.SNRmatrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(e)                                                    ');   
    newdisp_FC(TightCoupling_VARdelays.CC.SNRmatrix.PVAL_SNR, FigureProps)    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings and plots for TightCoupling_VARcbv scenario                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% number of prescribed CBV temporal delay values
cbvTauNsteps = TightCoupling_VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauNsteps;
% minimum of prescribed CBV temporal delay values
cbvTauMin = TightCoupling_VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauMin;
% step size for CBV temporal delay changes
cbvTauStepSize = TightCoupling_VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauStepSize;
% max CBV temporal delay value
cbvTauMax = cbvTauMin + (cbvTauNsteps-1) * cbvTauStepSize;
% minimum of prescribed alpha values
cbvAlphaMin = TightCoupling_VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaMin;
% number of prescribed alpha values
cbvAlphaNsteps = TightCoupling_VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaNsteps; 
% step size for alpha changes
cbvAlphaStepSize = TightCoupling_VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaStepSize; 
% max alpha value
cbvAlphaMax = cbvAlphaMin + (cbvAlphaNsteps-1) * cbvAlphaStepSize;
% allow for (ninterval+1) x & y AxisTicks
ninterval = 3;

    FigureProps.minVal = cbvTauMin;
    FigureProps.maxVal = cbvTauMax;   
    FigureProps.xAxisTick = 1:round(cbvTauNsteps/ninterval):cbvTauNsteps; 
    % label xAxisTicks with Tv values
    FigureProps.xAxisTick = cbvTauMin+(SmS+gap)/2:...
           	(SmS+gap)*round(cbvTauNsteps/ninterval):cbvTauNsteps*(SmS+gap);   
    % label xAxisTicks with Tv values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',cbvTauMin + (count-1)*((cbvTauMax-cbvTauMin)/ninterval))
	end
    FigureProps.xAxisLabel = '\tau_v'; 
    FigureProps.yAxisTick = cbvAlphaMin+(SmS+gap)/2:...
    	(SmS+gap)*round(cbvAlphaNsteps/ninterval):cbvAlphaNsteps*(SmS+gap); 
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.3f',cbvAlphaMax - (count-1)*((cbvAlphaMax-cbvAlphaMin)/ninterval))
    end
    FigureProps.yAxisLabel = '\alpha_v'; 

subplot(4,3,3)
    FigureProps.cLabel = 'CC (\tau_v, \alpha_v)';
	FigureProps.minVal = -0.4;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(TightCoupling_VARcbv.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.maxVal = max(TightCoupling_VARcbv.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%     	TightCoupling_VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.SNR0);    
    FigureProps.TitleStr = sprintf('(c)         CC = [%3.2f...%3.2f]            ',...
        min(TightCoupling_VARcbv.CC.SNRmatrix.RHO_SNR(:)),...
        max(TightCoupling_VARcbv.CC.SNRmatrix.RHO_SNR(:)));
    newdisp_FC(TightCoupling_VARcbv.CC.SNRmatrix.RHO_SNR, FigureProps)
    
subplot(4,3,6)
    FigureProps.cLabel = 'p value (\tau_v, \alpha_v)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(TightCoupling_VARcbv.CC.SNRmatrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(TightCoupling_VARcbv.CC.SNRmatrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(f)                                                    '); 
    newdisp_FC(TightCoupling_VARcbv.CC.SNRmatrix.PVAL_SNR, FigureProps)
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings and plots for SlowerCBV_VARm1_VARf1 scenario                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% number of prescribed CMRO2 values
m1Nsteps = SlowerCBV_VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Nsteps;
% minimum of prescribed CMRO2 values
m1Min = SlowerCBV_VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Min;
% step size for CMRO2 changes
m1StepSize = SlowerCBV_VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1StepSize;
% max CBF value
m1Max = m1Min + (m1Nsteps-1) * m1StepSize;
% minimum of prescribed CBF values
f1Min = SlowerCBV_VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1Min;
% number of prescribed CBF values
f1NSteps = SlowerCBV_VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1NSteps; 
% step size for CBF changes
f1StepSize = SlowerCBV_VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1StepSize; 
% max CBF value
f1Max = f1Min + (f1NSteps-1) * f1StepSize;
% allow for (ninterval+1) x & y AxisTicks
ninterval = 3;
    FigureProps.minVal = m1Min;
    FigureProps.maxVal = m1Max;
    FigureProps.xAxisTick = m1Min+(SmS+gap)/2:...
                    (SmS+gap)*round(m1Nsteps/ninterval):m1Nsteps*(SmS+gap);
    % label xAxisTicks with M1 values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',m1Min + (count-1)*((m1Max-m1Min)/ninterval));
	end
    FigureProps.xAxisLabel = 'm1';
    FigureProps.yAxisTick = f1Min+(SmS+gap)/2:...
                    (SmS+gap)*round(m1Nsteps/ninterval):f1NSteps*(SmS+gap); 
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.1f',f1Max - (count-1)*((f1Max-1)/ninterval));
    end
    FigureProps.yAxisLabel = 'f1';        
   
subplot(4,3,7)
    FigureProps.cLabel = 'CC (m1, f1)';
	FigureProps.minVal = -0.4;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(SlowerCBV_VARm1_VARf1.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.maxVal = max(SlowerCBV_VARm1_VARf1.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%         SlowerCBV_VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNR0); 
    FigureProps.TitleStr = sprintf('(g)         CC = [%3.2f...%3.2f]            ',...
        min(SlowerCBV_VARm1_VARf1.CC.SNRmatrix.RHO_SNR(:)),...
        max(SlowerCBV_VARm1_VARf1.CC.SNRmatrix.RHO_SNR(:)));
    newdisp_FC(SlowerCBV_VARm1_VARf1.CC.SNRmatrix.RHO_SNR, FigureProps)
    
subplot(4,3,10)
    FigureProps.cLabel = 'p value (m1, f1)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(SlowerCBV_VARm1_VARf1.CC.SNRmatrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(SlowerCBV_VARm1_VARf1.CC.SNRmatrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(j)                                                    ');   
    newdisp_FC(SlowerCBV_VARm1_VARf1.CC.SNRmatrix.PVAL_SNR, FigureProps)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings and plots for SlowerCBV_VARdelays scenario                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% number of prescribed CMRO2 values
m1TauNsteps = SlowerCBV_VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.m1TauNsteps;
% minimum of prescribed CMRO2 values
m1TauMin = SlowerCBV_VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.m1TauMin;
% step size for CMRO2 changes
m1TauStepSize = SlowerCBV_VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.m1TauStepSize;
% max CBF value
m1TauMax = m1TauMin + (m1TauNsteps-1) * m1TauStepSize;
% minimum of prescribed CBF values
f1TauMin = SlowerCBV_VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.f1TauMin;
% number of prescribed CBF values
f1TauNsteps = SlowerCBV_VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.f1TauNsteps; 
% step size for CBF changes
f1TauStepSize = SlowerCBV_VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.f1TauStepSize; 
% max CBF value
f1TauMax = f1TauMin + (f1TauNsteps-1) * f1TauStepSize;
% allow for (ninterval+1) x & y AxisTicks
ninterval = 5;

    FigureProps.minVal = m1TauMin;
    FigureProps.maxVal = m1TauMax;
    FigureProps.xAxisTick = m1TauMin+(SmS+gap)/2:...
           	(SmS+gap)*round(m1TauNsteps/ninterval):m1TauNsteps*(SmS+gap);
    % label xAxisTicks with Tm values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',m1TauMin + (count-1)*((m1TauMax-m1TauMin)/ninterval));
	end
    FigureProps.xAxisLabel = '\tau_m';
    FigureProps.yAxisTick = f1TauMin+(SmS+gap)/2:...
           	(SmS+gap)*round(m1TauNsteps/ninterval):f1TauNsteps*(SmS+gap); 
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.1f',f1TauMax - (count-1)*((f1TauMax-m1TauMin)/ninterval));
    end
    FigureProps.yAxisLabel = '\tau_f';       
    
subplot(4,3,8)
    FigureProps.cLabel = 'CC (\tau_m, \tau_f)';
	FigureProps.minVal = -0.4;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(SlowerCBV_VARdelays.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.maxVal = max(SlowerCBV_VARdelays.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%     	SlowerCBV_VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.SNR0);
    FigureProps.TitleStr = sprintf('(h)         CC = [%3.2f...%3.2f]            ',...
        min(SlowerCBV_VARdelays.CC.SNRmatrix.RHO_SNR(:)),...
        max(SlowerCBV_VARdelays.CC.SNRmatrix.RHO_SNR(:)));
    newdisp_FC(SlowerCBV_VARdelays.CC.SNRmatrix.RHO_SNR, FigureProps)
    
subplot(4,3,11)
    FigureProps.cLabel = 'p value (\tau_m, \tau_f)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(SlowerCBV_VARdelays.CC.SNRmatrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(SlowerCBV_VARdelays.CC.SNRmatrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(k)                                                    ');   
    newdisp_FC(SlowerCBV_VARdelays.CC.SNRmatrix.PVAL_SNR, FigureProps)    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings and plots for SlowerCBV_VARcbv scenario                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% number of prescribed CBV temporal delay values
cbvTauNsteps = SlowerCBV_VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauNsteps;
% minimum of prescribed CBV temporal delay values
cbvTauMin = SlowerCBV_VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauMin;
% step size for CBV temporal delay changes
cbvTauStepSize = SlowerCBV_VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauStepSize;
% max CBV temporal delay value
cbvTauMax = cbvTauMin + (cbvTauNsteps-1) * cbvTauStepSize;
% minimum of prescribed alpha values
cbvAlphaMin = SlowerCBV_VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaMin;
% number of prescribed alpha values
cbvAlphaNsteps = SlowerCBV_VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaNsteps; 
% step size for alpha changes
cbvAlphaStepSize = SlowerCBV_VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaStepSize; 
% max alpha value
cbvAlphaMax = cbvAlphaMin + (cbvAlphaNsteps-1) * cbvAlphaStepSize;
% allow for (ninterval+1) x & y AxisTicks
ninterval = 3;

    FigureProps.minVal = cbvTauMin;
    FigureProps.maxVal = cbvTauMax;   
    FigureProps.xAxisTick = 1:round(cbvTauNsteps/ninterval):cbvTauNsteps; 
    % label xAxisTicks with Tv values
    FigureProps.xAxisTick = cbvTauMin+(SmS+gap)/2:...
           	(SmS+gap)*round(cbvTauNsteps/ninterval):cbvTauNsteps*(SmS+gap);   
    % label xAxisTicks with Tv values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',cbvTauMin + (count-1)*((cbvTauMax-cbvTauMin)/ninterval))
	end
    FigureProps.xAxisLabel = '\tau_v'; 
    FigureProps.yAxisTick = cbvAlphaMin+(SmS+gap)/2:...
    	(SmS+gap)*round(cbvAlphaNsteps/ninterval):cbvAlphaNsteps*(SmS+gap); 
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.3f',cbvAlphaMax - (count-1)*((cbvAlphaMax-cbvAlphaMin)/ninterval))
    end
    FigureProps.yAxisLabel = '\alpha_v'; 

subplot(4,3,9)
    FigureProps.cLabel = 'CC (\tau_v, \alpha_v)';
	FigureProps.minVal = -0.4;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(SlowerCBV_VARcbv.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.maxVal = max(SlowerCBV_VARcbv.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%         SlowerCBV_VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.SNR0);
    FigureProps.TitleStr = sprintf('(i)         CC = [%3.2f...%3.2f]            ',...
        min(SlowerCBV_VARcbv.CC.SNRmatrix.RHO_SNR(:)),...
        max(SlowerCBV_VARcbv.CC.SNRmatrix.RHO_SNR(:)));
    newdisp_FC(SlowerCBV_VARcbv.CC.SNRmatrix.RHO_SNR, FigureProps)
    
subplot(4,3,12)
    FigureProps.cLabel = 'p value (\tau_v, \alpha_v)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(SlowerCBV_VARcbv.CC.SNRmatrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(SlowerCBV_VARcbv.CC.SNRmatrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(l)                                                    ');  
    newdisp_FC(SlowerCBV_VARcbv.CC.SNRmatrix.PVAL_SNR, FigureProps)
    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% save plot to file                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FNout = fullfile(ResultPath,sprintf('%s.tif', Label));
saveas(gcf,FNout);     
end

