function CheckTC(TC1, TC2, TC3, Label, ResultPath)
% TC needs to be a time series object
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot data to check TRs of timecourses                                   %                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure('Units','centimeters','Position',[10 10 15 10]); 
subplot(1,1,1)
plot(TC1,'x')
hold on
plot(TC2,'+')
hold on
plot(TC3,'r.')
hold off
xlabel('time [sec]')
ylabel('\delta S_{BOLD}')
axis([min(TC1.time) max(TC1.time) 0.992 1.018])
title(Label)
if nargin > 1
    FNout = fullfile(ResultPath,sprintf('%s.tif',Label));
    saveas(gcf,FNout);
end
end

