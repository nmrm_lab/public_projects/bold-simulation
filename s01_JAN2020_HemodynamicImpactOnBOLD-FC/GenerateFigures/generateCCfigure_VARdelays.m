function generateCCfigure_VARdelays(VARdelays, Label, ResultPath)
S_BOLD = VARdelays.S_BOLD;
CC = VARdelays.CC;
% CP 29MAR19 Plot CC arrays
% CP 01OCT19 generate figures for paper
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% convenience definitions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% temporal resolution of simulated BOLD signal curves [sec]
SampleTime = S_BOLD.VARdelays(1,1).SimulationPars.SampleTime; 
% simulated time period
totalDur = S_BOLD.VARdelays(1,1).SimulationPars.totalDur; 
% total duration of initial block stimulus
blockDur = S_BOLD.VARdelays(1,1).SimulationPars.blockDur;
% number of prescribed CMRO2 values
m1TauNsteps = S_BOLD.VARdelays(1,1).SimulationPars.m1TauNsteps;
% minimum of prescribed CMRO2 values
m1TauMin = S_BOLD.VARdelays(1,1).SimulationPars.m1TauMin;
% step size for CMRO2 changes
m1TauStepSize = S_BOLD.VARdelays(1,1).SimulationPars.m1TauStepSize;
% max CBF value
m1TauMax = m1TauMin + (m1TauNsteps-1) * m1TauStepSize;
% minimum of prescribed CBF values
f1TauMin = S_BOLD.VARdelays(1,1).SimulationPars.f1TauMin;
% number of prescribed CBF values
f1TauNsteps = S_BOLD.VARdelays(1,1).SimulationPars.f1TauNsteps; 
% step size for CBF changes
f1TauStepSize = S_BOLD.VARdelays(1,1).SimulationPars.f1TauStepSize; 
% max CBF value
f1TauMax = f1TauMin + (f1TauNsteps-1) * f1TauStepSize;

% allow for (ninterval+1) x & y AxisTicks
ninterval = 5;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%   Plot CC arrays created from BOLD TCs with different SNRs    %%%%%%
%%%%%%  (SNR array size = 1)                                         %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure('Units','centimeters','Position',[10 5 40 16]); 
    % Global settings
    FigureProps.GlobalTitleStr = sprintf('%s \n',Label); 
    FigureProps.cFsize = 1.1; % Fontsize 12 pt 
    FigureProps.cLabel = '';
    FigureProps.minVal = m1TauMin;
    FigureProps.maxVal = m1TauMax;    
%     FigureProps.NewColorMap = nawhimar;
    FigureProps.NewColorMap = gray;
    FigureProps.TitleStr = ''; % title for subplots
    FigureProps.xAxisTick = m1TauMin:round(m1TauNsteps/ninterval):m1TauNsteps;
    for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',m1TauMin + (count-1)*((m1TauMax-m1TauMin)/ninterval));
    end
    FigureProps.xAxisLabel = '\tau_m';
    FigureProps.yAxisTick = f1TauMin:round(f1TauNsteps/ninterval):f1TauNsteps;  
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.2f',f1TauMax - (count-1)*((f1TauMax-f1TauMin)/ninterval));
    end
    FigureProps.yAxisLabel = '\tau_f';   

subplot(2,4,1)
    FigureProps.cLabel = ''; 
%     FigureProps.minVal = -0.5;
%     FigureProps.maxVal = 0.5;
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_1000(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_1000(:));
    FigureProps.TitleStr = 'SNR_0 = 1000';      
    newdisp_FC(CC.varSNR.RHO_SNR0_1000, FigureProps)    
subplot(2,4,2)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_500(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_500(:));
    FigureProps.TitleStr = 'SNR_0 = 500'; 
    newdisp_FC(CC.varSNR.RHO_SNR0_500, FigureProps)
subplot(2,4,3)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_250(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_250(:));
    FigureProps.TitleStr = 'SNR_0 = 250'; 
    newdisp_FC(CC.varSNR.RHO_SNR0_250, FigureProps)
subplot(2,4,4)
    FigureProps.cLabel = 'CC'; 
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_125(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_125(:));
    FigureProps.TitleStr = 'SNR_0 = 125';     
    newdisp_FC(CC.varSNR.RHO_SNR0_125, FigureProps)
subplot(2,4,5)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;    
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_1000(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_1000(:));
    FigureProps.TitleStr = '';      
    newdisp_FC(CC.varSNR.PVAL_SNR0_1000, FigureProps)    
subplot(2,4,6)
    FigureProps.cLabel = ''; 
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_500(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_500(:));
    FigureProps.TitleStr = ''; 
    newdisp_FC(CC.varSNR.PVAL_SNR0_500, FigureProps)
subplot(2,4,7)
    FigureProps.cLabel = ''; 
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_250(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_250(:));
    FigureProps.TitleStr = ''; 
    newdisp_FC(CC.varSNR.PVAL_SNR0_250, FigureProps)    
subplot(2,4,8)
    FigureProps.cLabel = 'p value'; 
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_125(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_125(:));
    FigureProps.TitleStr = '';     
    newdisp_FC(CC.varSNR.PVAL_SNR0_125, FigureProps)
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% save plot to file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FNout = fullfile(ResultPath,sprintf('%s_varSNR.tif',Label));
saveas(gcf,FNout);      
   
end

