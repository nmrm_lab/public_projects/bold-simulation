function newdisp_FC(vol, FigureProps)
% displays 2D or 3D images 
% CP 28MAR2019 modified for display of correation matrices

% vol:      2D or 3D matrix containing the image(s)
% cFsize:   scaling factor for font size in color bar
%           (if 0 or not specified: no color bar)
% cLabel:   label for color bar (string)
% minVal:      minimum value displayed (if not specified: 0)
% maxVal:      maximum value displayed (if not specified: max val of vol)
% NewColorMap: color map (default gray) 
% title:        figure title (string)

% set NaNs to zero
vol(isnan(vol)) = 0;
vol(isinf(vol)) = 0;

if nargin == 0
    error('Input image (2D or 3D) for display required!')
end

if nargin > 1
    GlobalTitleStr = FigureProps.GlobalTitleStr;
    cFsize = FigureProps.cFsize;
    cLabel = FigureProps.cLabel;
    minVal = FigureProps.minVal;
    maxVal = FigureProps.maxVal;
    FigColorMap = FigureProps.NewColorMap;
    TitleStr = FigureProps.TitleStr;
    xAxisTick = FigureProps.xAxisTick;
    xAxisTickLabel = FigureProps.xAxisTickLabel;
    xAxisLabel = FigureProps.xAxisLabel;
    yAxisTick = FigureProps.yAxisTick;
    yAxisTickLabel = FigureProps.yAxisTickLabel; 
    yAxisLabel = FigureProps.yAxisLabel;
else
    GlobalTitleStr = ''; % no global figure title
    cFsize = 0;  % no scale on colorbar
    cLabel = ''; % no label on colorbar
    minVal = 0;  % minimum value displayed
    maxVal = max(vol(:)); % maximum value displayed
    FigColorMap = gray; % default color map 
end
    
if strcmp(GlobalTitleStr,'') == 0
    subtitle(GlobalTitleStr);
end

% Check for dimensions and reorder if necessary
dimens  = ndims(vol);
if dimens==2   
    bild = rot90(vol);
    maximum = max(bild(:));
elseif dimens==3
    [x, y, z] = size(vol);
    numx    = ceil(sqrt(z));
    numy    = ceil(z/numx);
    bild    = zeros(numy*y,numx*x);
    for nz=1:z
        col     = 1+x*mod((nz-1),numx);
        lin     = 1+y*floor((nz-1)/numx);
        bild(lin:lin+y-1,col:col+x-1) = rot90(vol(:,:,nz));
    end
    maximum = max(bild(:));
end

% Display image
imagesc(bild, [minVal, maxVal]);
if nargin > 1
    ax = gca;
    ax.Title.String = TitleStr;
    ax.Title.FontSize = 11;
    ax.XTick = xAxisTick;
    ax.XTickLabel = xAxisTickLabel;
    ax.XLabel.String = xAxisLabel;
    ax.YTick = yAxisTick;
    ax.YTickLabel = yAxisTickLabel;
    ax.YLabel.String = yAxisLabel;
end

% set properties of color map
set(gca,'Visible','on','DataAspectRatio',[1 1 1]);
colormap(FigColorMap);
if cFsize~=0
    c = colorbar;
    c.Label.String = cLabel;
    c.FontSize     = cFsize*10;
end
end

% subtitle function allow only one title above whole Figur:
function hout = subtitle(str)
% SUBTITLE Puts a title above all subplots.
%	SUBTITLE('text') adds text to the top of the figure
%	above all subplots (a "super title"). Use this function
%	after all subplot commands.

% Drea Thomas 6/15/95 drea@mathworks.com

% Warning: If the figure or axis units are non-default, this
% will break.

% Parameters used to position the supertitle.

% Amount of the figure window devoted to subplots
plotregion = .97;

% Y position of title in normalized coordinates
titleypos  = .95;

% Fontsize for supertitle
fs         = get(gcf,'defaultaxesfontsize')+2;

% Fudge factor to adjust y spacing between subplots
fudge = 1;

haold    = gca;
figunits = get(gcf,'units');

% Get the (approximate) difference between full height (plot + title
% + xlabel) and bounding rectangle.

if (~strcmp(figunits,'pixels')),
    set(gcf,'units','pixels');
    pos = get(gcf,'position');
    set(gcf,'units',figunits);
else
    pos = get(gcf,'position');
end
ff = (fs-4)*1.27*5/pos(4)*fudge;

% The 5 here reflects about 3 characters of height below
% an axis and 2 above. 1.27 is pixels per point.

% Determine the bounding rectange for all the plots

% h = findobj('Type','axes');

% findobj is a 4.2 thing.. if you don't have 4.2 comment out
% the next line and uncomment the following block.

%h = findobj(gcf,'Type','axes');  % Change suggested by Stacy J. Hills

% If you don't have 4.2, use this code instead
ch = get(gcf,'children');
h  = [];
for i=1:length(ch),
    if strcmp(get(ch(i),'type'),'axes'),
        h = [h,ch(i)];
    end
end

max_y=0;
min_y=1;

oldtitle =0;
for i=1:length(h),
    if (~strcmp(get(h(i),'Tag'),'subtitle')),
        pos=get(h(i),'pos');
        if (pos(2) < min_y), min_y=pos(2)-ff/5*3;end;
        if (pos(4)+pos(2) > max_y), max_y=pos(4)+pos(2)+ff/5*2;end;
    else
        oldtitle = h(i);
    end
end

if max_y > plotregion,
    scale = (plotregion-min_y)/(max_y-min_y);
    for i=1:length(h),
        pos = get(h(i),'position');
        pos(2) = (pos(2)-min_y)*scale+min_y;
        pos(4) = pos(4)*scale-(1-scale)*ff/5*3;
        set(h(i),'position',pos);
    end
end

np = get(gcf,'nextplot');
set(gcf,'nextplot','add');
if oldtitle ~= 0 % SK16JAN17 to prevent errors with Matlab 2016a
    delete(oldtitle);
end
ha=axes('pos',[0 1 1 1],'visible','off','Tag','subtitle');
ht=text(0.48,titleypos-1,str);set(ht,'horizontalalignment','center','fontsize',fs);
set(gcf,'nextplot',np);
axes(haold);
if nargout
    hout=ht;
end
end
