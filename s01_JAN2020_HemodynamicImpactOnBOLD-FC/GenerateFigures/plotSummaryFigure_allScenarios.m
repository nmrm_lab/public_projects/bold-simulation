function plotSummaryFigure_allScenarios(VARm1_VARf1,VARdelays,VARcbv,...
                                        Label, ResultPath)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VARm1_VARf1: Plot summary of simulation results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define local variables (for convenience)
m1Nsteps = VARm1_VARf1.BOLD_Model(1,1).SimulationPars.m1Nsteps;
f1NSteps = VARm1_VARf1.BOLD_Model(1,1).SimulationPars.f1NSteps;
m1StepSize = VARm1_VARf1.BOLD_Model(1,1).SimulationPars.m1StepSize;
f1StepSize = VARm1_VARf1.BOLD_Model(1,1).SimulationPars.f1StepSize;
SNR0 = VARm1_VARf1.BOLD_Model(1,1).SimulationPars.SNR0;
totaldur = VARm1_VARf1.BOLD_Model(1,1).SimulationPars.totalDur;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot all curves for selected f1 values
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure('Units','centimeters','Position',[15 0 35 35]); 

colormap('inferno')
set(0,'DefaultAxesColorOrder',viridis(m1Nsteps))

plotNr = 1;
subplot(3,4,plotNr)
plot(VARm1_VARf1.S_BOLD.reference.BOLD.bout,'b','LineWidth',2)
axis([0 totaldur -3 4])
title(''); 
pars_text = sprintf('m1 = %3.2f, f1 = %3.2f',...
                    VARm1_VARf1.S_BOLD.reference.CMRO2.amplitude, ....
                    VARm1_VARf1.S_BOLD.reference.CBF.amplitude);
text(50, 0.977,pars_text,'FontSize',11)
n_text_str = sprintf('n = %3.1f',...
	VARm1_VARf1.S_BOLD.reference.n.Prescribed);
text(50, 0.97,n_text_str,'FontSize',11)
axis([0 totaldur 0.965 1.035])
legend1 = sprintf('= %1.2f',VARm1_VARf1.S_BOLD.reference.CBF.amplitude);
legend2 = sprintf('= %1.1f',VARm1_VARf1.S_BOLD.reference.CBF.tau);
legend3 = sprintf('= %1.2f',VARm1_VARf1.S_BOLD.reference.CMRO2.amplitude);
legend4 = sprintf('= %1.1f',VARm1_VARf1.S_BOLD.reference.CMRO2.tau);
% legend5 = sprintf('= %1.1f',...
%                       (max(VARm1_VARf1.S_BOLD.reference.CBF.fin)-1)/...
%                       (max(VARm1_VARf1.S_BOLD.reference.CMRO2.min)-1));
legend6 = sprintf('= %1.3f',VARm1_VARf1.S_BOLD.reference.CBV.alpha);
legend7 = sprintf('= %1.1f',VARm1_VARf1.S_BOLD.reference.CBV.tau);

text(-130,1.035,'(a)','Fontsize',14);
text(50,1.03,'f1','Fontsize',10);
text(90,1.03,legend1,'Fontsize',10);
text(50,1.025,'\tau_f','Fontsize',10);
text(90,1.025,legend2,'Fontsize',10);
text(50,1.02,'m1','Fontsize',10);
text(90,1.02,legend3,'Fontsize',10);
text(50,1.015,'\tau_m','Fontsize',10);
text(90,1.015,legend4,'Fontsize',10);
% text(50,1.01,'n','Fontsize',10);
% text(90,1.01,legend5,'Fontsize',10);
text(230,1.03,'\alpha_v','Fontsize',10);
text(260,1.03,legend6,'Fontsize',10);
text(230,1.025,'\tau_v','Fontsize',10);
text(260,1.025,legend7,'Fontsize',10);
ylabel('S_{BOLD} / S_0')
xlabel('time [sec]')                
ax = gca;
ax.FontSize = 11;

for f1Val = [1 6 16]
%     if mod(f1Val,2) == 0
        plotNr = plotNr + 1;
        subplot(3,4,plotNr)  
        for m1Val=1:m1Nsteps
            plot(VARm1_VARf1.S_BOLD.VARm1_VARf1(m1Val,f1Val).BOLD.bout,'LineWidth',2)
            hold on
        end %for m1Val
        axis([0 totaldur 0.965 1.035])
        title(''); 
        if VARm1_VARf1.S_BOLD.VARm1_VARf1(1,f1Val).CBF.amplitude ...
                == VARm1_VARf1.S_BOLD.VARm1_VARf1(m1Nsteps,f1Val).CBF.amplitude
            pars_text = sprintf('m1 = [%3.1f-%3.1f] @f1 = %3.1f',...
                VARm1_VARf1.S_BOLD.VARm1_VARf1(1,f1Val).CMRO2.amplitude, ....
                VARm1_VARf1.S_BOLD.VARm1_VARf1(m1Nsteps,f1Val).CMRO2.amplitude,...
                VARm1_VARf1.S_BOLD.VARm1_VARf1(m1Nsteps,f1Val).CBF.amplitude);        
        else
            pars_text = sprintf('m1 = [%3.1f-%3.1f] @f1 = [%3.2f-%3.1f]',...
                VARm1_VARf1.S_BOLD.VARm1_VARf1(1,f1Val).CMRO2.amplitude, ....
                VARm1_VARf1.S_BOLD.VARm1_VARf1(m1Nsteps,f1Val).CMRO2.amplitude,...
                VARm1_VARf1.S_BOLD.VARm1_VARf1(1,f1Val).CBF.amplitude, ...
                VARm1_VARf1.S_BOLD.VARm1_VARf1(m1Nsteps,f1Val).CBF.amplitude);
        end
        text(50, 0.977,pars_text,'FontSize',11)
        n_text_str = sprintf('n = [%3.1f, %3.1f, %3.1f,...%3.1f]', ...
        	VARm1_VARf1.S_BOLD.VARm1_VARf1(1, f1Val).n.Actual, ...
            VARm1_VARf1.S_BOLD.VARm1_VARf1(2, f1Val).n.Actual, ...
        	VARm1_VARf1.S_BOLD.VARm1_VARf1(3, f1Val).n.Actual, ...
            VARm1_VARf1.S_BOLD.VARm1_VARf1(m1Nsteps,f1Val).n.Actual);
        text(50, 0.97,n_text_str,'FontSize',11)
        switch plotNr
            case 2
                text(-130,1.035,'(b)','Fontsize',14);
            case 3
                text(-130,1.035,'(c)','Fontsize',14);
            case 4
                text(-130,1.035,'(d)','Fontsize',14);
        end
        ylabel('S_{BOLD} / S_0')
        xlabel('time [sec]') 
        ax = gca;
        ax.FontSize = 11;
        hold off
%     end %if mod(f1Val,2) == 0
end %for f1Val

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VARdelays: Plot summary of simulation results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define local variables (for convenience)
m1TauNsteps = VARdelays.BOLD_Model(1,1).SimulationPars.m1TauNsteps;
f1TauNsteps = VARdelays.BOLD_Model(1,1).SimulationPars.f1TauNsteps;
m1TauStepSize = VARdelays.BOLD_Model(1,1).SimulationPars.m1TauStepSize;
f1TauStepSize = VARdelays.BOLD_Model(1,1).SimulationPars.f1TauStepSize;
SNR0 = VARdelays.BOLD_Model(1,1).SimulationPars.SNR0;
totaldur = VARdelays.BOLD_Model(1,1).SimulationPars.totalDur;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot all curves for selected f1 values
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
colormap('inferno')
set(0,'DefaultAxesColorOrder',viridis(m1TauNsteps))

plotNr = 5;
subplot(3,4,plotNr)
plot(VARdelays.S_BOLD.reference.BOLD.bout,'b','LineWidth',2)
axis([0 totaldur -3 4])
title(''); 
pars_text = sprintf('     = %3.1f,     = %3.1f',...
                    VARdelays.S_BOLD.reference.CMRO2.tau, ....
                    VARdelays.S_BOLD.reference.CBF.tau);
text(50,0.977,'\tau_m','Fontsize',11);
text(180,0.977,'\tau_f','Fontsize',11);
text(50, 0.977,pars_text,'FontSize',11)                
n_text_str = sprintf('n = %3.1f',...
	VARdelays.S_BOLD.reference.n.Actual);
text(50, 0.97,n_text_str,'FontSize',11)
axis([0 totaldur 0.965 1.035])
legend1 = sprintf('= %1.2f',VARdelays.S_BOLD.reference.CBF.amplitude);
legend2 = sprintf('= %1.1f',VARdelays.S_BOLD.reference.CBF.tau);
legend3 = sprintf('= %1.2f',VARdelays.S_BOLD.reference.CMRO2.amplitude);
legend4 = sprintf('= %1.1f',VARdelays.S_BOLD.reference.CMRO2.tau);
% legend5 = sprintf('= %1.1f',...
%                         (max(VARdelays.S_BOLD.reference.CBF.fin)-1)/...
%                         (max(VARdelays.S_BOLD.reference.CMRO2.min)-1));
legend6 = sprintf('= %1.3f',VARdelays.S_BOLD.reference.CBV.alpha);
legend7 = sprintf('= %1.1f',VARdelays.S_BOLD.reference.CBV.tau);

text(-130,1.035,'(e)','Fontsize',14);
text(50,1.03,'f1','Fontsize',10);
text(90,1.03,legend1,'Fontsize',10);
text(50,1.025,'\tau_f','Fontsize',10);
text(90,1.025,legend2,'Fontsize',10);
text(50,1.02,'m1','Fontsize',10);
text(90,1.02,legend3,'Fontsize',10);
text(50,1.015,'\tau_m','Fontsize',10);
text(90,1.015,legend4,'Fontsize',10);
% text(50,1.01,'n','Fontsize',10);
% text(90,1.01,legend5,'Fontsize',10);
text(230,1.03,'\alpha_v','Fontsize',10);
text(260,1.03,legend6,'Fontsize',10);
text(230,1.025,'\tau_v','Fontsize',10);
text(260,1.025,legend7,'Fontsize',10);
ylabel('S_{BOLD} / S_0')
xlabel('time [sec]')                
ax = gca;
ax.FontSize = 11;

for f1Val = [1 8 16]
%     if mod(f1Val,2) == 0
        plotNr = plotNr + 1;
        subplot(3,4,plotNr)  
        for m1Val=1:m1TauNsteps
            plot(VARdelays.S_BOLD.VARdelays(m1Val,f1Val).BOLD.bout,'LineWidth',2)
            hold on
        end %for m1Val
        axis([0 totaldur 0.965 1.035])
        title(''); 
        if VARdelays.S_BOLD.VARdelays(1,f1Val).CBF.tau ...
                == VARdelays.S_BOLD.VARdelays(m1TauNsteps,f1Val).CBF.tau
            pars_text = sprintf('     = [%3.1f-%3.1f] @    = %3.1f',...
                VARdelays.S_BOLD.VARdelays(1,f1Val).CMRO2.tau, ....
                VARdelays.S_BOLD.VARdelays(m1TauNsteps,f1Val).CMRO2.tau,...
                VARdelays.S_BOLD.VARdelays(m1TauNsteps,f1Val).CBF.tau);        
        else
            pars_text = sprintf('     = [%3.1f-%3.1f] @    = [%3.1f-%3.1f]',...
                VARdelays.S_BOLD.VARdelays(1,f1Val).CMRO2.tau, ....
                VARdelays.S_BOLD.VARdelays(m1TauNsteps,f1Val).CMRO2.tau,...
                VARdelays.S_BOLD.VARdelays(1,f1Val).CBF.tau, ...
                VARdelays.S_BOLD.VARdelays(m1TauNsteps,f1Val).CBF.tau);
        end
        text(50,0.977,'\tau_m','Fontsize',11);
        text(270,0.977,'\tau_f','Fontsize',11);
        text(50, 0.977,pars_text,'FontSize',11)
    
        n_text_str = sprintf('n = [%3.1f, %3.1f, %3.1f,...%3.1f]', ...
        	VARdelays.S_BOLD.VARdelays(1, f1Val).n.Actual, ...
            VARdelays.S_BOLD.VARdelays(2, f1Val).n.Actual, ...
        	VARdelays.S_BOLD.VARdelays(3, f1Val).n.Actual, ...
            VARdelays.S_BOLD.VARdelays(m1TauNsteps,f1Val).n.Actual);
        text(50, 0.97,n_text_str,'FontSize',11)
        switch plotNr
            case 6
                text(-130,1.035,'(f)','Fontsize',14);
            case 7
                text(-130,1.035,'(g)','Fontsize',14);
            case 8
                text(-130,1.035,'(h)','Fontsize',14);
        end        
        ylabel('S_{BOLD} / S_0')
        xlabel('time [sec]') 
        ax = gca;
        ax.FontSize = 11;
        hold off
%     end %if mod(f1Val,2) == 0
end %for f1Val

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VARcbv: Plot summary of simulation results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define local variables (for convenience)
cbvTauNsteps = VARcbv.BOLD_Model(1,1).SimulationPars.cbvTauNsteps;
cbvAlphaNsteps = VARcbv.BOLD_Model(1,1).SimulationPars.cbvAlphaNsteps;
cbvTauStepSize = VARcbv.BOLD_Model(1,1).SimulationPars.cbvTauStepSize;
cbvAlphaStepSize = VARcbv.BOLD_Model(1,1).SimulationPars.cbvAlphaStepSize;
SNR0 = VARcbv.BOLD_Model(1,1).SimulationPars.SNR0;
totaldur = VARcbv.BOLD_Model(1,1).SimulationPars.totalDur;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot all curves without noise
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
colormap('inferno')
set(0,'DefaultAxesColorOrder',viridis(cbvTauNsteps))

plotNr = 9;
subplot(3,4,plotNr)
plot(VARcbv.S_BOLD.reference.BOLD.bout,'b','LineWidth',2)
axis([0 totaldur -3 4])
title(''); 
pars_text = sprintf('     = %3.1f,      = %3.1f',...
                    VARcbv.S_BOLD.reference.CBV.alpha, ....
                    VARcbv.S_BOLD.reference.CBV.tau);
text(50,0.977,'\alpha_v','Fontsize',11);
text(180,0.977,'\tau_v','Fontsize',11);                
text(50, 0.977,pars_text,'FontSize',11)             
n_text_str = sprintf('n = %3.1f',...
	VARcbv.S_BOLD.reference.n.Actual);
text(50, 0.97,n_text_str,'FontSize',11)
axis([0 totaldur 0.965 1.035])
legend1 = sprintf('= %1.2f',VARcbv.S_BOLD.reference.CBF.amplitude);
legend2 = sprintf('= %1.1f',VARcbv.S_BOLD.reference.CBF.tau);
legend3 = sprintf('= %1.2f',VARcbv.S_BOLD.reference.CMRO2.amplitude);
legend4 = sprintf('= %1.1f',VARcbv.S_BOLD.reference.CMRO2.tau);
% legend5 = sprintf('= %1.1f',...
%                              (max(VARcbv.S_BOLD.reference.CBF.fin)-1)/...
%                             (max(VARcbv.S_BOLD.reference.CMRO2.min)-1));
legend6 = sprintf('= %1.3f',VARcbv.S_BOLD.reference.CBV.alpha);
legend7 = sprintf('= %1.1f',VARcbv.S_BOLD.reference.CBV.tau);

text(-130,1.035,'(i)','Fontsize',14);
text(50,1.03,'f1','Fontsize',10);
text(90,1.03,legend1,'Fontsize',10);
text(50,1.025,'\tau_f','Fontsize',10);
text(90,1.025,legend2,'Fontsize',10);
text(50,1.02,'m1','Fontsize',10);
text(90,1.02,legend3,'Fontsize',10);
text(50,1.015,'\tau_m','Fontsize',10);
text(90,1.015,legend4,'Fontsize',10);
% text(50,1.01,'n','Fontsize',10);
% text(90,1.01,legend5,'Fontsize',10);
text(230,1.03,'\alpha_v','Fontsize',10);
text(260,1.03,legend6,'Fontsize',10);
text(230,1.025,'\tau_v','Fontsize',10);
text(260,1.025,legend7,'Fontsize',10);
ylabel('S_{BOLD} / S_0')
xlabel('time [sec]')                
ax = gca;
ax.FontSize = 11;

for f1Val = [1 8 16]
	plotNr = plotNr + 1;
	subplot(3,4,plotNr)    
	for m1Val=1:cbvAlphaNsteps
        plot(VARcbv.S_BOLD.VARcbv(m1Val,f1Val).BOLD.bout,'LineWidth',2)
        hold on
	end %for m1Val
	axis([0 totaldur 0.965 1.035])
	title('');  
    if VARcbv.S_BOLD.VARcbv(1,f1Val).CBV.alpha == VARcbv.S_BOLD.VARcbv(cbvTauNsteps,f1Val).CBV.alpha
        pars_text = sprintf('    = [%3.1f-%3.1f] @     = %1.3f',...        	
            VARcbv.S_BOLD.VARcbv(1,f1Val).CBV.tau, ....
          	VARcbv.S_BOLD.VARcbv(cbvTauNsteps,f1Val).CBV.tau,...
            VARcbv.S_BOLD.VARcbv(1,f1Val).CBV.alpha);       
	else
        pars_text = sprintf('    = [%3.1f-%3.1f] @     = [%3.1f-%3.1f] ',...
            VARcbv.S_BOLD.VARcbv(1,f1Val).CBV.tau, ...
         	VARcbv.S_BOLD.VARcbv(cbvTauNsteps,f1Val).CBV.tau,...
            VARcbv.S_BOLD.VARcbv(1,f1Val).CBV.alpha, ....
          	VARcbv.S_BOLD.VARcbv(cbvTauNsteps,f1Val).CBV.alpha);
	end
    text(20,0.977,'\tau_v','Fontsize',11);
    text(250,0.977,'\alpha_v','Fontsize',11);
    text(20, 0.977,pars_text,'FontSize',11)      
	n_text_str = sprintf('n = [%3.1f, %3.1f, %3.1f,...%3.1f]', ...
                VARcbv.S_BOLD.VARcbv(1, f1Val).n.Actual, ...
                VARcbv.S_BOLD.VARcbv(2, f1Val).n.Actual, ...
                VARcbv.S_BOLD.VARcbv(3, f1Val).n.Actual, ...
                VARcbv.S_BOLD.VARcbv(cbvTauNsteps,f1Val).n.Actual);
	text(20, 0.97,n_text_str,'FontSize',11)
	switch plotNr
    	case 10
        	text(-130,1.035,'(j)','Fontsize',14);
    	case 11
        	text(-130,1.035,'(k)','Fontsize',14);
    	case 12
        	text(-130,1.035,'(l)','Fontsize',14);
	end
	ylabel('S_{BOLD} / S_0')
	xlabel('time [sec]') 
	hold off
    ax = gca;
    ax.FontSize = 11;
end %for f1Val


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% save plot to file                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin > 1
    FNout = fullfile(ResultPath,sprintf('%s.tif',Label));
    saveas(gcf,FNout);
end

end

