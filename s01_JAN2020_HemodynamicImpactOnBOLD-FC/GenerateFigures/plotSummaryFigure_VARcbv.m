function plotSummaryFigure_VARcbv(BOLD_Model,Label, ResultPath)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot summary of simulation results

% Define local variables (for convenience)
cbvTauNsteps = BOLD_Model.BOLD_Model(1,1).SimulationPars.cbvTauNsteps;
cbvAlphaNsteps = BOLD_Model.BOLD_Model(1,1).SimulationPars.cbvAlphaNsteps;
cbvTauStepSize = BOLD_Model.BOLD_Model(1,1).SimulationPars.cbvTauStepSize;
cbvAlphaStepSize = BOLD_Model.BOLD_Model(1,1).SimulationPars.cbvAlphaStepSize;
SNR0 = BOLD_Model.BOLD_Model(1,1).SimulationPars.SNR0;
totaldur = BOLD_Model.BOLD_Model(1,1).SimulationPars.totalDur;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot all curves without noise
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure('Units','centimeters','Position',[10 10 35 12]); 

colormap('inferno')
set(0,'DefaultAxesColorOrder',viridis(cbvTauNsteps))

plotNr = 1;
subplot(1,4,plotNr)
plot(BOLD_Model.S_BOLD.reference.BOLD.bout,'b','LineWidth',2)
axis([0 totaldur -3 4])
title_str = sprintf('Reference [alpha = %3.1f, T_v = %3.1f]',...
                    BOLD_Model.S_BOLD.reference.CBV.alpha, ....
                    BOLD_Model.S_BOLD.reference.CBV.tau);
title(title_str); 
n_text_str = sprintf('n = %3.1f',...
	BOLD_Model.S_BOLD.reference.n.Actual);
text(50, 0.97,n_text_str,'FontSize',11)
text(-50, 1.04,Label,'FontSize',11)
axis([0 totaldur 0.965 1.035])
legend1 = sprintf('= %1.2f',BOLD_Model.S_BOLD.reference.CBF.amplitude);
legend2 = sprintf('= %1.1f',BOLD_Model.S_BOLD.reference.CBF.tau);
legend3 = sprintf('= %1.2f',BOLD_Model.S_BOLD.reference.CMRO2.amplitude);
legend4 = sprintf('= %1.1f',BOLD_Model.S_BOLD.reference.CMRO2.tau);
legend5 = sprintf('= %1.1f',...
        (max(BOLD_Model.S_BOLD.reference.CBF.fin)-1)/...
        (max(BOLD_Model.S_BOLD.reference.CMRO2.min)-1));
legend6 = sprintf('= %1.3f',BOLD_Model.S_BOLD.reference.CBV.alpha);
legend7 = sprintf('= %1.1f',BOLD_Model.S_BOLD.reference.CBV.tau);

text(50,1.03,'f1','Fontsize',11);
text(90,1.03,legend1,'Fontsize',11);
text(50,1.0265,'\tau_f','Fontsize',11);
text(90,1.0265,legend2,'Fontsize',11);
text(50,1.022,'m1','Fontsize',11);
text(90,1.022,legend3,'Fontsize',11);
text(50,1.0185,'\tau_m','Fontsize',11);
text(90,1.0185,legend4,'Fontsize',11);
text(50,1.015,'n','Fontsize',11);
text(90,1.015,legend5,'Fontsize',11);
text(50,1.0105,'\alpha','Fontsize',11);
text(90,1.0105,legend6,'Fontsize',11);
text(50,1.007,'\tau_v','Fontsize',11);
text(90,1.007,legend7,'Fontsize',11);
ylabel('S_{BOLD} / S_0')
xlabel('time [sec]')                
ax = gca;
ax.FontSize = 11;

for f1Val = [1 8 16]
	plotNr = plotNr + 1;
	subplot(1,4,plotNr)    
	for m1Val=1:cbvAlphaNsteps
        plot(BOLD_Model.S_BOLD.VARcbv(m1Val,f1Val).BOLD.bout,'LineWidth',2)
        hold on
	end %for m1Val
	if BOLD_Model.S_BOLD.VARcbv(1,f1Val).CBV.alpha == BOLD_Model.S_BOLD.VARcbv(cbvTauNsteps,f1Val).CBV.alpha
        title_str = sprintf('T_v = [%3.1f-%3.1f] @alpha = %1.3f',...        	
            BOLD_Model.S_BOLD.VARcbv(1,f1Val).CBV.tau, ....
          	BOLD_Model.S_BOLD.VARcbv(cbvTauNsteps,f1Val).CBV.tau,...
            BOLD_Model.S_BOLD.VARcbv(1,f1Val).CBV.alpha);       
	else
        title_str = sprintf('T_v = [%3.1f-%3.1f] @alpha = [%3.1f-%3.1f] ',...
            BOLD_Model.S_BOLD.VARcbv(1,f1Val).CBV.tau, ...
         	BOLD_Model.S_BOLD.VARcbv(cbvTauNsteps,f1Val).CBV.tau,...
            BOLD_Model.S_BOLD.VARcbv(1,f1Val).CBV.alpha, ....
          	BOLD_Model.S_BOLD.VARcbv(cbvTauNsteps,f1Val).CBV.alpha);
	end
	title(title_str);     
	n_text_str = sprintf('n = [%3.1f, %3.1f, %3.1f,...%3.1f]', ...
                BOLD_Model.S_BOLD.VARcbv(1, f1Val).n.Actual, ...
                BOLD_Model.S_BOLD.VARcbv(2, f1Val).n.Actual, ...
                BOLD_Model.S_BOLD.VARcbv(3, f1Val).n.Actual, ...
                BOLD_Model.S_BOLD.VARcbv(cbvTauNsteps,f1Val).n.Actual);
	text(50, 0.97,n_text_str,'FontSize',11)
	axis([0 totaldur 0.965 1.035])
	ylabel('S_{BOLD} / S_0')
	xlabel('time [sec]') 
	hold off
    ax = gca;
    ax.FontSize = 11;
end %for f1Val

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% save plot to file                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin > 1
    FNout = fullfile(ResultPath,sprintf('%s.tif',Label));
    saveas(gcf,FNout);
end

end

