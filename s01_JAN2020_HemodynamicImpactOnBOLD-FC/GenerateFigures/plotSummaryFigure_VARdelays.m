function plotSummaryFigure_VARdelays(BOLD_Model,Label, ResultPath)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot summary of simulation results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define local variables (for convenience)
m1TauNsteps = BOLD_Model.BOLD_Model(1,1).SimulationPars.m1TauNsteps;
f1TauNsteps = BOLD_Model.BOLD_Model(1,1).SimulationPars.f1TauNsteps;
m1TauStepSize = BOLD_Model.BOLD_Model(1,1).SimulationPars.m1TauStepSize;
f1TauStepSize = BOLD_Model.BOLD_Model(1,1).SimulationPars.f1TauStepSize;
SNR0 = BOLD_Model.BOLD_Model(1,1).SimulationPars.SNR0;
totaldur = BOLD_Model.BOLD_Model(1,1).SimulationPars.totalDur;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot all curves for selected f1 values
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure('Units','centimeters','Position',[10 10 35 12]); 

colormap('inferno')
set(0,'DefaultAxesColorOrder',viridis(m1TauNsteps))

plotNr = 1;
subplot(1,4,plotNr)
plot(BOLD_Model.S_BOLD.reference.BOLD.bout,'b','LineWidth',2)
axis([0 totaldur -3 4])
title_str = sprintf('Reference [T_m = %3.1f, T_f = %3.1f]',...
                    BOLD_Model.S_BOLD.reference.CMRO2.tau, ....
                    BOLD_Model.S_BOLD.reference.CBF.tau);
title(title_str); 
n_text_str = sprintf('n = %3.1f',...
	BOLD_Model.S_BOLD.reference.n.Actual);
text(50, 0.97,n_text_str,'FontSize',11)
text(-50, 1.04,Label,'FontSize',11)
axis([0 totaldur 0.965 1.035])
legend1 = sprintf('= %1.2f',BOLD_Model.S_BOLD.reference.CBF.amplitude);
legend2 = sprintf('= %1.1f',BOLD_Model.S_BOLD.reference.CBF.tau);
legend3 = sprintf('= %1.2f',BOLD_Model.S_BOLD.reference.CMRO2.amplitude);
legend4 = sprintf('= %1.1f',BOLD_Model.S_BOLD.reference.CMRO2.tau);
legend5 = sprintf('= %1.1f',...
        (max(BOLD_Model.S_BOLD.reference.CBF.fin)-1)/...
        (max(BOLD_Model.S_BOLD.reference.CMRO2.min)-1));
legend6 = sprintf('= %1.3f',BOLD_Model.S_BOLD.reference.CBV.alpha);
legend7 = sprintf('= %1.1f',BOLD_Model.S_BOLD.reference.CBV.tau);

text(50,1.03,'f1','Fontsize',11);
text(90,1.03,legend1,'Fontsize',11);
text(50,1.0265,'\tau_f','Fontsize',11);
text(90,1.0265,legend2,'Fontsize',11);
text(50,1.022,'m1','Fontsize',11);
text(90,1.022,legend3,'Fontsize',11);
text(50,1.0185,'\tau_m','Fontsize',11);
text(90,1.0185,legend4,'Fontsize',11);
text(50,1.015,'n','Fontsize',11);
text(90,1.015,legend5,'Fontsize',11);
text(50,1.0105,'\alpha','Fontsize',11);
text(90,1.0105,legend6,'Fontsize',11);
text(50,1.007,'\tau_v','Fontsize',11);
text(90,1.007,legend7,'Fontsize',11);
ylabel('S_{BOLD} / S_0')
xlabel('time [sec]')                
ax = gca;
ax.FontSize = 11;

for f1Val = [1 8 16]
%     if mod(f1Val,2) == 0
        plotNr = plotNr + 1;
        subplot(1,4,plotNr)  
        for m1Val=1:m1TauNsteps
            plot(BOLD_Model.S_BOLD.VARdelays(m1Val,f1Val).BOLD.bout,'LineWidth',2)
            hold on
        end %for m1Val
        if BOLD_Model.S_BOLD.VARdelays(1,f1Val).CBF.tau ...
                == BOLD_Model.S_BOLD.VARdelays(m1TauNsteps,f1Val).CBF.tau
            title_str = sprintf('T_m = [%3.1f-%3.1f] @T_f = %3.1f',...
                BOLD_Model.S_BOLD.VARdelays(1,f1Val).CMRO2.tau, ....
                BOLD_Model.S_BOLD.VARdelays(m1TauNsteps,f1Val).CMRO2.tau,...
                BOLD_Model.S_BOLD.VARdelays(m1TauNsteps,f1Val).CBF.tau);        
        else
            title_str = sprintf('T_m = [%3.1f-%3.1f] @T_f = [%3.1f-%3.1f]',...
                BOLD_Model.S_BOLD.VARdelays(1,f1Val).CMRO2.tau, ....
                BOLD_Model.S_BOLD.VARdelays(m1TauNsteps,f1Val).CMRO2.tau,...
                BOLD_Model.S_BOLD.VARdelays(1,f1Val).CBF.tau, ...
                BOLD_Model.S_BOLD.VARdelays(m1TauNsteps,f1Val).CBF.tau);
        end
        title(title_str);     
        n_text_str = sprintf('n = [%3.1f, %3.1f, %3.1f,...%3.1f]', ...
        	BOLD_Model.S_BOLD.VARdelays(1, f1Val).n.Actual, ...
            BOLD_Model.S_BOLD.VARdelays(2, f1Val).n.Actual, ...
        	BOLD_Model.S_BOLD.VARdelays(3, f1Val).n.Actual, ...
            BOLD_Model.S_BOLD.VARdelays(m1TauNsteps,f1Val).n.Actual);
        text(50, 0.97,n_text_str,'FontSize',11)
        axis([0 totaldur 0.965 1.035])
        ylabel('S_{BOLD} / S_0')
        xlabel('time [sec]') 
        ax = gca;
        ax.FontSize = 11;
        hold off
%     end %if mod(f1Val,2) == 0
end %for f1Val

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% save plot to file                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin > 1
    FNout = fullfile(ResultPath,sprintf('%s.tif',Label));
    saveas(gcf,FNout);
end

end

