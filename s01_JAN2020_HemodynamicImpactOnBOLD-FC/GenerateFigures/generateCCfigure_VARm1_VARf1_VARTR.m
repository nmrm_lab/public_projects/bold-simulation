function generateCCfigure_VARm1_VARf1_VARTR(TR100, TR1000, TR2000, ...
                                            Label, ResultPath)
% function generateCCfigure_VARm1_VARf1_VARTR(TR100, TR1000, TR2000, ...
%                                             Label, ResultPath)
% CP 29MAR19 Plot CC arrays
% CP 01OCT19 generate figures for paper
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%   Plot CC array created from BOLD TCs with same SNRs          %%%%%%
%%%%%%  (SNR array size = BOLD_Model.SimulationPars.SNRmatrixSize)   %%%%%%
%%%%%%  but different random number realizations                     %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SNR matrix size --> sqrt(number of different random noise realizations)
% --> create SmS x SmS patches in CC maps with same SNR but different noise
%     realizations
SmS = TR100.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNRmatrixSize;
% Border width between SmS x SmS patches with same SNR
% gap = TR100.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNRgapSize;
gap = TR100.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNRgapSize;

figure('Units','centimeters','Position',[10 5 40 20]); 
    % Global settings %%%%% 
    FigureProps.GlobalTitleStr = '';%sprintf('%s\n\n',Label);
    FigureProps.cFsize = 1.1; % Fontsize 12 pt 
    FigureProps.cLabel = ''; 
%     FigureProps.NewColorMap = nawhimar;
    FigureProps.NewColorMap = gray;
    FigureProps.TitleStr = ''; % title for subplots
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings and plots for VARm1_VARf1 TR100 ms scenario                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% number of prescribed CMRO2 values
m1Nsteps = TR100.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Nsteps;
% minimum of prescribed CMRO2 values
m1Min = TR100.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Min;
% step size for CMRO2 changes
m1StepSize = TR100.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1StepSize;
% max CBF value
m1Max = m1Min + (m1Nsteps-1) * m1StepSize;
% minimum of prescribed CBF values
f1Min = TR100.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1Min;
% number of prescribed CBF values
f1NSteps = TR100.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1NSteps; 
% step size for CBF changes
f1StepSize = TR100.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1StepSize; 
% max CBF value
f1Max = f1Min + (f1NSteps-1) * f1StepSize;
% allow for (ninterval+1) x & y AxisTicks
ninterval = 3;
    FigureProps.minVal = m1Min;
    FigureProps.maxVal = m1Max;
    FigureProps.xAxisTick = m1Min+(SmS+gap)/2:...
                    (SmS+gap)*round(m1Nsteps/ninterval):m1Nsteps*(SmS+gap);
    % label xAxisTicks with M1 values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',m1Min + (count-1)*((m1Max-m1Min)/ninterval));
	end
    FigureProps.xAxisLabel = 'm1';
    FigureProps.yAxisTick = f1Min+(SmS+gap)/2:...
                    (SmS+gap)*round(m1Nsteps/ninterval):f1NSteps*(SmS+gap); 
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.1f',f1Max - (count-1)*((f1Max-1)/ninterval));
    end
    FigureProps.yAxisLabel = 'f1';        
   
subplot(2,3,1)
    FigureProps.cLabel = 'CC (m1, f1)';
	FigureProps.minVal = -0.4;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(TR100.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.maxVal = max(TR100.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%                      TR100.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNR0); 
    FigureProps.TitleStr = sprintf('(a)                       CC = [%3.2f...%3.2f]                  	          ',...
        min(TR100.CC.SNRmatrix.RHO_SNR(:)),max(TR100.CC.SNRmatrix.RHO_SNR(:)));
    newdisp_FC(TR100.CC.SNRmatrix.RHO_SNR, FigureProps)
    
subplot(2,3,4)
    FigureProps.cLabel = 'p value (m1, f1)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(TR100.CC.SNRmatrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(TR100.CC.SNRmatrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(d)                                                                               ');     
    newdisp_FC(TR100.CC.SNRmatrix.PVAL_SNR, FigureProps)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings and plots for VARm1_VARf1 TR1000 ms scenario                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% number of prescribed CMRO2 values
m1Nsteps = TR1000.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Nsteps;
% minimum of prescribed CMRO2 values
m1Min = TR1000.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Min;
% step size for CMRO2 changes
m1StepSize = TR1000.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1StepSize;
% max CBF value
m1Max = m1Min + (m1Nsteps-1) * m1StepSize;
% minimum of prescribed CBF values
f1Min = TR1000.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1Min;
% number of prescribed CBF values
f1NSteps = TR1000.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1NSteps; 
% step size for CBF changes
f1StepSize = TR1000.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1StepSize; 
% max CBF value
f1Max = f1Min + (f1NSteps-1) * f1StepSize;
% allow for (ninterval+1) x & y AxisTicks
ninterval = 3;
    FigureProps.minVal = m1Min;
    FigureProps.maxVal = m1Max;
    FigureProps.xAxisTick = m1Min+(SmS+gap)/2:...
                    (SmS+gap)*round(m1Nsteps/ninterval):m1Nsteps*(SmS+gap);
    % label xAxisTicks with M1 values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',m1Min + (count-1)*((m1Max-m1Min)/ninterval));
	end
    FigureProps.xAxisLabel = 'm1';
    FigureProps.yAxisTick = f1Min+(SmS+gap)/2:...
                    (SmS+gap)*round(m1Nsteps/ninterval):f1NSteps*(SmS+gap); 
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.1f',f1Max - (count-1)*((f1Max-1)/ninterval));
    end
    FigureProps.yAxisLabel = 'f1';        
   
subplot(2,3,2)
    FigureProps.cLabel = 'CC (m1, f1)';
	FigureProps.minVal = -0.4;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(TR1000.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.maxVal = max(TR1000.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%                      TR1000.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNR0);
    FigureProps.TitleStr = sprintf('(b)                       CC = [%3.2f...%3.2f]                  	          ',...
        min(TR1000.CC.SNRmatrix.RHO_SNR(:)),max(TR1000.CC.SNRmatrix.RHO_SNR(:)));
    newdisp_FC(TR1000.CC.SNRmatrix.RHO_SNR, FigureProps)
    
subplot(2,3,5)
    FigureProps.cLabel = 'p value (m1, f1)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(TR1000.CC.SNRmatrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(TR1000.CC.SNRmatrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(e)                                                                               ');    
    newdisp_FC(TR1000.CC.SNRmatrix.PVAL_SNR, FigureProps)    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings and plots for VARm1_VARf1 TR2000 ms scenario                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% number of prescribed CMRO2 values
m1Nsteps = TR2000.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Nsteps;
% minimum of prescribed CMRO2 values
m1Min = TR2000.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Min;
% step size for CMRO2 changes
m1StepSize = TR2000.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1StepSize;
% max CBF value
m1Max = m1Min + (m1Nsteps-1) * m1StepSize;
% minimum of prescribed CBF values
f1Min = TR2000.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1Min;
% number of prescribed CBF values
f1NSteps = TR2000.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1NSteps; 
% step size for CBF changes
f1StepSize = TR2000.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1StepSize; 
% max CBF value
f1Max = f1Min + (f1NSteps-1) * f1StepSize;
% allow for (ninterval+1) x & y AxisTicks
ninterval = 3;
    FigureProps.minVal = m1Min;
    FigureProps.maxVal = m1Max;
    FigureProps.xAxisTick = m1Min+(SmS+gap)/2:...
                    (SmS+gap)*round(m1Nsteps/ninterval):m1Nsteps*(SmS+gap);
    % label xAxisTicks with M1 values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',m1Min + (count-1)*((m1Max-m1Min)/ninterval));
	end
    FigureProps.xAxisLabel = 'm1';
    FigureProps.yAxisTick = f1Min+(SmS+gap)/2:...
                    (SmS+gap)*round(m1Nsteps/ninterval):f1NSteps*(SmS+gap); 
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.1f',f1Max - (count-1)*((f1Max-1)/ninterval));
    end
    FigureProps.yAxisLabel = 'f1';        
   
subplot(2,3,3)
    FigureProps.cLabel = 'CC (m1, f1)';
	FigureProps.minVal = -0.4;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(TR2000.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.maxVal = max(TR2000.CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%                      TR2000.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNR0);
    FigureProps.TitleStr = sprintf('(c)                       CC = [%3.2f...%3.2f]                  	          ',...
        min(TR2000.CC.SNRmatrix.RHO_SNR(:)),max(TR2000.CC.SNRmatrix.RHO_SNR(:)));
    newdisp_FC(TR2000.CC.SNRmatrix.RHO_SNR, FigureProps)
    
subplot(2,3,6)
    FigureProps.cLabel = 'p value (m1, f1)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(TR2000.CC.SNRmatrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(TR2000.CC.SNRmatrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(f)                                                                               ');   
    newdisp_FC(TR2000.CC.SNRmatrix.PVAL_SNR, FigureProps)    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% save plot to file                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FNout = fullfile(ResultPath,sprintf('%s.tif', Label));
saveas(gcf,FNout);     
end

