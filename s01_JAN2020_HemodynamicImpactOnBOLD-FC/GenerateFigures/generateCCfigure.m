function generateCCfigure_VARm1_VARf1(VARm1_VARf1, VARdelays, VARcbv, Label, ResultPath)
% CP 29MAR19 Plot CC arrays
% CP 01OCT19 generate figures for paper
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%   Plot CC array created from BOLD TCs with same SNRs          %%%%%%
%%%%%%  (SNR array size = BOLD_Model.SimulationPars.SNRmatrixSize)   %%%%%%
%%%%%%  but different random number realizations                     %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SNR matrix size --> sqrt(number of different random noise realizations)
% --> create SmS x SmS patches in CC maps with same SNR but different noise
%     realizations
SmS = VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNRmatrixSize;
% Border width between SmS x SmS patches with same SNR
% gap = VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNRgapSize;
gap = VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNRgapSize;

figure('Units','centimeters','Position',[10 5 40 20]); 
    % Global settings %%%%% 
    FigureProps.GlobalTitleStr = sprintf('%s\n\n',Label);
    FigureProps.cFsize = 1.1; % Fontsize 12 pt 
    FigureProps.cLabel = ''; 
%     FigureProps.NewColorMap = nawhimar;
    FigureProps.NewColorMap = gray;
    FigureProps.TitleStr = ''; % title for subplots
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings and plots for VARm1_VARf1 scenario                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% number of prescribed CMRO2 values
m1Nsteps = VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Nsteps;
% minimum of prescribed CMRO2 values
m1Min = VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Min;
% step size for CMRO2 changes
m1StepSize = VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1StepSize;
% max CBF value
m1Max = m1Min + (m1Nsteps-1) * m1StepSize;
% minimum of prescribed CBF values
f1Min = VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1Min;
% number of prescribed CBF values
f1NSteps = VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1NSteps; 
% step size for CBF changes
f1StepSize = VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1StepSize; 
% max CBF value
f1Max = f1Min + (f1NSteps-1) * f1StepSize;
% allow for (ninterval+1) x & y AxisTicks
ninterval = 3;
    FigureProps.minVal = m1Min;
    FigureProps.maxVal = m1Max;
    FigureProps.xAxisTick = m1Min+(SmS+gap)/2:...
                    (SmS+gap)*round(m1Nsteps/ninterval):m1Nsteps*(SmS+gap);
    % label xAxisTicks with M1 values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.2f',m1Min + (count-1)*((m1Max-m1Min)/ninterval));
	end
    FigureProps.xAxisLabel = 'm1';
    FigureProps.yAxisTick = f1Min+(SmS+gap)/2:...
                    (SmS+gap)*round(m1Nsteps/ninterval):f1NSteps*(SmS+gap); 
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.2f',f1Max - (count-1)*((f1Max-1)/ninterval));
    end
    FigureProps.yAxisLabel = 'f1';        
   
subplot(2,3,1)
    FigureProps.cLabel = 'CC (m1, f1)';
% 	FigureProps.minVal = -0.5;
% 	FigureProps.maxVal = 0.5; 
    FigureProps.minVal = min(VARm1_VARf1.CC.SNRmatrix.RHO_SNR(:));
    FigureProps.maxVal = max(VARm1_VARf1.CC.SNRmatrix.RHO_SNR(:));
    FigureProps.TitleStr = sprintf('SNR = %3.0f',...
                     VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNR0);    
    newdisp_FC(VARm1_VARf1.CC.SNRmatrix.RHO_SNR, FigureProps)
    
subplot(2,3,4)
    FigureProps.cLabel = 'p value (m1, f1)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(VARm1_VARf1.CC.SNRmatrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(VARm1_VARf1.CC.SNRmatrix.PVAL_SNR(:));
    FigureProps.TitleStr = '';    
    newdisp_FC(VARm1_VARf1.CC.SNRmatrix.PVAL_SNR, FigureProps)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings and plots for VARdelays scenario                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% number of prescribed CMRO2 values
m1TauNsteps = VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.m1TauNsteps;
% minimum of prescribed CMRO2 values
m1TauMin = VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.m1TauMin;
% step size for CMRO2 changes
m1TauStepSize = VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.m1TauStepSize;
% max CBF value
m1TauMax = m1TauMin + (m1TauNsteps-1) * m1TauStepSize;
% minimum of prescribed CBF values
f1TauMin = VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.f1TauMin;
% number of prescribed CBF values
f1TauNsteps = VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.f1TauNsteps; 
% step size for CBF changes
f1TauStepSize = VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.f1TauStepSize; 
% max CBF value
f1TauMax = f1TauMin + (f1TauNsteps-1) * f1TauStepSize;
% allow for (ninterval+1) x & y AxisTicks
ninterval = 5;

    FigureProps.minVal = m1TauMin;
    FigureProps.maxVal = m1TauMax;
    FigureProps.xAxisTick = m1TauMin+(SmS+gap)/2:...
           	(SmS+gap)*round(m1TauNsteps/ninterval):m1TauNsteps*(SmS+gap);
    % label xAxisTicks with Tm values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.2f',m1TauMin + (count-1)*((m1TauMax-m1TauMin)/ninterval));
	end
    FigureProps.xAxisLabel = '\tau_m';
    FigureProps.yAxisTick = f1TauMin+(SmS+gap)/2:...
           	(SmS+gap)*round(m1TauNsteps/ninterval):f1TauNsteps*(SmS+gap); 
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.2f',f1TauMax - (count-1)*((f1TauMax-m1TauMin)/ninterval));
    end
    FigureProps.yAxisLabel = '\tau_f';       
    
subplot(2,3,2)
    FigureProps.cLabel = 'CC (\tau_m, \tau_f)';
% 	FigureProps.minVal = -0.5;
% 	FigureProps.maxVal = 0.5; 
    FigureProps.minVal = min(VARdelays.CC.SNRmatrix.RHO_SNR(:));
    FigureProps.maxVal = max(VARdelays.CC.SNRmatrix.RHO_SNR(:));
    FigureProps.TitleStr = sprintf('SNR = %3.0f',...
                     VARdelays.S_BOLD.VARdelays(1,1).SimulationPars.SNR0);    
    newdisp_FC(VARdelays.CC.SNRmatrix.RHO_SNR, FigureProps)
    
subplot(2,3,5)
    FigureProps.cLabel = 'p value (\tau_m, \tau_f)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(VARdelays.CC.SNRmatrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(VARdelays.CC.SNRmatrix.PVAL_SNR(:));
    FigureProps.TitleStr = '';    
    newdisp_FC(VARdelays.CC.SNRmatrix.PVAL_SNR, FigureProps)    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings and plots for VARcbv scenario                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% number of prescribed CBV temporal delay values
cbvTauNsteps = VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauNsteps;
% minimum of prescribed CBV temporal delay values
cbvTauMin = VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauMin;
% step size for CBV temporal delay changes
cbvTauStepSize = VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauStepSize;
% max CBV temporal delay value
cbvTauMax = cbvTauMin + (cbvTauNsteps-1) * cbvTauStepSize;
% minimum of prescribed alpha values
cbvAlphaMin = VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaMin;
% number of prescribed alpha values
cbvAlphaNsteps = VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaNsteps; 
% step size for alpha changes
cbvAlphaStepSize = VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaStepSize; 
% max alpha value
cbvAlphaMax = cbvAlphaMin + (cbvAlphaNsteps-1) * cbvAlphaStepSize;
% allow for (ninterval+1) x & y AxisTicks
ninterval = 3;

    FigureProps.minVal = cbvTauMin;
    FigureProps.maxVal = cbvTauMax;   
    FigureProps.xAxisTick = 1:round(cbvTauNsteps/ninterval):cbvTauNsteps; 
    % label xAxisTicks with Tv values
    FigureProps.xAxisTick = cbvTauMin+(SmS+gap)/2:...
           	(SmS+gap)*round(cbvTauNsteps/ninterval):cbvTauNsteps*(SmS+gap);   
    % label xAxisTicks with Tv values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',cbvTauMin + (count-1)*((cbvTauMax-cbvTauMin)/ninterval))
	end
    FigureProps.xAxisLabel = '\tau_v'; 
    FigureProps.yAxisTick = cbvAlphaMin+(SmS+gap)/2:...
    	(SmS+gap)*round(cbvAlphaNsteps/ninterval):cbvAlphaNsteps*(SmS+gap); 
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.3f',cbvAlphaMax - (count-1)*((cbvAlphaMax-cbvAlphaMin)/ninterval))
    end
    FigureProps.yAxisLabel = '\alpha'; 

subplot(2,3,3)
    FigureProps.cLabel = 'CC (\tau_v, \alpha_v)';
% 	FigureProps.minVal = 0;
% 	FigureProps.maxVal = 0.5; 
    FigureProps.minVal = min(VARcbv.CC.SNRmatrix.RHO_SNR(:));
    FigureProps.maxVal = max(VARcbv.CC.SNRmatrix.RHO_SNR(:));
    FigureProps.TitleStr = sprintf('SNR = %3.0f',...
                     VARcbv.S_BOLD.VARcbv(1,1).SimulationPars.SNR0);    
    newdisp_FC(VARcbv.CC.SNRmatrix.RHO_SNR, FigureProps)
    
subplot(2,3,6)
    FigureProps.cLabel = 'p value (\tau_v, \alpha_v)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(VARcbv.CC.SNRmatrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(VARcbv.CC.SNRmatrix.PVAL_SNR(:));
    FigureProps.TitleStr = '';    
    newdisp_FC(VARcbv.CC.SNRmatrix.PVAL_SNR, FigureProps)
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% save plot to file                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FNout = fullfile(ResultPath,sprintf('%s.tif', Label));
saveas(gcf,FNout);     
end

