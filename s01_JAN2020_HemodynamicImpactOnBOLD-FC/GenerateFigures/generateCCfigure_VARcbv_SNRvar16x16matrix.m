function generateCCfigure_VARcbv_SNRvar16x16matrix(Results, ...
                                                Label, ResultPath)
% CP 29MAR19 Plot CC arrays
% CP 01OCT19 generate figures for paper
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%   Plot CC array created from BOLD TCs with same SNRs          %%%%%%
%%%%%%  (SNR array size = BOLD_Model.SimulationPars.SNRmatrixSize)   %%%%%%
%%%%%%  but different random number realizations                     %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SNR matrix size --> sqrt(number of different random noise realizations)
% --> create SmS x SmS patches in CC maps with same SNR but different noise
%     realizations
SmS = Results.S_BOLD.VARcbv(1,1).SimulationPars.SNRmatrixSize;
% Border width between SmS x SmS patches with same SNR
% gap = VARm1_VARf1.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNRgapSize;
gap = Results.S_BOLD.VARcbv(1,1).SimulationPars.SNRgapSize;

figure('Units','centimeters','Position',[10 10 40 15]); 
    % Global settings %%%%% 
    FigureProps.GlobalTitleStr = '';%sprintf('%s\n\n',Label);
    FigureProps.cFsize = 1.1; % Fontsize 12 pt 
    FigureProps.cLabel = ''; 
%     FigureProps.NewColorMap = nawhimar;
    FigureProps.NewColorMap = gray;
    FigureProps.TitleStr = ''; % title for subplots

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings and plots for VARcbv scenario                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% number of prescribed CBV temporal delay values
cbvTauNsteps = Results.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauNsteps;
% minimum of prescribed CBV temporal delay values
cbvTauMin = Results.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauMin;
% step size for CBV temporal delay changes
cbvTauStepSize = Results.S_BOLD.VARcbv(1,1).SimulationPars.cbvTauStepSize;
% max CBV temporal delay value
cbvTauMax = cbvTauMin + (cbvTauNsteps-1) * cbvTauStepSize;
% minimum of prescribed alpha values
cbvAlphaMin = Results.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaMin;
% number of prescribed alpha values
cbvAlphaNsteps = Results.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaNsteps; 
% step size for alpha changes
cbvAlphaStepSize = Results.S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaStepSize; 
% max alpha value
cbvAlphaMax = cbvAlphaMin + (cbvAlphaNsteps-1) * cbvAlphaStepSize;
% allow for (ninterval+1) x & y AxisTicks
ninterval = 3;

    FigureProps.minVal = cbvTauMin;
    FigureProps.maxVal = cbvTauMax;   
    FigureProps.xAxisTick = 1:round(cbvTauNsteps/ninterval):cbvTauNsteps; 
    % label xAxisTicks with Tv values
    FigureProps.xAxisTick = cbvTauMin+(SmS+gap)/2:...
           	(SmS+gap)*round(cbvTauNsteps/ninterval):cbvTauNsteps*(SmS+gap);   
    % label xAxisTicks with Tv values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',cbvTauMin + (count-1)*((cbvTauMax-cbvTauMin)/ninterval));
	end
    FigureProps.xAxisLabel = '\tau_v'; 
    FigureProps.yAxisTick = cbvAlphaMin+(SmS+gap)/2:...
    	(SmS+gap)*round(cbvAlphaNsteps/ninterval):cbvAlphaNsteps*(SmS+gap); 
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.3f',cbvAlphaMax - (count-1)*((cbvAlphaMax-cbvAlphaMin)/ninterval));
    end
    FigureProps.yAxisLabel = '\alpha_v'; 
    
%--------------------------------------------------------------------------
% SNR = 1000
%-------------------------------------------------------------------------- 
subplot(2,4,1)
    FigureProps.cLabel = 'CC (\tau_v, \alpha_v)';
	FigureProps.minVal = -0.40;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(Results.CC.SNR1000matrix.RHO_SNR(:));
%     FigureProps.maxVal = max(Results.CC.SNR1000matrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%                      Results.S_BOLD.VARcbv(1,1).SimulationPars.SNR0);
    FigureProps.TitleStr = sprintf('(a)              CC = [%3.2f...%3.2f]                      ',...
        min(Results.CC.SNR1000matrix.RHO_SNR(:)),max(Results.CC.SNR1000matrix.RHO_SNR(:)));
    newdisp_FC(Results.CC.SNR1000matrix.RHO_SNR, FigureProps)
    
subplot(2,4,5)
    FigureProps.cLabel = 'p value (\tau_v, \alpha_v)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(Results.CC.SNR1000matrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(Results.CC.SNR1000matrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(e)                                                                 '); 
    newdisp_FC(Results.CC.SNR1000matrix.PVAL_SNR, FigureProps)
    
%--------------------------------------------------------------------------
% SNR = 500
%-------------------------------------------------------------------------- 
subplot(2,4,2)
    FigureProps.cLabel = 'CC (\tau_v, \alpha_v)';
	FigureProps.minVal = -0.40;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(Results.CC.SNR500matrix.RHO_SNR(:));
%     FigureProps.maxVal = max(Results.CC.SNR500matrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%                      Results.S_BOLD.VARcbv(1,1).SimulationPars.SNR0);
    FigureProps.TitleStr = sprintf('(b)              CC = [%3.2f...%3.2f]                      ',...
        min(Results.CC.SNR500matrix.RHO_SNR(:)),max(Results.CC.SNR500matrix.RHO_SNR(:)));
    newdisp_FC(Results.CC.SNR500matrix.RHO_SNR, FigureProps)
    
subplot(2,4,6)
    FigureProps.cLabel = 'p value (\tau_v, \alpha_v)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(Results.CC.SNR500matrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(Results.CC.SNR500matrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(f)                                                                 '); 
    newdisp_FC(Results.CC.SNR500matrix.PVAL_SNR, FigureProps)
    
%--------------------------------------------------------------------------
% SNR = 250
%-------------------------------------------------------------------------- 
subplot(2,4,3)
    FigureProps.cLabel = 'CC (\tau_v, \alpha_v)';
	FigureProps.minVal = -0.40;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(Results.CC.SNR250matrix.RHO_SNR(:));
%     FigureProps.maxVal = max(Results.CC.SNR250matrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%                      Results.S_BOLD.VARcbv(1,1).SimulationPars.SNR0);
    FigureProps.TitleStr = sprintf('(c)              CC = [%3.2f...%3.2f]                      ',...
        min(Results.CC.SNR250matrix.RHO_SNR(:)),max(Results.CC.SNR250matrix.RHO_SNR(:)));
    newdisp_FC(Results.CC.SNR250matrix.RHO_SNR, FigureProps)
    
subplot(2,4,7)
    FigureProps.cLabel = 'p value (\tau_v, \alpha_v)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(Results.CC.SNR250matrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(Results.CC.SNR250matrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(g)                                                                 '); 
    newdisp_FC(Results.CC.SNR250matrix.PVAL_SNR, FigureProps)

%--------------------------------------------------------------------------
% SNR = 125
%-------------------------------------------------------------------------- 
subplot(2,4,4)
    FigureProps.cLabel = 'CC (\tau_v, \alpha_v)';
	FigureProps.minVal = -0.40;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(Results.CC.SNR125matrix.RHO_SNR(:));
%     FigureProps.maxVal = max(Results.CC.SNR125matrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%                      Results.S_BOLD.VARcbv(1,1).SimulationPars.SNR0);
    FigureProps.TitleStr = sprintf('(d)              CC = [%3.2f...%3.2f]                      ',...
        min(Results.CC.SNR125matrix.RHO_SNR(:)),max(Results.CC.SNR125matrix.RHO_SNR(:)));
    newdisp_FC(Results.CC.SNR125matrix.RHO_SNR, FigureProps)
    
subplot(2,4,8)
    FigureProps.cLabel = 'p value (\tau_v, \alpha_v)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(Results.CC.SNR125matrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(Results.CC.SNR125matrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(h)                                                                 '); 
    newdisp_FC(Results.CC.SNR125matrix.PVAL_SNR, FigureProps)
                       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% save plot to file                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FNout = fullfile(ResultPath,sprintf('%s.tif', Label));
saveas(gcf,FNout);     
end

