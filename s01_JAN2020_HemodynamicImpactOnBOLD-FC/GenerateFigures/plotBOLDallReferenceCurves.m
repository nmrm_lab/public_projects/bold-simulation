function plotBOLDallReferenceCurves(TR100_BalloonModel_TightCoupling, ...
                                    TR100_BalloonModel_SlowerCBV, ...
                                    TR1000_BalloonModel_TightCoupling, ...
                                    TR1000_BalloonModel_SlowerCBV, ...
                                    TR2000_BalloonModel_TightCoupling, ...
                                    TR2000_BalloonModel_SlowerCBV, ...
                                    Label, ResultPath)
% m1Val == f1Val == 0 -> use neuronal input '1' as reference

%plot time course variables
totalDur = TR100_BalloonModel_TightCoupling.reference.SimulationPars.totalDur;
hrfDur = ceil(max(TR100_BalloonModel_TightCoupling.reference.CBF.h.Time));

figure('Units','centimeters','Position',[0 5 50 20]); 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Inputs                                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

subplot(2,5,1); % Neuronal Input N(t)
nInput = 1; % for plot of reference curve
plot(TR100_BalloonModel_TightCoupling.reference.NeuronalInput.ts(nInput),...
                                                       'b-','LineWidth',2)
maxval_y = 1.1*...
    max(TR100_BalloonModel_TightCoupling.reference.NeuronalInput.ts(nInput).Data);
axis([0 totalDur -0.05 maxval_y])
text(-100,1.08,'(a)','Fontsize',14);
title('')
xlabel('time [sec]')
ylabel('neuronal input - N(t)','Fontsize',11)
ax = gca;
ax.FontSize = 11;


subplot(2,5,6); % gamma variate h(t)
plot(TR100_BalloonModel_TightCoupling.reference.CBF.h,'b-','LineWidth',2)
hold on
plot(TR100_BalloonModel_TightCoupling.reference.CMRO2.h,'r-','LineWidth',2)
legend('h_{f}(t)','h_{m}(t)','FontSize',9)
maxval_y = 1.1*max(max(TR100_BalloonModel_TightCoupling.reference.CBF.h), ....
               max(TR100_BalloonModel_TightCoupling.reference.CMRO2.h));
axis([0 hrfDur -0.1*maxval_y maxval_y])
text(-30,0.147,'(f)','Fontsize',14);
title('')
xlabel('time [sec]','Fontsize',11)
ylabel('gamma variate - h(t)','Fontsize',11)
ax = gca;
ax.FontSize = 11;
hold off

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulation Results - Tight Coupling Reference                                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

subplot(2,5,2);
plot(100*(TR100_BalloonModel_TightCoupling.reference.CBF.fin-1),'b-','LineWidth',2)
hold on
plot(100*(TR100_BalloonModel_TightCoupling.reference.CBF.fout-1),'c-','LineWidth',2)
plot(100*(TR100_BalloonModel_TightCoupling.reference.CMRO2.min-1),'r-','LineWidth',2)
plot(100*(TR100_BalloonModel_TightCoupling.reference.CBV.vout-1),'g','LineWidth',2)

legend('CBF_{in}','CBF_{out}','CMRO2','CBV_v','FontSize',10)
axis([0 totalDur -5 65])
text(-100,64,'(b)','Fontsize',14);
% title('Tight Coupling')
legend1 = sprintf('= %1.2f',TR100_BalloonModel_TightCoupling.reference.CBF.amplitude);
legend2 = sprintf('= %1.1f',TR100_BalloonModel_TightCoupling.reference.CBF.tau);
legend3 = sprintf('= %1.2f',TR100_BalloonModel_TightCoupling.reference.CMRO2.amplitude);
legend4 = sprintf('= %1.1f',TR100_BalloonModel_TightCoupling.reference.CMRO2.tau);
legend5 = sprintf('= %1.1f',...
        (max(TR100_BalloonModel_TightCoupling.reference.CBF.fin)-1)/...
        (max(TR100_BalloonModel_TightCoupling.reference.CMRO2.min)-1));
legend6 = sprintf('= %1.3f',TR100_BalloonModel_TightCoupling.reference.CBV.alpha);
legend7 = sprintf('= %1.1f',TR100_BalloonModel_TightCoupling.reference.CBV.tau);

text(50,60,'f1','Fontsize',10);
text(80,60,legend1,'Fontsize',10);
text(50,56,'\tau_f','Fontsize',10);
text(80,56,legend2,'Fontsize',10);
text(50,50,'m1','Fontsize',10);
text(80,50,legend3,'Fontsize',10);
text(50,46,'\tau_m','Fontsize',10);
text(80,46,legend4,'Fontsize',10);
text(50,42,'n','Fontsize',10);
text(80,42,legend5,'Fontsize',10);
text(50,36,'\alpha','Fontsize',10);
text(80,36,legend6,'Fontsize',10);
text(50,32,'\tau_v','Fontsize',10);
text(80,32,legend7,'Fontsize',10);

% yticks([1.0 1.2 1.4 1.6 1.8 2.0])
yticks([0 50])
ylabel('\Delta f / f_0 [%]')
% xticks(0:20:20*floor(totalDur/20))
xlabel('time [sec]')
ax = gca;
ax.FontSize = 11;
hold off

%--------------------------------------------------------------------------
% Simulation Results - Tight Coupling Reference - BOLD signal TR=100ms                                                            %
%--------------------------------------------------------------------------
subplot(2,5,3);

plot(100*(TR100_BalloonModel_TightCoupling.reference.BOLD.boutNoise-1),'r','LineWidth',1)
hold on
plot(100*(TR100_BalloonModel_TightCoupling.reference.BOLD.bout-1),'b','LineWidth',2)
plot([0 totalDur], [0 0],'k--','LineWidth',1)
legend('noisy BOLD','model BOLD','FontSize',10)
axis([0 totalDur -3 4])
text(-100,4,'(c)','Fontsize',14);
title('')
% yticks([0.99 1.00 1.01 1.02 1.03 1.04])
yticks([-3 -2 -1 0 1 2 3 4])
text_str = sprintf('SNR_0 = %3.0f',...
    TR100_BalloonModel_TightCoupling.reference.SimulationPars.SNR0);
text(15,3.4,text_str)
text(15,3.0,'TR = 100 ms');
ylabel('\Delta S_{BOLD} / S_0 [%]')
xlabel('time [sec]')                
ax = gca;
ax.FontSize = 11;
hold off

%--------------------------------------------------------------------------
% Simulation Results - Tight Coupling Reference - BOLD signal TR=1000ms                                                            %
%--------------------------------------------------------------------------
subplot(2,5,4);

plot(100*(TR1000_BalloonModel_TightCoupling.reference.BOLD.boutNoise-1),'r','LineWidth',1)
hold on
plot(100*(TR1000_BalloonModel_TightCoupling.reference.BOLD.bout-1),'b','LineWidth',2)
plot([0 totalDur], [0 0],'k--','LineWidth',1)
legend('noisy BOLD','model BOLD','FontSize',10)
axis([0 totalDur -3 4])
text(-100,4,'(d)','Fontsize',14);
title('')
% yticks([0.99 1.00 1.01 1.02 1.03 1.04])
yticks([-3 -2 -1 0 1 2 3 4])
text_str = sprintf('SNR_0 = %3.0f',...
    TR1000_BalloonModel_TightCoupling.reference.SimulationPars.SNR0);
text(15,3.4,text_str)
text(15,3.0,'TR = 1000 ms');
ylabel('\Delta S_{BOLD} / S_0 [%]')
xlabel('time [sec]')                
ax = gca;
ax.FontSize = 11;
hold off

%--------------------------------------------------------------------------
% Simulation Results - Tight Coupling Reference - BOLD signal TR=1000ms                                                            %
%--------------------------------------------------------------------------
subplot(2,5,5);

plot(100*(TR2000_BalloonModel_TightCoupling.reference.BOLD.boutNoise-1),'r','LineWidth',1)
hold on
plot(100*(TR2000_BalloonModel_TightCoupling.reference.BOLD.bout-1),'b','LineWidth',2)
plot([0 totalDur], [0 0],'k--','LineWidth',1)
legend('noisy BOLD','model BOLD','FontSize',10)
axis([0 totalDur -3 4])
text(-100,4,'(e)','Fontsize',14);
title('')
% yticks([0.99 1.00 1.01 1.02 1.03 1.04])
yticks([-3 -2 -1 0 1 2 3 4])
text_str = sprintf('SNR_0 = %3.0f',...
    TR2000_BalloonModel_TightCoupling.reference.SimulationPars.SNR0);
text(15,3.4,text_str)
text(15,3.0,'TR = 2000 ms');
ylabel('\Delta S_{BOLD} / S_0 [%]')
xlabel('time [sec]')                
ax = gca;
ax.FontSize = 11;
hold off

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulation Results - Slower CBV Reference                                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

subplot(2,5,7);
plot(100*(TR100_BalloonModel_SlowerCBV.reference.CBF.fin-1),'b-','LineWidth',2)
hold on
plot(100*(TR100_BalloonModel_SlowerCBV.reference.CBF.fout-1),'c-','LineWidth',2)
plot(100*(TR100_BalloonModel_SlowerCBV.reference.CMRO2.min-1),'r-','LineWidth',2)
plot(100*(TR100_BalloonModel_SlowerCBV.reference.CBV.vout-1),'g','LineWidth',2)

legend('CBF_{in}','CBF_{out}','CMRO2','CBV_v','FontSize',10)
axis([0 totalDur -5 65])
text(-100,64,'(g)','Fontsize',14);
% title('Slower CBV')
legend1 = sprintf('= %1.2f',TR100_BalloonModel_SlowerCBV.reference.CBF.amplitude);
legend2 = sprintf('= %1.1f',TR100_BalloonModel_SlowerCBV.reference.CBF.tau);
legend3 = sprintf('= %1.2f',TR100_BalloonModel_SlowerCBV.reference.CMRO2.amplitude);
legend4 = sprintf('= %1.1f',TR100_BalloonModel_SlowerCBV.reference.CMRO2.tau);
legend5 = sprintf('= %1.1f',...
        (max(TR100_BalloonModel_SlowerCBV.reference.CBF.fin)-1)/...
        (max(TR100_BalloonModel_SlowerCBV.reference.CMRO2.min)-1));
legend6 = sprintf('= %1.3f',TR100_BalloonModel_SlowerCBV.reference.CBV.alpha);
legend7 = sprintf('= %1.1f',TR100_BalloonModel_SlowerCBV.reference.CBV.tau);

text(50,60,'f1','Fontsize',10);
text(80,60,legend1,'Fontsize',10);
text(50,56,'\tau_f','Fontsize',10);
text(80,56,legend2,'Fontsize',10);
text(50,50,'m1','Fontsize',10);
text(80,50,legend3,'Fontsize',10);
text(50,46,'\tau_m','Fontsize',10);
text(80,46,legend4,'Fontsize',10);
text(50,42,'n','Fontsize',10);
text(80,42,legend5,'Fontsize',10);
text(50,36,'\alpha','Fontsize',10);
text(80,36,legend6,'Fontsize',10);
text(50,32,'\tau_v','Fontsize',10);
text(80,32,legend7,'Fontsize',10);

% yticks([1.0 1.2 1.4 1.6 1.8 2.0])
yticks([0 50])
ylabel('\Delta f / f_0 [%]')
% xticks(0:20:20*floor(totalDur/20))
xlabel('time [sec]')
ax = gca;
ax.FontSize = 11;
hold off

%--------------------------------------------------------------------------
% Simulation Results - Slower CBV Reference - BOLD signal TR=100ms                                                            %
%--------------------------------------------------------------------------
subplot(2,5,8);

plot(100*(TR100_BalloonModel_SlowerCBV.reference.BOLD.boutNoise-1),'r','LineWidth',1)
hold on
plot(100*(TR100_BalloonModel_SlowerCBV.reference.BOLD.bout-1),'b','LineWidth',2)
plot([0 totalDur], [0 0],'k--','LineWidth',1)
legend('noisy BOLD','model BOLD','FontSize',10)
axis([0 totalDur -3 4])
text(-100,4,'(h)','Fontsize',14);
title('')
% yticks([0.99 1.00 1.01 1.02 1.03 1.04])
yticks([-3 -2 -1 0 1 2 3 4])
text_str = sprintf('SNR_0 = %3.0f',TR100_BalloonModel_SlowerCBV.reference.SimulationPars.SNR0);
text(15,3.4,text_str)
text(15,3.0,'TR = 100 ms');
ylabel('\Delta S_{BOLD} / S_0 [%]')
xlabel('time [sec]')                
ax = gca;
ax.FontSize = 11;
hold off

%--------------------------------------------------------------------------
% Simulation Results - Slower CBV Reference - BOLD signal TR=100ms                                                            %
%--------------------------------------------------------------------------
subplot(2,5,9);

plot(100*(TR1000_BalloonModel_SlowerCBV.reference.BOLD.boutNoise-1),'r','LineWidth',1)
hold on
plot(100*(TR1000_BalloonModel_SlowerCBV.reference.BOLD.bout-1),'b','LineWidth',2)
plot([0 totalDur], [0 0],'k--','LineWidth',1)
legend('noisy BOLD','model BOLD','FontSize',10)
axis([0 totalDur -3 4])
text(-100,4,'(i)','Fontsize',14);
title('')
% yticks([0.99 1.00 1.01 1.02 1.03 1.04])
yticks([-3 -2 -1 0 1 2 3 4])
text_str = sprintf('SNR_0 = %3.0f',TR1000_BalloonModel_SlowerCBV.reference.SimulationPars.SNR0);
text(15,3.4,text_str)
text(15,3.0,'TR = 1000 ms');
ylabel('\Delta S_{BOLD} / S_0 [%]')
xlabel('time [sec]')                
ax = gca;
ax.FontSize = 11;
hold off

%--------------------------------------------------------------------------
% Simulation Results - Slower CBV Reference - BOLD signal TR=100ms                                                            %
%--------------------------------------------------------------------------
subplot(2,5,10);

plot(100*(TR2000_BalloonModel_SlowerCBV.reference.BOLD.boutNoise-1),'r','LineWidth',1)
hold on
plot(100*(TR2000_BalloonModel_SlowerCBV.reference.BOLD.bout-1),'b','LineWidth',2)
plot([0 totalDur], [0 0],'k--','LineWidth',1)
legend('noisy BOLD','model BOLD','FontSize',10)
axis([0 totalDur -3 4])
text(-100,4,'(j)','Fontsize',14);
title('')
% yticks([0.99 1.00 1.01 1.02 1.03 1.04])
yticks([-3 -2 -1 0 1 2 3 4])
text_str = sprintf('SNR_0 = %3.0f',TR2000_BalloonModel_SlowerCBV.reference.SimulationPars.SNR0);
text(15,3.4,text_str)
text(15,3.0,'TR = 2000 ms');
ylabel('\Delta S_{BOLD} / S_0 [%]')
xlabel('time [sec]')                
ax = gca;
ax.FontSize = 11;
hold off
% save plot to file
if nargin > 1
    FNout = fullfile(ResultPath, sprintf('%s.tif',Label));
    saveas(gcf,FNout);
end
end

