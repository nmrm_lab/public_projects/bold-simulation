function generateCCfigure_VARm1_VARf1_SNRvar16x16matrix(Results, ...
                                                Label, ResultPath)
% CP 29MAR19 Plot CC arrays
% CP 01OCT19 generate figures for paper
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%   Plot CC array created from BOLD TCs with same SNRs          %%%%%%
%%%%%%  (SNR array size = BOLD_Model.SimulationPars.SNRmatrixSize)   %%%%%%
%%%%%%  but different random number realizations                     %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SNR matrix size --> sqrt(number of different random noise realizations)
% --> create SmS x SmS patches in CC maps with same SNR but different noise
%     realizations
SmS = Results.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNRmatrixSize;
% Border width between SmS x SmS patches with same SNR
% gap = Results.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNRgapSize;
gap = Results.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNRgapSize;

figure('Units','centimeters','Position',[10 10 40 15]); 
    % Global settings %%%%% 
    FigureProps.GlobalTitleStr = '';%sprintf('%s\n\n',Label);
    FigureProps.cFsize = 1.1; % Fontsize 12 pt 
    FigureProps.cLabel = ''; 
%     FigureProps.NewColorMap = nawhimar;
    FigureProps.NewColorMap = gray;
    FigureProps.TitleStr = ''; % title for subplots
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings and plots for VARm1_VARf1                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% number of prescribed CMRO2 values
m1Nsteps = Results.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Nsteps;
% minimum of prescribed CMRO2 values
m1Min = Results.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Min;
% step size for CMRO2 changes
m1StepSize = Results.S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1StepSize;
% max CBF value
m1Max = m1Min + (m1Nsteps-1) * m1StepSize;
% minimum of prescribed CBF values
f1Min = Results.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1Min;
% number of prescribed CBF values
f1NSteps = Results.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1NSteps; 
% step size for CBF changes
f1StepSize = Results.S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1StepSize; 
% max CBF value
f1Max = f1Min + (f1NSteps-1) * f1StepSize;
% allow for (ninterval+1) x & y AxisTicks
ninterval = 3;
    FigureProps.minVal = m1Min;
    FigureProps.maxVal = m1Max;
    FigureProps.xAxisTick = m1Min+(SmS+gap)/2:...
                    (SmS+gap)*round(m1Nsteps/ninterval):m1Nsteps*(SmS+gap);
    % label xAxisTicks with M1 values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',m1Min + (count-1)*((m1Max-m1Min)/ninterval));
	end
    FigureProps.xAxisLabel = 'm1';
    FigureProps.yAxisTick = f1Min+(SmS+gap)/2:...
                    (SmS+gap)*round(m1Nsteps/ninterval):f1NSteps*(SmS+gap); 
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.1f',f1Max - (count-1)*((f1Max-1)/ninterval));
    end
    FigureProps.yAxisLabel = 'f1';        
    
%--------------------------------------------------------------------------
% SNR = 1000
%--------------------------------------------------------------------------
subplot(2,4,1)
    FigureProps.cLabel = 'CC (m1, f1)';
	FigureProps.minVal = -0.4;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(Results.CC.SNR1000matrix.RHO_SNR(:));
%     FigureProps.maxVal = max(Results.CC.SNR1000matrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%                      Results.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNR0); 
    FigureProps.TitleStr = sprintf('(a)            CC = [%3.2f...%3.2f]                ',...
        min(Results.CC.SNR1000matrix.RHO_SNR(:)),max(Results.CC.SNR1000matrix.RHO_SNR(:)));
    newdisp_FC(Results.CC.SNR1000matrix.RHO_SNR, FigureProps)
    
subplot(2,4,5)
    FigureProps.cLabel = 'p value (m1, f1)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.NewColorMap = gray;
%     FigureProps.minVal = min(Results.CC.SNR1000matrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(Results.CC.SNR1000matrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(e)                                                          ');     
    newdisp_FC(Results.CC.SNR1000matrix.PVAL_SNR, FigureProps)

%--------------------------------------------------------------------------
% SNR = 500
%--------------------------------------------------------------------------
subplot(2,4,2)
    FigureProps.cLabel = 'CC (m1, f1)';
	FigureProps.minVal = -0.4;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(Results.CC.SNR500matrix.RHO_SNR(:));
%     FigureProps.maxVal = max(Results.CC.SNR500matrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%                      Results.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNR0); 
    FigureProps.TitleStr = sprintf('(b)            CC = [%3.2f...%3.2f]                ',...
        min(Results.CC.SNR500matrix.RHO_SNR(:)),max(Results.CC.SNR500matrix.RHO_SNR(:)));
    newdisp_FC(Results.CC.SNR500matrix.RHO_SNR, FigureProps)
    
subplot(2,4,6)
    FigureProps.cLabel = 'p value (m1, f1)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(Results.CC.SNR500matrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(Results.CC.SNR500matrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(f)                                                          ');    
    newdisp_FC(Results.CC.SNR500matrix.PVAL_SNR, FigureProps)   

%--------------------------------------------------------------------------
% SNR = 250
%--------------------------------------------------------------------------
subplot(2,4,3)
    FigureProps.cLabel = 'CC (m1, f1)';
	FigureProps.minVal = -0.4;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(Results.CC.SNR250matrix.RHO_SNR(:));
%     FigureProps.maxVal = max(Results.CC.SNR250matrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%                      Results.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNR0); 
    FigureProps.TitleStr = sprintf('(c)            CC = [%3.2f...%3.2f]                ',...
        min(Results.CC.SNR250matrix.RHO_SNR(:)),max(Results.CC.SNR250matrix.RHO_SNR(:)));
    newdisp_FC(Results.CC.SNR250matrix.RHO_SNR, FigureProps)
    
subplot(2,4,7)
    FigureProps.cLabel = 'p value (m1, f1)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(Results.CC.SNR250matrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(Results.CC.SNR250matrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(g)                                                          ');      
    newdisp_FC(Results.CC.SNR250matrix.PVAL_SNR, FigureProps)       

%--------------------------------------------------------------------------
% SNR = 125
%--------------------------------------------------------------------------
subplot(2,4,4)
    FigureProps.cLabel = 'CC (m1, f1)';
	FigureProps.minVal = -0.4;
	FigureProps.maxVal = 0.4; 
%     FigureProps.minVal = min(Results.CC.SNR125matrix.RHO_SNR(:));
%     FigureProps.maxVal = max(Results.CC.SNR125matrix.RHO_SNR(:));
%     FigureProps.TitleStr = sprintf('SNR = %3.0f',...
%                      Results.S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNR0); 
    FigureProps.TitleStr = sprintf('(d)            CC = [%3.2f...%3.2f]                ',...
        min(Results.CC.SNR125matrix.RHO_SNR(:)),max(Results.CC.SNR125matrix.RHO_SNR(:)));
    newdisp_FC(Results.CC.SNR125matrix.RHO_SNR, FigureProps)
    
subplot(2,4,8)
    FigureProps.cLabel = 'p value (m1, f1)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(Results.CC.SNR125matrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(Results.CC.SNR125matrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('(h)                                                          ');      
    newdisp_FC(Results.CC.SNR125matrix.PVAL_SNR, FigureProps)       
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% save plot to file                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FNout = fullfile(ResultPath,sprintf('%s.tif', Label));
saveas(gcf,FNout);     
end

