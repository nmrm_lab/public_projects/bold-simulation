function RUN_SimulateBOLDSignal_NearUncouplingDrew2011
% CP 11FEB20
% simulate BOLD signals resulting from (a-)periodic visual stimuli 
%
% number of steps for any parameter change
Nsteps = 1;
BOLD_Model.SimulationPars.resultPath = ...
	'C:\Christine Preibisch\04_Calibrated_fMRI\BOLD_Simulations\Results\';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set global parameters for simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--------------------------------------------------------------------------
% Set parameters governing BOLD response 
%--------------------------------------------------------------------------
% modeltype - 1: BOLD signal change (in %) according to Balloon model 
%                (Buxton et al. MRM, 1998); updated in (Buxton et al.,2004) 
%                according to (Obata et al., NI, 2004) with coefficients;
%                contains intravascular CBV effects
% [default] - 2: BOLD signal change (in %) according to Simon & Buxton 
%                (NI, 2015); does not contain direct CBV effects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
BOLD_Model.SimulationPars.modeltype = '1';

% number of neuronal inputs
BOLD_Model.SimulationPars.nInputs = 3;
% temporal resolution of neuronal input [sec]
BOLD_Model.SimulationPars.deltaT = 0.05; 
% temporal resolution of simulated BOLD signal curves [sec]
% needs to be entered in Simulink Model Output blocks !!!!
BOLD_Model.SimulationPars.SampleTime = 1.0; 
% simulated time period [sec]
BOLD_Model.SimulationPars.totalDur = 90; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define input neuronal activity that allows to mimick effects of
% entrained alpha activity etc.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
BOLD_Model.NeuronalInput = generateNeuronalInput_VARlength(BOLD_Model.SimulationPars);
BOLD_Model.NeuronalInput.label = 'test';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set Defaults for Balloon Model (Buxton et al. 2004; Simon&Buxton, 2015)
% --> Model parameters govern the BOLD response via time constants 
% for hemodynamic (CBF/CBF0) -> fin(t)) and metabolic (CMRO2/CMRO20 -> m(t)) 
% response functions (see Table 1 in (Simon & Buxton, 2015)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nCoupling = 2;
    if nCoupling == 1
        % Use default values for ballon model assuming tight coupling of CBF,  
        % CMRO2, and CBVv dynamics (CMRO2.tau=2s, CBF.tau=2s, CBV.tau=0 s).
        BOLD_Model.SimulationPars.label = 'TightCoupling';
        BOLD_Model = setDefaultValues(BOLD_Model);
    elseif  nCoupling == 2
        % Use default values for ballon model assuming slower CBV response
        BOLD_Model.SimulationPars.label = 'SlowerCBV';          
        BOLD_Model = setDefaultValues(BOLD_Model);
        % transit time for blood through venous compartment
        BOLD_Model.CBV.tau0 = 2.0; % 2sec [0.75 sec - 2.5 sec]
        % characteristic time constant for CBF [variable]                     
        BOLD_Model.CBV.tau = 20.0; 
    end % if nCouplings
    
    BOLD_Model.CBF.tau = 2.0;
    BOLD_Model.CBF.amplitude = 1.5; 
    BOLD_Model.CMRO2.amplitude = 1.25;
    BOLD_Model.CMRO2.tau = 2.0;
   
    for nInput = 1:BOLD_Model.SimulationPars.nInputs
        S_BOLD(nInput) = SimulateBOLDsignal_nInputs(BOLD_Model,nInput);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % plot simulation results                                 %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        plotBOLDSimulationResults_nInputs(S_BOLD(nInput),nInput+3,nInput);
    end
    
save('S_BOLD_VARInputlength.mat', 'S_BOLD')


end

