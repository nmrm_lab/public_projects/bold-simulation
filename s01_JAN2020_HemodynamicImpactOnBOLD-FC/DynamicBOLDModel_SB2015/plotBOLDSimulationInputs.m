function plotBOLDSimulationInputs(BalloonModel, fignr, m1Val, f1Val)
% m1Val == f1Val == 0 -> use neuronal input '1' as reference
if m1Val== 0 && f1Val == 0
    nInput = 1;
else
    nInput = 2;
end

%plot time course variables
totalDur = BalloonModel.SimulationPars.totalDur;

figure(fignr); 

subplot(2,2,1);
plot(BalloonModel.NeuronalInput.ts(nInput),'b-','LineWidth',2)
% axis([ts_N.Time(1) ts_N.Time(size(ts_N.time,1)) -0.05 1.05])
maxval_y = 1.1*max(BalloonModel.NeuronalInput.ts(nInput).Data);
axis([0 totalDur -0.05 maxval_y])
% title('')
xlabel('time [sec]','Fontsize',11)
ylabel('neuronal input - N(t)','Fontsize',11)

subplot(2,2,2);
plot(BalloonModel.CBF.h,'b-','LineWidth',2)
hold on
plot(BalloonModel.CMRO2.h,'r-','LineWidth',2)
legend('h_{f}(t)','h_{m}(t)','FontSize',9)
maxval_y = 1.1*max(max(BalloonModel.CBF.h), max(BalloonModel.CMRO2.h));
axis([0 totalDur -0.1*maxval_y maxval_y])
title('')
xlabel('time [sec]','Fontsize',11)
ylabel('gamma variate - h(t)','Fontsize',11)
hold off

subplot(2,2,3);
plot(BalloonModel.CBF.fin,'b-','LineWidth',2)
axis([0 totalDur 0.9 1.7])
legend1 = sprintf('f1 = %1.2f',BalloonModel.CBF.amplitude);
legend2 = sprintf('T_f = %1.1f',BalloonModel.CBF.tau);
% legend3 = sprintf('T_v = %1.1f',BalloonModel.CBV.tau);
% legend4 = sprintf('a = %1.2f',BalloonModel.CBV.alpha);
text(40,1.62,legend1,'Fontsize',10);
text(40,1.49,legend2,'Fontsize',10);
% text(40,1.37,legend3,'Fontsize',10);
% text(40,1.25,legend4,'Fontsize',10);
title('')
xlabel('time [sec]','Fontsize',11)
ylabel('CBF input - f_{in}(t)','Fontsize',11)

subplot(2,2,4);
plot(BalloonModel.CMRO2.min,'b-','LineWidth',2)
axis([0 totalDur 0.9 1.7])
legend1 = sprintf('m1 = %1.2f',BalloonModel.CMRO2.amplitude);
legend2 = sprintf('T_m = %1.1f',BalloonModel.CMRO2.tau);
legend3 = sprintf('n = %1.1f',...
        (max(BalloonModel.CBF.fin)-1)/(max(BalloonModel.CMRO2.min)-1));
text(40,1.62,legend1,'Fontsize',10);
text(40,1.49,legend2,'Fontsize',10);
text(40,1.40,legend3,'Fontsize',10);
title('')
xlabel('time [sec]','Fontsize',11)
ylabel('CMRO2 input - m(t)','Fontsize',11)

FNout = fullfile(BalloonModel.SimulationPars.resultPath,...
        sprintf('Fig%d_%d_%s_%s_m1-%3.3f_Tm-%3.1f_f1-%3.3f_Tf-%3.1f_Tcbv-%3.1f_alpha-%1.3f_n-%3.1f.jpg',...
        m1Val,f1Val,...
        BalloonModel.SimulationPars.label,BalloonModel.NeuronalInput.label, ...
        BalloonModel.CMRO2.amplitude, BalloonModel.CMRO2.tau,...
        BalloonModel.CBF.amplitude, BalloonModel.CBF.tau, ...
        BalloonModel.CBV.tau, BalloonModel.CBV.alpha, ...
        BalloonModel.n.Prescribed));
saveas(gcf,FNout);
end

