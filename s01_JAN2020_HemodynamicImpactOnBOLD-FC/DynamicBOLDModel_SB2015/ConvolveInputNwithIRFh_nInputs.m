function BalloonModel = ConvolveInputNwithIRFh_nInputs(BalloonModel,nInput)
% Generate CBF or CMRO2 responses by convolution of a neuronal activation
% input N(t) with gamma variate impulse response function (IRF) h(t)
% according to Buxton et al. NI (2004); Eq. [13]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% select neuronal input
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin < 2
    nInput = 1;
end

for nHRFs = 1:2 % 1: generate CBF input; 2: generate CMRO2 input
    if nHRFs == 1    
        % convolve N(t) with gamma variate IRF h(t)
        % according to Buxton et al. NI (2004); Eq. [13]
        input = conv(squeeze(BalloonModel.CBF.h.Data),...
                     squeeze(BalloonModel.NeuronalInput.ts(nInput).Data));
        z = BalloonModel.CBF.z;
        tau = BalloonModel.CBF.tau;
        amplitude = BalloonModel.CBF.amplitude;
    elseif nHRFs == 2
        % convolve N(t) with gamma variate IRF h(t)
        % according to Buxton et al. NI (2004); Eq. [13]
        input = conv(squeeze(BalloonModel.CMRO2.h.Data),...
                     squeeze(BalloonModel.NeuronalInput.ts(nInput).Data));        
        z = BalloonModel.CMRO2.z;
        tau = BalloonModel.CMRO2.tau;
        amplitude = BalloonModel.CMRO2.amplitude;
    end
    
    % for small values of tau and z the scalefactor is given by deltaT
    if (z > 1 ) && (z < 13) && (tau < 13)
        scalefactor = BalloonModel.SimulationPars.deltaT;
    else % For larger values(that will probably never be used) deviations 
         % increase with z and tau --> Determine scalefactor to achieve a 
         % defined flow or CMRO2 response for a prolonged block type stimulus 
         % (amplitude defined by InputPars.amplitude)
        ts_prolongedBlock = blockInput;
        prolongedBlockResponse = ...
            conv(squeeze(ts_h.Data),squeeze(ts_prolongedBlock.Data));
        scalefactor = 1/max(prolongedBlockResponse);
    end
    % define time vector
    deltaT = BalloonModel.SimulationPars.deltaT;
    time = deltaT:deltaT:(deltaT*size(input,1));

    % Convert to timeseries and apply scaling
    if nHRFs == 1
        BalloonModel.CBF.fin = ...
            timeseries(1+(amplitude - 1)*input*scalefactor,time,...
            'Name',BalloonModel.CBF.Name);
    elseif nHRFs == 2
        BalloonModel.CMRO2.min = ...
            timeseries(1+(amplitude - 1)*input*scalefactor,time,...
            'Name',BalloonModel.CBF.Name);
    end
end
end

