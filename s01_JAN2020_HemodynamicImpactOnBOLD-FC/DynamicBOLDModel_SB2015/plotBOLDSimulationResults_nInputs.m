function plotBOLDSimulationResults_nInputs(BalloonModel,fignr, nInput)
% m1Val == f1Val == 0 -> use neuronal input '1' as reference
if nargin < 3
    nInput = 1;
end

% plot all results of Simulink BOLD Simulation
totalDur = BalloonModel.SimulationPars.totalDur;

figure(fignr); 

subplot(1,2,1);
plot(-30+BalloonModel.NeuronalInput.ts(nInput)*28,'k--','LineWidth',1)
hold on
plot(100*(BalloonModel.CBF.fin-1),'b-','LineWidth',2)
plot(100*(BalloonModel.CBF.fout-1),'c-','LineWidth',2)
plot(100*(BalloonModel.CMRO2.min-1),'r-','LineWidth',2)
plot(100*(BalloonModel.CBV.vout-1),'g','LineWidth',2)

legend('input','CBF_{in}','CBF_{out}','CMRO2','CBV_v','FontSize',9)
axis([0 totalDur -30 90])
title(BalloonModel.NeuronalInput.label)
legend1 = sprintf('f1 = %1.2f',BalloonModel.CBF.amplitude);
legend2 = sprintf('T_f = %1.2f',BalloonModel.CBF.tau);
legend3 = sprintf('m1 = %1.2f',BalloonModel.CMRO2.amplitude);
legend4 = sprintf('T_m = %1.2f',BalloonModel.CMRO2.tau);
legend5 = sprintf('T_v = %1.2f',BalloonModel.CBV.tau);
legend6 = sprintf('a = %1.3f',BalloonModel.CBV.alpha);
legend7 = sprintf('n = %1.1f',...
        (max(BalloonModel.CBF.fin)-1)/(max(BalloonModel.CMRO2.min)-1));
text(5,86,legend1,'Fontsize',10);
text(5,80,legend2,'Fontsize',10);
text(5,75,legend3,'Fontsize',10);
text(5,68,legend4,'Fontsize',10);
text(5,62,legend5,'Fontsize',10);
text(5,57,legend6,'Fontsize',10);
text(5,51,legend7,'Fontsize',10);
% yticks([1.0 1.2 1.4 1.6 1.8 2.0])
yticks([0 50])
ylabel('\Delta f / f_0 [%]')
% xticks(0:20:20*floor(totalDur/20))
xlabel('time [sec]')
ax = gca;
ax.FontSize = 10;
hold off

subplot(1,2,2);
plot(100*(BalloonModel.BOLD.bout-1),'b','LineWidth',2)
hold on
plot([0 totalDur], [0 0],'k--','LineWidth',1)
legend('model BOLD','FontSize',9)
axis([0 totalDur -3 4])
title(BalloonModel.SimulationPars.label)
% yticks([0.99 1.00 1.01 1.02 1.03 1.04])
yticks([-3 -2 -1 0 1 2 3 4])

ylabel('\Delta S_{BOLD} / S_0 [%]')
xlabel('time [sec]')                
ax = gca;
ax.FontSize = 10;
hold off

% save plot to file
if nargin > 1
    FNout = fullfile(BalloonModel.SimulationPars.resultPath,...
        sprintf('Fig%d_%d_%s_Response_m1-%3.3f_Tm-%3.1f_f1-%3.3f_Tf-%3.1f_n-%3.1f.jpg',...
        nInput,BalloonModel.SimulationPars.label, ...
        BalloonModel.CMRO2.amplitude, BalloonModel.CMRO2.tau,...
        BalloonModel.CBF.amplitude, BalloonModel.CBF.tau, ...
        BalloonModel.n.Prescribed));
    saveas(gcf,FNout);
end
end

