function BalloonModel = CallBalloonModel(BalloonModel,fignr,m1Val,f1Val)
% CP 26MAR2019
% Calculate driving functions (Inflow&CMRO2) & call simulink Balloon Model 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate BalloonModel.CBF.fin (=f(t)) and BalloonModel.CMRO2.min (=m(t))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin > 1
    % if fignr input argument exist --> input curves are plotted
    BalloonModel = CreateFlowCMRO2Input(BalloonModel, fignr,m1Val,f1Val);
else
    BalloonModel = CreateFlowCMRO2Input(BalloonModel);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform simulation --> Call balloon model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% required inputs for Balloon model:
% BalloonModel.CBF.fin:   input flow  (normalized to baseline = 1)
% BalloonModel.CMRO2.min: input CMRO2 (normalized to baseline = 1)
%--------------------------------------------------------------------------
% Set parameters in Simulink blocks 
%--------------------------------------------------------------------------
% INFO: Find block parameter names for setting block parameters 
% (uncomment the following two lines of code if needed)
%--------------------------------------------------------------------------
% load_system('BalloonModel_2015'); % --> list of Block paths
% BlockPaths = find_system('BalloonModel_2015','Type','Block')
%--------------------------------------------------------------------------
% tau0 transit time of blood through venous compartment: 2s [0.75s - 2.5s]
% needed for DeoxyHb and venous CBV module
MTT0 = num2str(1/BalloonModel.CBV.tau0);
set_param('BalloonModel_2015/Simulink Function/Resulting DeoxyHb q(t)/1//MTT0',...
            'Value',MTT0);
set_param('BalloonModel_2015/Simulink Function/Resulting venous CBV v(t)/1//MTT0',...
            'Value',MTT0);
% for selection of appropriate model for fout calculation because more 
% recent complex model (Simon and Buxton, 2015) does not converge for
% tau0 -> 0  
set_param('BalloonModel_2015/Simulink Function/tau0',...
            'Value', num2str(BalloonModel.CBV.tau0))         
%--------------------------------------------------------------------------
% Set parameters governing CBV response & viscoelastic properties
%--------------------------------------------------------------------------
% CBV.alpha: exponent describing steady state venous flow-volume coupling
% canonical value. alphaV = 0.38 (Grubb et al. 1974); 
% variable according to (Simon & Buxton, 2015)
% lower alphaV produces weaker CBV responses
set_param('BalloonModel_2015/Simulink Function/alphaV',...
                            'Value', num2str(BalloonModel.CBV.alpha))
% CBV.tau:   characteristic time constant for venous CBV [0 ... 30]
set_param('BalloonModel_2015/Simulink Function/tauV',....
                            'Value', num2str(BalloonModel.CBV.tau))
%--------------------------------------------------------------------------
% Set parameters governing BOLD response 
%--------------------------------------------------------------------------
% modeltype - 1: BOLD signal change (in %) according to Balloon model 
%                (Buxton et al. MRM, 1998); updated in (Buxton et al.,2004) 
%                according to (Obata et al., NI, 2004) with coefficients;
%                contains intravsacular CBV effects
% [default] - 2: BOLD signal change (in %) according to Simon & Buxton 
%                (NI, 2015); does not contain direct CBV effects
modeltype = BalloonModel.SimulationPars.modeltype;
set_param('BalloonModel_2015/Simulink Function/modeltype',...
                            'Value', modeltype)
% resting OEF; default OEF0 = 0.4; % (Leenders et al., 1990)
set_param('BalloonModel_2015/Simulink Function/OEF0',....
                            'Value', num2str(BalloonModel.BOLD.OEF0))
% resting venous CBV; default CBV0 = 2.0; % in %
set_param('BalloonModel_2015/Simulink Function/CBV0',....
                            'Value', num2str(BalloonModel.BOLD.CBV0))
% echo time; TE = 0.03; % sec                        
set_param('BalloonModel_2015/Simulink Function/TE',....
                            'Value', num2str(BalloonModel.BOLD.TE))                              
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Save input timeseries to file (to enable Simulink call within function)
%--------------------------------------------------------------------------
ts_fin = BalloonModel.CBF.fin;      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
save 'ts_fin.mat' -v7.3 ts_fin      % CAVE: Read input from workspace     %
ts_min = BalloonModel.CMRO2.min;    %       works only within scripts     %
save 'ts_min.mat' -v7.3 ts_min      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Call Simulink simulation module
%--------------------------------------------------------------------------
sim('BalloonModel_2015')
%-------------------------------------------------------------------------- 
% Incorporate model output into structure
%-------------------------------------------------------------------------- 
BalloonModel.CBF.fout = ts_fout;
BalloonModel.CBV.vout = ts_vout;
BalloonModel.BOLD.qout = ts_qout;
BalloonModel.BOLD.bout = ts_bout;
end

