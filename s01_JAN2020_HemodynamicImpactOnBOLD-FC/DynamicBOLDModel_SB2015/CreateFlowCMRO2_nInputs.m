function BalloonModel = CreateFlowCMRO2_nInputs(BalloonModel, fignr,nInput)
% CP 21FEB2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create input flow (ts_fin) and CMRO2 (ts_min) pattern as driving functions 
% for BOLD signal model (baseline flow  and CMRO2 fin0 = min0 = 1)
% according to Buxton et al. NI (2004); Eq. [13]
%
% NeuronalInput.ts :     arbitrary neuronal activity pattern 
%                        DEFAULT - blockInput with amplitude 1
% BalloonModel.CBF.h:    hemodynamic impulse response function 
% BalloonModel.CMRO2.h:  metabolic impulse response function 
%                        DEFAULT - gamma variate 
%                        (Buxton et al. NI,2004); Eq. [12]) 
% f1 :   flow (CBF) increase relative to baseline (f = 1)
%        DEFAULT - f1 = 1.5 --> 50 % flow increase
% n : flow metabolism coupling n = (f-1)/(m-1) 
%        (Buxton et al. NI (2004); Eq. [3])
%        DEFAULT - n = 2-3
%        [Experimental measurements find n = 2�3 (Davis et al., 1998; 
%        Hoge et al., 1999; Kastrup et al.,  2002; Marrett and Gjedde, 1997; 
%        Seitz and Roland, 1992), although larger values have also  been 
%        reported (Fox and Raichle, 1986; Kuwabara et al., 1992)]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate adapted Gamma Variate functions
% BalloonModel.CBF.h:    hemodynamic impulse response function 
% BalloonModel.CMRO2.h:  metabolic impulse response function 
BalloonModel = GammaVariateSB2015(BalloonModel);

% Calculation of driving inputs f(t) und m(t) for BOLD model
% BalloonModel.CBF.fin:    normalized flow input function 
% BalloonModel.CMRO2.min:  normalized metybolic input function
BalloonModel = ConvolveInputNwithIRFh_nInputs(BalloonModel,nInput);

if nargin > 1
    % Plot all generated response input functions
    plotBOLDSimulation_nInputs(BalloonModel, fignr,nInput)
end
end

