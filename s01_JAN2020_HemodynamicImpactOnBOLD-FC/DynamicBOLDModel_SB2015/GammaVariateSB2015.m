function BalloonModel = GammaVariateSB2015(BalloonModel)
% Gamma variate function serving as an impulse response function to obtain
% CBF and CMRO2 from simulated neuronal activity 
% Default settings according to Buxton et al. NI (2004); Eq. [12]
% resulting in a gamma variate function with FWHM of about 4 sec

% total duration [sec] of Gamma Variate function
GammaVariateDur = 120;

for nHRFs = 1:2
    if nargin < 1
        %CAVE: temporal resolution needs to be identical for N(t) and h(t)
        deltaT = 0.01; %sec
        tau = 2; % 0.5*FWHM [sec] [default 2]
        z = 3;% integer constant
    else
        deltaT = BalloonModel.SimulationPars.deltaT;
        if nHRFs == 1 
            tau = BalloonModel.CBF.tau;
            z = BalloonModel.CBF.z;
        elseif nHRFs == 2
            tau = BalloonModel.CMRO2.tau;
            z = BalloonModel.CMRO2.z;            
        end
    end
    % definition of time vector
    time = 0:deltaT:GammaVariateDur;

    % calculation of '(z-1)!'
    zFakultaet = 1;
    k = z;
    while k > 1
        k = k-1;
        zFakultaet = zFakultaet * k;
    end

    %calculation of gamma variate function

    h =   ( (time/tau).^(z-1) .* exp(-time/tau) ) / (tau*zFakultaet);
    
    if nHRFs == 1
        BalloonModel.CBF.h = timeseries(h,time,'Name','hf - GammaVariate RF');
    elseif nHRFs == 2
        BalloonModel.CMRO2.h = timeseries(h,time,'Name','hm - GammaVariate RF');
    end
end %for nHRFs
end
