%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 12.02.2020, Christine Preibisch
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function S_BOLD = SimulateBOLDsignal_nInputs(BOLD_Model, nInput)
% CP 26MAR19
% simulate BOLD response using Balloon model (Buxton et al. MRM, 1998;
% Buxton et al. NI, 2004; Simon & Buxton, NI 2015) for intrisic neuronal
% signals

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extract nominal CBF/CMRO2 coupling ratio derived from prescribed
% CBF and CMRO2 amplitudes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
BOLD_Model.n.Prescribed = (BOLD_Model.CBF.amplitude-1)/...
                          (BOLD_Model.CMRO2.amplitude-1); 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Call Balloon Model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fignr = nInput;
S_BOLD = CallBalloonModel_nInputs(BOLD_Model,fignr,nInput);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extract actual CBF/CMRO2 coupling ratio derived from resulting 
% maximum CBF and CMRO2 amplitudes (influenced by delay constants 
% and stimulus timings)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
S_BOLD.n.Actual = (max(S_BOLD.CBF.fin)-1)/...
                  (max(S_BOLD.CMRO2.min)-1);        
end

