#
# Main changes in BOLD Simulations on 24.07.2018
#
# CP 02JUL19
# modified to allow for a seperate neuronal input signal that serves as a
# reference (should be simulated to electrophysiologically connected to the
# test region with electrophysiological impairment). 
# - NeuronalInput '1' will be used as reference.
# - Reference CBF (f1) und CMRO2 (m1) response  
#
# Main function 'RUN_SimulateImpairedFC' calls subfunctions to allow the
# simulation of several scenarios how impaired physiology impacts on
# intrinsic functional connectivity (iFC)
#
#    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#    %%% 12.07.2019, Christine Preibisch
#    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#    % Matlab functions to perform simulations of the impact of impaired  
#    % physiology on functional connectivity (FC) via simulation of BOLD signal
#    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#    % 1) Influence of variable CMRO2 (m1) and CBF (f1) amplitudes on FC is 
#    %    simulated by
#    %         - SimulateImpairedFC_VARm1_VARf1(BOLD_Model)
#    %    		By default, simulations can be performed 
#    %    		for 'ThightCoupling' or 'SlowerCBV'.
#    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#    BOLD_Model.SimulationPars.resultPath = ...
#        fullfile(save_path,'VARm1_VARf1',BOLD_Model.SimulationPars.label); 
#    SimulateImpairedFC_VARm1_VARf1(BOLD_Model)

#    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#    %2) Influence of delayed CBF (CBF.tau) or CMRO2 (CMRO2.tau) response  
#    %   is simulated by
#    %        - SimulateImpairedFC_VARdelays(BOLD_Model)
#    %   		By default, simulations can be performed for 
#    %   		'TightCoupling' or 'SlowerCBV'.
#    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#    BOLD_Model.SimulationPars.resultPath = ...
#        fullfile(save_path,'VARdelays',BOLD_Model.SimulationPars.label);
#    SimulateImpairedFC_VARdelays(BOLD_Model)

#    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#    % 3) Influence of veriable CBV response (CBV.tau, CBV.alpha) is 
#    %    simulated by
#    %         - SimulateImpairedFC_VARcbv(BOLD_Model)
#    %    By default, simulations can be performed for a reference signal  
#    %    with 'ThightCoupling' or 'SlowerCBV'.
#    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#    BOLD_Model.SimulationPars.resultPath = ...
#        fullfile(save_path,'VARcbv',BOLD_Model.SimulationPars.label);
#    SimulateImpairedFC_VARcbv(BOLD_Model)