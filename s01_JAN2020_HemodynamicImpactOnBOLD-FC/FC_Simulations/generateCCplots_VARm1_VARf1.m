function generateCCplots_VARm1_VARf1(S_BOLD, CC)
% CP 29MAR19 Plot CC arrays
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% convenience definitions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% temporal resolution of simulated BOLD signal curves [sec]
SampleTime = S_BOLD.VARm1_VARf1(1,1).SimulationPars.SampleTime; 
% simulated time period
totalDur = S_BOLD.VARm1_VARf1(1,1).SimulationPars.totalDur; 
% total duration of initial block stimulus
blockDur = S_BOLD.VARm1_VARf1(1,1).SimulationPars.blockDur;
% number of prescribed CMRO2 values
m1Nsteps = S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Nsteps;
% minimum of prescribed CMRO2 values
m1Min = S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Min;
% step size for CMRO2 changes
m1StepSize = S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1StepSize;
% max CBF value
m1Max = m1Min + (m1Nsteps-1) * m1StepSize;
% minimum of prescribed CBF values
f1Min = S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1Min;
% number of prescribed CBF values
f1NSteps = S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1NSteps; 
% step size for CBF changes
f1StepSize = S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1StepSize; 
% max CBF value
f1Max = f1Min + (f1NSteps-1) * f1StepSize;

% allow for (ninterval+1) x & y AxisTicks
ninterval = 3;

% SNR matrix size --> sqrt(number of different random noise realizations)
% --> create SmS x SmS patches in CC maps with same SNR but different noise
%     realizations
SmS = S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNRmatrixSize;
% Border width between SmS x SmS patches with same SNR
% gap = S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNRgapSize;
gap = S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNRgapSize;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%   Plot CC arrays created from BOLD TCs with different SNRs    %%%%%%
%%%%%%  (SNR array size = 1)                                         %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(10); 
    % Global settings
    FigureProps.GlobalTitleStr = ...
        sprintf('Simulated FC(m1, f1) @%s for different SNR',...
                        S_BOLD.reference.SimulationPars.label);
    FigureProps.cFsize = 1.1; % Fontsize 12 pt 
    FigureProps.NewColorMap = gray;
    FigureProps.TitleStr = ''; % title for subplots
    FigureProps.xAxisTick = m1Min:round(m1Nsteps/ninterval):m1Nsteps;
    FigureProps.yAxisTick = f1Min:round(f1NSteps/ninterval):f1NSteps;  
subplot(3,4,1)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = m1Min;
    FigureProps.maxVal = m1Max;
    FigureProps.TitleStr = 'rel. CMRO2 increase (m1)';

	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',m1Min + (count-1)*((m1Max-m1Min)/ninterval));
	end
    FigureProps.xAxisLabel = 'm1';
	for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = '';
	end
    FigureProps.yAxisLabel = '';
    newdisp_FC(CC.varSNR.m1Values, FigureProps)
subplot(3,4,2)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = f1Min;
    FigureProps.maxVal = f1Min + f1StepSize*(f1NSteps-1);
	for count = 1:size(FigureProps.xAxisTick,2)    
        FigureProps.xAxisTickLabel{count} = '';
	end
    FigureProps.xAxisLabel = '';
    yAxTicks = f1NSteps:-round(f1NSteps/ninterval):f1Min; % axis is inverted!
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
        	sprintf('%1.2f',f1Max - (count-1)*((f1Max-f1Min)/ninterval));
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % set axis for all other plots
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    FigureProps.yAxisLabel = 'f1';     
    FigureProps.TitleStr = 'rel. CBF increase (f1)';     
    newdisp_FC(CC.varSNR.f1Values, FigureProps)   
    for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
            sprintf('%1.1f',m1Min + (count-1)*((m1Max-m1Min)/ninterval));
    end
    FigureProps.xAxisLabel = 'm1';
    yAxTicks = f1NSteps:-round(f1NSteps/ninterval):f1Min; % axis is inverted!
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.2f',f1Max - (count-1)*((f1Max-f1Min)/ninterval));
    end
    FigureProps.yAxisLabel = 'f1';      
subplot(3,4,3)
    FigureProps.cLabel = 'n = (f1-1)/(m1-1)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 5;
%     FigureProps.minVal = min(CC.varSNR.nActual(:));
%     FigureProps.maxVal = max(CC.varSNR.nActual(:));    
    FigureProps.TitleStr = 'prescribed coupling ratio';     
    newdisp_FC(CC.varSNR.nPrescribed, FigureProps)
subplot(3,4,4)
    FigureProps.cLabel = 'n = (f1-1)/(m1-1)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 5;
%     FigureProps.minVal = min(CC.varSNR.nActual(:));
%     FigureProps.maxVal = max(CC.varSNR.nActual(:));    
    FigureProps.TitleStr = 'actual coupling ratio';     
    newdisp_FC(CC.varSNR.nActual, FigureProps)    
subplot(3,4,5)
    FigureProps.cLabel = 'CC'; 
%     FigureProps.minVal = -0.5;
%     FigureProps.maxVal = 0.5;
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_1000(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_1000(:));
    FigureProps.TitleStr = 'CC @SNR_0 = 1000';      
    newdisp_FC(CC.varSNR.RHO_SNR0_1000, FigureProps)    
subplot(3,4,6)
    FigureProps.cLabel = 'CC'; 
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_500(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_500(:));
    FigureProps.TitleStr = 'CC @SNR_0 = 500'; 
    newdisp_FC(CC.varSNR.RHO_SNR0_500, FigureProps)
subplot(3,4,7)
    FigureProps.cLabel = 'CC'; 
     FigureProps.minVal = min(CC.varSNR.RHO_SNR0_250(:));
     FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_250(:));
    FigureProps.TitleStr = 'CC @SNR_0 = 250'; 
    newdisp_FC(CC.varSNR.RHO_SNR0_250, FigureProps)
subplot(3,4,8)
    FigureProps.cLabel = 'CC'; 
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_125(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_125(:));
    FigureProps.TitleStr = 'CC @SNR_0 = 125';     
    newdisp_FC(CC.varSNR.RHO_SNR0_125, FigureProps)    
subplot(3,4,9)
    FigureProps.cLabel = 'p value'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;    
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_1000(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_1000(:));
    FigureProps.TitleStr = 'p value @SNR_0 = 1000';      
    newdisp_FC(CC.varSNR.PVAL_SNR0_1000, FigureProps)    
subplot(3,4,10)
    FigureProps.cLabel = 'p value'; 
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_500(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_500(:));
    FigureProps.TitleStr = 'p value @SNR_0 = 500'; 
    newdisp_FC(CC.varSNR.PVAL_SNR0_500, FigureProps)
subplot(3,4,11)
    FigureProps.cLabel = 'p value'; 
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_250(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_250(:));
    FigureProps.TitleStr = 'p value @SNR_0 = 250'; 
    newdisp_FC(CC.varSNR.PVAL_SNR0_250, FigureProps)  
subplot(3,4,12)
    FigureProps.cLabel = 'p value'; 
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_125(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_125(:));
    FigureProps.TitleStr = 'p value @SNR_0 = 125';     
    newdisp_FC(CC.varSNR.PVAL_SNR0_125, FigureProps) 
    
% save plot to file
FNout = fullfile(S_BOLD.reference.SimulationPars.resultPath,...
	sprintf('VAR_m1-%3.2f-%3.2f_f1-%3.2f-%3.2f_CC_%s_varSNR.jpg',...
            m1Min,m1Max,f1Min,f1Max,S_BOLD.reference.SimulationPars.label));
saveas(gcf,FNout);    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%   Plot CC array created from BOLD TCs with same SNRs          %%%%%%
%%%%%%  (SNR array size = BOLD_Model.SimulationPars.SNRmatrixSize)   %%%%%%
%%%%%%  but different random number realizations                     %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(11); 
    % Global settings %%%%% 
    FigureProps.GlobalTitleStr = ...
        sprintf('Simulated FC(m1, f1) @SNR=%3.0f & %s; [%3.0fx%3.0f] random noise realizations',...
        S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNR0,...
        S_BOLD.reference.SimulationPars.label,SmS,SmS);
    FigureProps.cFsize = 1.1; % Fontsize 12 pt 
    FigureProps.NewColorMap = gray;
    FigureProps.TitleStr = ''; % title for subplots
    FigureProps.xAxisTick = m1Min+(SmS+gap)/2:...
                    (SmS+gap)*round(m1Nsteps/ninterval):m1Nsteps*(SmS+gap);
    FigureProps.yAxisTick = f1Min+(SmS+gap)/2:...
                    (SmS+gap)*round(m1Nsteps/ninterval):f1NSteps*(SmS+gap); 
subplot(2,3,1)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = m1Min;
    FigureProps.maxVal = m1Max;
    FigureProps.TitleStr = 'rel. CMRO2 increase (m1)';
    % label xAxisTicks with M1 values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.2f',m1Min + (count-1)*((m1Max-m1Min)/ninterval));
	end
    FigureProps.xAxisLabel = 'm1';
    % no Ticks and label for y axis
	for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = '';
	end
    FigureProps.yAxisLabel = '';
    newdisp_FC(CC.SNRmatrix.m1Values, FigureProps)
    
subplot(2,3,2)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = f1Min;
    FigureProps.maxVal = f1Max;
    % no Ticks and label for x axis
	for count = 1:size(FigureProps.xAxisTick,2)    
        FigureProps.xAxisTickLabel{count} = '';
	end
    FigureProps.xAxisLabel = '';
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.2f',f1Max - (count-1)*((f1Max-1)/ninterval));
    end
    FigureProps.yAxisLabel = 'f1';     
    FigureProps.TitleStr = 'rel. CBF increase (f1)';     
    newdisp_FC(CC.SNRmatrix.f1Values, FigureProps)       
    
subplot(2,3,3)
    FigureProps.cLabel = 'n = (f1-1)/(m1-1)'; 
    FigureProps.minVal = min(CC.varSNR.nActual(:));
    FigureProps.maxVal = max(CC.varSNR.nActual(:));
    FigureProps.TitleStr = 'prescribed coupling ratio (n)'; 
    % label xAxisTicks with M1 values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.2f',m1Min + (count-1)*((m1Max-m1Min)/ninterval));
	end
    FigureProps.xAxisLabel = 'm1';
    newdisp_FC(CC.SNRmatrix.nPrescribed, FigureProps)

subplot(2,3,4)
    FigureProps.cLabel = 'n = (f1-1)/(m1-1)'; 
    FigureProps.minVal = min(CC.varSNR.nActual(:));
    FigureProps.maxVal = max(CC.varSNR.nActual(:));
    FigureProps.TitleStr = 'actual coupling ratio (n)'; 
    % label xAxisTicks with M1 values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.2f',m1Min + (count-1)*((m1Max-m1Min)/ninterval));
	end
    FigureProps.xAxisLabel = 'm1';
    newdisp_FC(CC.SNRmatrix.nActual, FigureProps)    

subplot(2,3,5)
    FigureProps.cLabel = 'CC (m1, f1)';
% 	FigureProps.minVal = -0.5;
% 	FigureProps.maxVal = 0.5; 
    FigureProps.minVal = min(CC.SNRmatrix.RHO_SNR(:));
    FigureProps.maxVal = max(CC.SNRmatrix.RHO_SNR(:));
    FigureProps.TitleStr = sprintf('CC @SNR = %3.0f',...
                     S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNR0);    
    newdisp_FC(CC.SNRmatrix.RHO_SNR, FigureProps)
    
subplot(2,3,6)
    FigureProps.cLabel = 'p value (m1, f1)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(CC.SNRmatrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(CC.SNRmatrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('p values @SNR = %3.0f',...
                     S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNR0);    
    newdisp_FC(CC.SNRmatrix.PVAL_SNR, FigureProps)
        
        
    % save plot to file
    FNout = fullfile(S_BOLD.reference.SimulationPars.resultPath,...
    	sprintf('VAR_m1-%3.2f-%3.2f_f1-%3.2f-%3.2f_CC_%s_matrixSNR-%3.0f.jpg',...
        m1Min,m1Max,f1Min,f1Max,S_BOLD.reference.SimulationPars.label,...
        S_BOLD.VARm1_VARf1(1,1).SimulationPars.SNR0));
    saveas(gcf,FNout);    
end

