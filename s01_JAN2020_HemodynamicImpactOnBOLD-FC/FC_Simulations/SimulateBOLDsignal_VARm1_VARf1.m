%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 10.07.2019, Christine Preibisch
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Matlab functions to perform simulations of the impact of impaired  
% physiology on functional connectivity (FC) via simulation of BOLD signal
% 
% 1) Influence of variable CMRO2 (m1) and CBF (f1) amplitudes on FC is 
%    simulated by
%         - SimulateImpairedFC_VARm1_VARf1(save_path)
%         - SimulateBOLDsignal_VARm1_VARf1(BOLD_Model)
%    By default, simulations can be performed for 'ThightCoupling' or 
%    'SlowerCBV'.

function S_BOLD = SimulateBOLDsignal_VARm1_VARf1(BOLD_Model)
% CP 26MAR19
% simulate BOLD response using Balloon model (Buxton et al. MRM, 1998;
% Buxton et al. NI, 2004; Simon & Buxton, NI 2015) for intrisic neuronal
% signals
%
% CP 02JUL19
% modified to allow for a seperate neuronal input signal that serves as a
% reference (should be simulated to electrophysiologically connected to the
% test region with electrophysiological impairment). NeuronalInput '1' will
% be used as reference
%
% Define local variables (for convenience)
m1Nsteps = BOLD_Model.SimulationPars.m1Nsteps;
f1NSteps = BOLD_Model.SimulationPars.f1NSteps;
m1Min = BOLD_Model.SimulationPars.m1Min;
m1StepSize = BOLD_Model.SimulationPars.m1StepSize;
f1Min = BOLD_Model.SimulationPars.f1Min;
f1StepSize = BOLD_Model.SimulationPars.f1StepSize;
SNR0 = BOLD_Model.SimulationPars.SNR0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulate BOLD response for reference neuronal input (#1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % define reference CMRO2 (m1) and CBF (f1) amplitudes
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        BOLD_Model.CMRO2.amplitude = 1.25; % CMRO2 response amplitude 
        BOLD_Model.CBF.amplitude = 1.5; % CBF response amplitude 
        
        m1Val = 0; % values are used to identify function call for 
        f1Val = 0; % calculation of reference signal
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Extract nominal CBF/CMRO2 coupling ratio derived from prescribed
        % CBF and CMRO2 amplitudes
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        BOLD_Model.n.Prescribed = (BOLD_Model.CBF.amplitude-1)/...
                                  (BOLD_Model.CMRO2.amplitude-1);        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Call Balloon Model
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        S_BOLD.reference = CallBalloonModel(BOLD_Model,1,m1Val,f1Val); 

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Extract actual CBF/CMRO2 coupling ratio derived from resulting 
        % maximum CBF and CMRO2 amplitudes (influenced by delay constants 
        % and stimulus timings)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        S_BOLD.reference.n.Actual = (max(S_BOLD.reference.CBF.fin)-1)/...
                                    (max(S_BOLD.reference.CMRO2.min)-1);        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Add noise to the simulated time course
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        S_BOLD.reference.BOLD.boutNoise = ...
                AddNoise(S_BOLD.reference.BOLD.bout,1/SNR0);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Extract rs-fMRI time course (cut initial block stimulation period)
        % (with and without noise)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
        % number  of total time points
        npts = size(S_BOLD.reference.BOLD.bout.Data,1); 
        % index where rs-fMRI signal starts
%         % start after block end        
%         rsStart = ceil(npts*S_BOLD.reference.SimulationPars.blockDur/...
%                             S_BOLD.reference.SimulationPars.totalDur);
        % start 200 sec before end (to cut of initial transients)
        rsStart = ceil(npts*(S_BOLD.reference.SimulationPars.totalDur-200)/...
                                    S_BOLD.reference.SimulationPars.totalDur);
        S_BOLD.reference.BOLD.rs_boutNoise = ...
            timeseries(S_BOLD.reference.BOLD.boutNoise.Data(rsStart:npts),...
                       S_BOLD.reference.BOLD.boutNoise.Time(rsStart:npts),...
                       'Name','noisy rs-BOLD');
        S_BOLD.reference.BOLD.rs_bout = ...
            timeseries(S_BOLD.reference.BOLD.bout.Data(rsStart:npts),...
                       S_BOLD.reference.BOLD.bout.Time(rsStart:npts),...
                       'Name','noisy rs-BOLD');            
        plotBOLDSimulationResults(S_BOLD.reference,3,0,0); 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulate BOLD response resulting from different CMRO2 amplitudes (m1)
% and CBF amplitudes (f1) (CBF/CMRO2 coupling ratio n = (f1-1)/(m1-1))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Step through different parameter
for m1Val = 1:m1Nsteps 
    for f1Val = 1:f1NSteps       
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % introduce variable CMRO2 (m1) and CBF (f1) amplitudes
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        m1 = m1Min + m1StepSize * (m1Val-1); % 1st value -> no response
        f1 = f1Min + f1StepSize * (f1Val-1); % 1st value -> no response
        BOLD_Model.CMRO2.amplitude = m1; % CMRO2 response amplitude 
        BOLD_Model.CBF.amplitude = f1; % CBF response amplitude 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Extract nominal CBF/CMRO2 coupling ratio derived from prescribed
        % CBF and CMRO2 amplitudes
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        BOLD_Model.n.Prescribed = (BOLD_Model.CBF.amplitude-1)/...
                                  (BOLD_Model.CMRO2.amplitude-1); 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Call Balloon Model
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        S_BOLD.VARm1_VARf1(m1Val,f1Val) = ...
            CallBalloonModel(BOLD_Model,2,m1Val,f1Val);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Extract actual CBF/CMRO2 coupling ratio derived from resulting 
        % maximum CBF and CMRO2 amplitudes (influenced by delay constants 
        % and stimulus timings)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        S_BOLD.VARm1_VARf1(m1Val,f1Val).n.Actual = ...
                        (max(S_BOLD.VARm1_VARf1(m1Val,f1Val).CBF.fin)-1)/...
                        (max(S_BOLD.VARm1_VARf1(m1Val,f1Val).CMRO2.min)-1);        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Add noise to the simulated time course
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        S_BOLD.VARm1_VARf1(m1Val,f1Val).BOLD.boutNoise = ...
                AddNoise(S_BOLD.VARm1_VARf1(m1Val,f1Val).BOLD.bout,1/SNR0);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Extract rs-fMRI time course (cut initial block stimulation period)
        % (with and without noise)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
        S_BOLD.VARm1_VARf1(m1Val,f1Val).BOLD.rs_boutNoise = ...
            timeseries(S_BOLD.VARm1_VARf1(m1Val,f1Val).BOLD.boutNoise.Data(rsStart:npts),...
                       S_BOLD.VARm1_VARf1(m1Val,f1Val).BOLD.boutNoise.Time(rsStart:npts),...
                       'Name','noisy rs-BOLD');
        S_BOLD.VARm1_VARf1(m1Val,f1Val).BOLD.rs_bout = ...
            timeseries(S_BOLD.VARm1_VARf1(m1Val,f1Val).BOLD.bout.Data(rsStart:npts),...
                       S_BOLD.VARm1_VARf1(m1Val,f1Val).BOLD.bout.Time(rsStart:npts),...
                       'Name','noisy rs-BOLD');        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Plot Summary of simulation results
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        plotBOLDSimulationResults(S_BOLD.VARm1_VARf1(m1Val,f1Val),3,...
                                                     m1Val,f1Val); 
    end %for f1Val
end % for m1Val
save S_BOLD S_BOLD

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot summary of simulation results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
plotSummary_VARm1_VARf1(S_BOLD.VARm1_VARf1,4)
end

