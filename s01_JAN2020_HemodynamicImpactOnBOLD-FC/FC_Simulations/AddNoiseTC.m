function noisyTC = AddNoiseTC(TC, sigma)
% Generate random noise to achieve a given standard deviation of noise
% (sigma) resulting in a defined temporal SNR (tSNR) = 1/sigma  for a BOLD
% time series normalized to '1' at baseline

% set default -> SNR = 100 @ mu = 1
if nargin < 2
    sigma = 0.01;
end

% rng('shuffle') seeds the random number generator based on the current 
% time. Thus, rand, randi, and randn produce a different sequence of 
% numbers after each time rng is called.
rng('shuffle')
npoints = size(TC,1);

% Create probability distribution object with parameter values mu and sigma
% mu = baseline BOLD signal, default '1' 
pd = makedist('Normal','mu',1,'sigma',sigma);
noise = random(pd,[1 npoints]); 

% apply noise vector to achieve prescribed SNR
% CAVE: works only for BOLD signal with mean value '1' at baseline
noisyTC = TC.*transpose(noise);

end

