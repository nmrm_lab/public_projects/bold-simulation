%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 12.07.2019, Christine Preibisch
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function SimulateImpairedFC_VARdelays(BOLD_Model_VARdelays)
% CP 26MAR19
% simulate functional connectivity between 'intrinsic' BOLD responses 
% calculated from simulated neuronal inputs using Balloon model 
% (Buxton et al. MRM, 1998;Buxton et al. NI, 2004; Simon & Buxton, NI 2015)
%
% CP 02JUL19
% modified to allow for a seperate neuronal input signal that serves as a
% reference (should be simulated to electrophysiologically connected to the
% test region with electrophysiological impairment). 
% - NeuronalInput '1' will be used as reference.
% - Reference CBF (f1) und CMRO2 (m1) response  
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set global parameters for simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% minimum of prescribed CMRO2.tau values
BOLD_Model_VARdelays.SimulationPars.m1TauMin = 0.5; % TTP = 2*tau
% number of prescribed CMRO2.tau values % Note: output plots expect 16 steps
BOLD_Model_VARdelays.SimulationPars.m1TauNsteps = 16; 
% step size for CMRO2.tau changes
BOLD_Model_VARdelays.SimulationPars.m1TauStepSize = 0.5;
% minimum of prescribed CBF.tau values
BOLD_Model_VARdelays.SimulationPars.f1TauMin = 0.5; % TTP = 2*tau
% number of prescribed CBF.tau values % Note: output plots expect 16 steps
BOLD_Model_VARdelays.SimulationPars.f1TauNsteps = 16; 
% step size for CBF changes
BOLD_Model_VARdelays.SimulationPars.f1TauStepSize = 0.5;  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Call simulation of BOLD response for specified range of CMRO2 (-> m1)
% and CBF (-> f1) amplitudes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
S_BOLD_VARdelays = SimulateBOLDsignal_VARdelays(BOLD_Model_VARdelays);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Correlate noisy time courses & plot CC arrays @differenr SNR [1000...125]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CC_VARdelays.varSNR = CorrelateS_BOLD_VARdelays_varSNR(S_BOLD_VARdelays);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Correlate noisy time courses at given SNR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CC_VARdelays.SNR1000matrix = ...
                CorrelateS_BOLD_VARdelays_SNRmatrix(S_BOLD_VARdelays,1000);
CC_VARdelays.SNR500matrix = ...
                CorrelateS_BOLD_VARdelays_SNRmatrix(S_BOLD_VARdelays,500);
CC_VARdelays.SNR250matrix = ...
                CorrelateS_BOLD_VARdelays_SNRmatrix(S_BOLD_VARdelays,250);
CC_VARdelays.SNR125matrix = ...
                CorrelateS_BOLD_VARdelays_SNRmatrix(S_BOLD_VARdelays,125);
% Define default to be compatible with previous version plot programs            
CC_VARdelays.SNRmatrix = CC_VARdelays.SNR250matrix;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot CC arrays 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
generateCCplots_VARdelays(S_BOLD_VARdelays, CC_VARdelays) 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% save results to deicated folder
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
save(fullfile(BOLD_Model_VARdelays.SimulationPars.resultPath,...
                                            'SimulationResults.mat'));
end