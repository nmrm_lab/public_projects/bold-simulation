function generateCCplots_VARdelays(S_BOLD, CC)
% CP 29MAR19 Plot CC arrays
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% convenience definitions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% temporal resolution of simulated BOLD signal curves [sec]
SampleTime = S_BOLD.VARdelays(1,1).SimulationPars.SampleTime; 
% simulated time period
totalDur = S_BOLD.VARdelays(1,1).SimulationPars.totalDur; 
% total duration of initial block stimulus
blockDur = S_BOLD.VARdelays(1,1).SimulationPars.blockDur;
% number of prescribed CMRO2 values
m1TauNsteps = S_BOLD.VARdelays(1,1).SimulationPars.m1TauNsteps;
% minimum of prescribed CMRO2 values
m1TauMin = S_BOLD.VARdelays(1,1).SimulationPars.m1TauMin;
% step size for CMRO2 changes
m1TauStepSize = S_BOLD.VARdelays(1,1).SimulationPars.m1TauStepSize;
% max CBF value
m1TauMax = m1TauMin + (m1TauNsteps-1) * m1TauStepSize;
% minimum of prescribed CBF values
f1TauMin = S_BOLD.VARdelays(1,1).SimulationPars.f1TauMin;
% number of prescribed CBF values
f1TauNsteps = S_BOLD.VARdelays(1,1).SimulationPars.f1TauNsteps; 
% step size for CBF changes
f1TauStepSize = S_BOLD.VARdelays(1,1).SimulationPars.f1TauStepSize; 
% max CBF value
f1TauMax = f1TauMin + (f1TauNsteps-1) * f1TauStepSize;

% allow for (ninterval+1) x & y AxisTicks
ninterval = 5;

% SNR matrix size --> sqrt(number of different random noise realizations)
% --> create SmS x SmS patches in CC maps with same SNR but different noise
%     realizations
SmS = S_BOLD.VARdelays(1,1).SimulationPars.SNRmatrixSize;
% Border width between SmS x SmS patches with same SNR
% gap = S_BOLD.VARdelays(1,1).SimulationPars.SNRgapSize;
gap = S_BOLD.VARdelays(1,1).SimulationPars.SNRgapSize;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%   Plot CC arrays created from BOLD TCs with different SNRs    %%%%%%
%%%%%%  (SNR array size = 1)                                         %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(10); 
    % Global settings
    FigureProps.GlobalTitleStr = ...
        sprintf('Simulated FC(T_m, T_f) @%s for different SNR',...
                            S_BOLD.reference.SimulationPars.label);
    FigureProps.cFsize = 1.1; % Fontsize 12 pt 
    FigureProps.NewColorMap = gray;
    FigureProps.TitleStr = ''; % title for subplots
    FigureProps.xAxisTick = m1TauMin:round(m1TauNsteps/ninterval):m1TauNsteps;
    FigureProps.yAxisTick = f1TauMin:round(f1TauNsteps/ninterval):f1TauNsteps;  
subplot(3,4,1)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = m1TauMin;
    FigureProps.maxVal = m1TauMax;
    FigureProps.TitleStr = 'CMRO2 temporal delay (T_m)';

	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',m1TauMin + (count-1)*((m1TauMax-m1TauMin)/ninterval));
	end
    FigureProps.xAxisLabel = 'T_m';
	for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = '';
	end
    FigureProps.yAxisLabel = '';
    newdisp_FC(CC.varSNR.TmValues, FigureProps)
subplot(3,4,2)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = f1TauMin;
    FigureProps.maxVal = f1TauMin + f1TauStepSize*(f1TauNsteps-1);
	for count = 1:size(FigureProps.xAxisTick,2)    
        FigureProps.xAxisTickLabel{count} = '';
	end
    FigureProps.xAxisLabel = '';
    yAxTicks = f1TauNsteps:-round(f1TauNsteps/ninterval):f1TauMin; % axis is inverted!
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
        	sprintf('%1.2f',f1TauMax - (count-1)*((f1TauMax-f1TauMin)/ninterval));
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % set axis for all other plots
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    FigureProps.yAxisLabel = 'T_f';     
    FigureProps.TitleStr = 'CBF temporal delay (T_f)';     
    newdisp_FC(CC.varSNR.TfValues, FigureProps)   
    for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
            sprintf('%1.1f',m1TauMin + (count-1)*((m1TauMax-m1TauMin)/ninterval));
    end
    FigureProps.xAxisLabel = 'T_m';
    yAxTicks = f1TauNsteps:-round(f1TauNsteps/ninterval):f1TauMin; % axis is inverted!
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.2f',f1TauMax - (count-1)*((f1TauMax-f1TauMin)/ninterval));
    end
    FigureProps.yAxisLabel = 'T_f';      
subplot(3,4,3)
    FigureProps.cLabel = 'n = (f1-1)/(m1-1)'; 
    FigureProps.minVal = min(CC.varSNR.nActual(:));
    FigureProps.maxVal = max(CC.varSNR.nActual(:));
%     FigureProps.minVal = min(CC.varSNR.nActual(:));
%     FigureProps.maxVal = max(CC.varSNR.nActual(:));
    FigureProps.TitleStr = 'prescribed coupling ratio';     
    newdisp_FC(CC.varSNR.nPrescribed, FigureProps)
subplot(3,4,4)
    FigureProps.cLabel = 'n = (f1-1)/(m1-1)'; 
    FigureProps.minVal = min(CC.varSNR.nActual(:));
    FigureProps.maxVal = max(CC.varSNR.nActual(:));
%     FigureProps.minVal = min(CC.varSNR.nActual(:));
%     FigureProps.maxVal = max(CC.varSNR.nActual(:));
    FigureProps.TitleStr = 'actual coupling ratio';     
    newdisp_FC(CC.varSNR.nActual, FigureProps)
subplot(3,4,5)
    FigureProps.cLabel = 'CC'; 
%     FigureProps.minVal = -0.5;
%     FigureProps.maxVal = 0.5;
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_1000(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_1000(:));
    FigureProps.TitleStr = 'CC @SNR_0 = 1000';      
    newdisp_FC(CC.varSNR.RHO_SNR0_1000, FigureProps)    
subplot(3,4,6)
    FigureProps.cLabel = 'CC'; 
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_500(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_500(:));
    FigureProps.TitleStr = 'CC @SNR_0 = 500'; 
    newdisp_FC(CC.varSNR.RHO_SNR0_500, FigureProps)
subplot(3,4,7)
    FigureProps.cLabel = 'CC'; 
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_250(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_250(:));
    FigureProps.TitleStr = 'CC @SNR_0 = 250'; 
    newdisp_FC(CC.varSNR.RHO_SNR0_250, FigureProps)
subplot(3,4,8)
    FigureProps.cLabel = 'CC'; 
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_125(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_125(:));
    FigureProps.TitleStr = 'CC @SNR_0 = 125';     
    newdisp_FC(CC.varSNR.RHO_SNR0_125, FigureProps)
subplot(3,4,9)
    FigureProps.cLabel = 'p value'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;    
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_1000(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_1000(:));
    FigureProps.TitleStr = 'p value @SNR_0 = 1000';      
    newdisp_FC(CC.varSNR.PVAL_SNR0_1000, FigureProps)    
subplot(3,4,10)
    FigureProps.cLabel = 'p value'; 
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_500(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_500(:));
    FigureProps.TitleStr = 'p value @SNR_0 = 500'; 
    newdisp_FC(CC.varSNR.PVAL_SNR0_500, FigureProps)
subplot(3,4,11)
    FigureProps.cLabel = 'p value'; 
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_250(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_250(:));
    FigureProps.TitleStr = 'p value @SNR_0 = 250'; 
    newdisp_FC(CC.varSNR.PVAL_SNR0_250, FigureProps)    
subplot(3,4,12)
    FigureProps.cLabel = 'p value'; 
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_125(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_125(:));
    FigureProps.TitleStr = 'p value @SNR_0 = 125';     
    newdisp_FC(CC.varSNR.PVAL_SNR0_125, FigureProps)
    
% save plot to file
FNout = fullfile(S_BOLD.reference.SimulationPars.resultPath,...
	sprintf('VAR_Tm-%3.2f-%3.2f_Tf-%3.2f-%3.2f_CC_%s_varSNR.jpg',...
        m1TauMin,m1TauMax,f1TauMin,f1TauMax,S_BOLD.reference.SimulationPars.label));
saveas(gcf,FNout);    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%   Plot CC array created from BOLD TCs with same SNRs          %%%%%%
%%%%%%  (SNR array size = BOLD_Model.SimulationPars.SNRmatrixSize)   %%%%%%
%%%%%%  but different random number realizations                     %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(11); 
    % Global settings %%%%% 
    FigureProps.GlobalTitleStr = ...
        sprintf('Simulated FC(T_m, T_f) @SNR=%3.0f & %s; [%3.0fx%3.0f] random noise realizations',...
        S_BOLD.VARdelays(1,1).SimulationPars.SNR0,...
        S_BOLD.reference.SimulationPars.label,SmS,SmS);
    FigureProps.cFsize = 1.1; % Fontsize 12 pt 
    FigureProps.NewColorMap = gray;
    FigureProps.TitleStr = ''; % title for subplots
    FigureProps.xAxisTick = m1TauMin+(SmS+gap)/2:...
           	(SmS+gap)*round(m1TauNsteps/ninterval):m1TauNsteps*(SmS+gap);
    FigureProps.yAxisTick = f1TauMin+(SmS+gap)/2:...
           	(SmS+gap)*round(m1TauNsteps/ninterval):f1TauNsteps*(SmS+gap); 
subplot(2,3,1)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = m1TauMin;
    FigureProps.maxVal = m1TauMax;
    FigureProps.TitleStr = 'CMRO2 temporal delay (T_m)';
    % label xAxisTicks with M1 values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.2f',m1TauMin + (count-1)*((m1TauMax-m1TauMin)/ninterval));
	end
    FigureProps.xAxisLabel = 'T_m';
    % no Ticks and label for y axis
	for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = '';
	end
    FigureProps.yAxisLabel = '';
    newdisp_FC(CC.SNRmatrix.TmValues, FigureProps)
    
subplot(2,3,2)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = f1TauMin;
    FigureProps.maxVal = f1TauMax;
    % no Ticks and label for x axis
	for count = 1:size(FigureProps.xAxisTick,2)    
        FigureProps.xAxisTickLabel{count} = '';
	end
    FigureProps.xAxisLabel = '';
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.2f',f1TauMax - (count-1)*((f1TauMax-m1TauMin)/ninterval));
    end
    FigureProps.yAxisLabel = 'T_f';     
    FigureProps.TitleStr = 'CBF temporal delay (T_f)';     
    newdisp_FC(CC.SNRmatrix.TfValues, FigureProps)       
    
subplot(2,3,3)
    FigureProps.cLabel = 'n = (f1-1)/(m1-1)'; 
    FigureProps.minVal = min(CC.varSNR.nActual(:));
    FigureProps.maxVal = max(CC.varSNR.nActual(:));
    FigureProps.TitleStr = 'prescribed coupling ratio (n)'; 
    % label xAxisTicks with M1 values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.2f',m1TauMin + (count-1)*((m1TauMax-m1TauMin)/ninterval));
	end
    FigureProps.xAxisLabel = 'T_m';
    newdisp_FC(CC.SNRmatrix.nPrescribed, FigureProps)

subplot(2,3,4)
    FigureProps.cLabel = 'n = (f1-1)/(m1-1)'; 
    FigureProps.minVal = min(CC.varSNR.nActual(:));
    FigureProps.maxVal = max(CC.varSNR.nActual(:));
    FigureProps.TitleStr = 'actual coupling ratio (n)'; 
    % label xAxisTicks with M1 values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.2f',m1TauMin + (count-1)*((m1TauMax-m1TauMin)/ninterval));
	end
    FigureProps.xAxisLabel = 'T_m';
    newdisp_FC(CC.SNRmatrix.nActual, FigureProps)    
    
subplot(2,3,5)
    FigureProps.cLabel = 'CC (T_m, T_f)';
	FigureProps.minVal = -0.5;
	FigureProps.maxVal = 0.5; 
%     FigureProps.minVal = min(CC.SNRmatrix.RHO_SNR(:));
%     FigureProps.maxVal = max(CC.SNRmatrix.RHO_SNR(:));
    FigureProps.TitleStr = sprintf('CC @SNR = %3.0f',...
                     S_BOLD.VARdelays(1,1).SimulationPars.SNR0);    
    newdisp_FC(CC.SNRmatrix.RHO_SNR, FigureProps)
    
subplot(2,3,6)
    FigureProps.cLabel = 'p value (T_m, T_f)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(CC.SNRmatrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(CC.SNRmatrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('p values @SNR = %3.0f',...
                     S_BOLD.VARdelays(1,1).SimulationPars.SNR0);    
    newdisp_FC(CC.SNRmatrix.PVAL_SNR, FigureProps)
        
    
    % save plot to file
    FNout = fullfile(S_BOLD.reference.SimulationPars.resultPath,...
    	sprintf('VAR_Tm-%3.2f-%3.2f_Tf-%3.2f-%3.2f_CC_%s_matrixSNR-%3.0f.jpg',...
        m1TauMin,m1TauMax,f1TauMin,f1TauMax,S_BOLD.reference.SimulationPars.label,...
        S_BOLD.VARdelays(1,1).SimulationPars.SNR0));
    saveas(gcf,FNout);    
end

