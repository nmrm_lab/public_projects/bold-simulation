%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 12.07.2019, Christine Preibisch
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function SimulateImpairedFC_VARm1_VARf1(BOLD_Model_VARm1_VARf1)
% CP 26MAR19
% simulate functional connectivity between 'intrinsic' BOLD responses 
% calculated from simulated neuronal inputs using Balloon model 
% (Buxton et al. MRM, 1998;Buxton et al. NI, 2004; Simon & Buxton, NI 2015)
%
% CP 02JUL19
% modified to allow for a seperate neuronal input signal that serves as a
% reference (should be simulated to electrophysiologically connected to the
% test region with electrophysiological impairment). 
% - NeuronalInput '1' will be used as reference.
% - Reference CBF (f1) und CMRO2 (m1) response  
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set global parameters for simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% minimum of prescribed CMRO2 values
BOLD_Model_VARm1_VARf1.SimulationPars.m1Min = 1;
% number of prescribed CMRO2 values % Note: output plots expect 16 steps
BOLD_Model_VARm1_VARf1.SimulationPars.m1Nsteps = 16; 
% step size for CMRO2 changes
BOLD_Model_VARm1_VARf1.SimulationPars.m1StepSize = 0.02;
% minimum of prescribed CBF values
BOLD_Model_VARm1_VARf1.SimulationPars.f1Min = 1;
% number of prescribed CBF values % Note: output plots expect 16 steps
BOLD_Model_VARm1_VARf1.SimulationPars.f1NSteps = 16;
% step size for CBF changes
BOLD_Model_VARm1_VARf1.SimulationPars.f1StepSize = 0.04;  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Call simulation of BOLD response for specified range of CMRO2 (-> m1)
% and CBF (-> f1) amplitudes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
S_BOLD_VARm1_VARf1 = SimulateBOLDsignal_VARm1_VARf1(BOLD_Model_VARm1_VARf1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Correlate noisy time courses & plot CC arrays @differenr SNR [1000...125]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CC_VARm1_VARf1.varSNR = ...
                CorrelateS_BOLD_VARm1_VARf1_varSNR(S_BOLD_VARm1_VARf1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Correlate noisy time courses at given SNR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CC_VARm1_VARf1.SNR1000matrix = ...
        CorrelateS_BOLD_VARm1_VARf1_SNRmatrix(S_BOLD_VARm1_VARf1,1000);
CC_VARm1_VARf1.SNR500matrix = ...
        CorrelateS_BOLD_VARm1_VARf1_SNRmatrix(S_BOLD_VARm1_VARf1,500);  
CC_VARm1_VARf1.SNR250matrix = ...
        CorrelateS_BOLD_VARm1_VARf1_SNRmatrix(S_BOLD_VARm1_VARf1,250);    
CC_VARm1_VARf1.SNR125matrix = ...
        CorrelateS_BOLD_VARm1_VARf1_SNRmatrix(S_BOLD_VARm1_VARf1,125);
% Define default to be compatible with previous version plot programs 
CC_VARm1_VARf1.SNRmatrix = CC_VARm1_VARf1.SNR250matrix;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot CC arrays 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
generateCCplots_VARm1_VARf1(S_BOLD_VARm1_VARf1, CC_VARm1_VARf1) 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% save results to deicated folder
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
save(fullfile(BOLD_Model_VARm1_VARf1.SimulationPars.resultPath,...
                                            'SimulationResults.mat'));
end