function BalloonModel = setDefaultValues(BalloonModel)
% CP 25.022019
% Default values for ballon model assuming tight coupling of CBF, CMRO2, 
% and CBVv dynamics (CMRO2.tau=2s, CBF.tau=2s, CBV.tau=0 s). 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define Balloon Model Parameters governing BOLD response via time constants 
% for hemodynamic (CBF/CBF0) -> fin(t)) and metabolic (CMRO2/CMRO20 -> m(t)) 
% response functions (see Table 1 in (Simon & Buxton, 2015) and 
% (Buxton et al. 2004)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--------------------------------------------------------------------------
% CBF response - define input parameters
%--------------------------------------------------------------------------
BalloonModel.CBF.Name = 'Flow Input - f(t)'; 
BalloonModel.CBF.amplitude = 1.5; % flow increase = (f1-1)*100 % [1.5 = 50%]
BalloonModel.CBF.tau = 2; % characteristic time constant for CBF [variable]
BalloonModel.CBF.z = 3; % shape parameter for CBF convolution kernel 3[2-4]
%--------------------------------------------------------------------------
% (Coupled) CMRO2 response - define input parameters
%--------------------------------------------------------------------------
BalloonModel.CMRO2.Name = 'CMRO2 Input - m(t)';
BalloonModel.CMRO2.amplitude = 1.25; % CMRO2 increase = (m1-1)*100 % [1.25 = 25%]
BalloonModel.CMRO2.tau = 2; % characteristic time constant for CMRO2 [variable]
BalloonModel.CMRO2.z = 3; % shape parameter for CMRO2 convolution kernel 3[2-4]
%--------------------------------------------------------------------------
% CBV response - define input parameters
%--------------------------------------------------------------------------
BalloonModel.CBV.tau0 = 0.75; % transit time for blood through venous compartment
                              % 2sec [0.75 sec - 2.5 sec]
BalloonModel.CBV.tau = 0.0;   % characteristic time constant for CBF [variable]
BalloonModel.CBV.alpha = 0.2; % exponent describing steady state venous flow-
                                % volume copupling; canonical value 0.38
                                % (Grubb et al., 1974) [variable]
                                % Recent values [0.18 ... 0.2]
%--------------------------------------------------------------------------
% CBV response - define input parameters
%--------------------------------------------------------------------------
BalloonModel.BOLD.OEF0 = 0.4; % resting OEF; default OEF0 = 0.4; 
                                % (Leenders et al., 1990) 
BalloonModel.BOLD.CBV0 = 2.5; % resting venous CBV [%] ; default CBV0 = 2.0;
BalloonModel.BOLD.TE = 0.03;  % echo time; TE = 0.03; % sec  
end

