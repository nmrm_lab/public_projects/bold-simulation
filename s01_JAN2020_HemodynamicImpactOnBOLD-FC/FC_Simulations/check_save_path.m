%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 17.02.2020, Mario Archila
%%% Function to check/create the correct folder structure to store the
%%% results of the simulations.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function check_save_path (save_path)

dir_names = {'VARcbv', 'VARdelays', 'VARm1_VARf1'};
subdir_names = {'SlowerCBV', 'TightCoupling'};

for dirIdx = 1:numel(dir_names)
    for subdirIdx = 1:numel(subdir_names)
        currDir = fullfile([save_path, filesep, ...
            dir_names{dirIdx}, filesep, subdir_names{subdirIdx}]);
        if ~exist(currDir, 'dir')
               mkdir(currDir)
        end
    end
end
end