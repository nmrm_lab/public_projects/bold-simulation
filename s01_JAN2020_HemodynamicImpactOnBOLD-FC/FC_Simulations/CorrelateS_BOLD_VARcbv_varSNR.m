function CC = CorrelateS_BOLD_VARcbv_varSNR(S_BOLD)
% CP 28MAR2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Correlate noisy time courses
% SNR of time courses with mean signal '1'; 
% standard deviation corresponds to sigma = 1/SNR0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% convenience definitions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % temporal resolution of simulated BOLD signal curves [sec]
% SampleTime = S_BOLD.VARcbv(1,1).SimulationPars.SampleTime; 
% % simulated time period
% totalDur = S_BOLD.VARcbv(1,1).SimulationPars.totalDur; 
% % total duration of initial block stimulus
% blockDur = S_BOLD.VARcbv(1,1).SimulationPars.blockDur;
% number of prescribed CMRO2 values
cbvTauNsteps = S_BOLD.VARcbv(1,1).SimulationPars.cbvTauNsteps; 
% % step size for CMRO2 changes
% m1StepSize = S_BOLD.VARcbv(1,1).SimulationPars.m1StepSize;
% number of prescribed CBF values
cbvAlphaNsteps= S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaNsteps; 
% % step size for CBF changes
% f1StepSize = S_BOLD.VARcbv(1,1).SimulationPars.f1StepSize; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialize array for m1, f1, n values
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CC.m1Values(1:cbvTauNsteps,1:cbvAlphaNsteps) = 0; 
CC.TmValues(1:cbvTauNsteps,1:cbvAlphaNsteps) = 0; 
CC.f1Values(1:cbvTauNsteps,1:cbvAlphaNsteps) = 0; 
CC.TfValues(1:cbvTauNsteps,1:cbvAlphaNsteps) = 0; 
CC.nPrescribed(1:cbvTauNsteps,1:cbvAlphaNsteps) = 0; 
CC.nActual(1:cbvTauNsteps,1:cbvAlphaNsteps) = 0; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%- Initialize arrays for CCs for S_BOLD(VARm1, VARf1) with different noises
%- Prepare Reference signal obtained from separate electrophysiological
%  signal, that is simulated to stem from a connected region
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ideal - no noise
CC.RHO_ideal(1:cbvTauNsteps,1:cbvAlphaNsteps) = 0; 
CC.PVAL_ideal(1:cbvTauNsteps,1:cbvAlphaNsteps) = 0; 
CC.REF_signal = S_BOLD.reference.BOLD.rs_bout.Data;
% irrealistic high SNR
CC.RHO_SNR0_1000(1:cbvTauNsteps,1:cbvAlphaNsteps) = 0; 
CC.PVAL_SNR0_1000(1:cbvTauNsteps,1:cbvAlphaNsteps) = 0; 
CC.REF_SNR0_1000_signal = ...
    AddNoiseTC(CC.REF_signal, 1/1000);
% irrealistic high SNR
CC.RHO_SNR0_750(1:cbvTauNsteps,1:cbvAlphaNsteps) = 0; 
CC.PVAL_SNR0_750(1:cbvTauNsteps,1:cbvAlphaNsteps) = 0; 
CC.REF_SNR0_750_signal = ...
    AddNoiseTC(CC.REF_signal, 1/750);
% high SNR
CC.RHO_SNR0_500(1:cbvTauNsteps,1:cbvAlphaNsteps) = 0; 
CC.PVAL_SNR0_500(1:cbvTauNsteps,1:cbvAlphaNsteps) = 0;
CC.REF_SNR0_500_signal = ...
    AddNoiseTC(CC.REF_signal, 1/500);
% common SNR
CC.RHO_SNR0_250(1:cbvTauNsteps,1:cbvAlphaNsteps) = 0; 
CC.PVAL_SNR0_250(1:cbvTauNsteps,1:cbvAlphaNsteps) = 0; 
CC.REF_SNR0_250_signal = ...
    AddNoiseTC(CC.REF_signal, 1/250);
% low SNR
CC.RHO_SNR0_125(1:cbvTauNsteps,1:cbvAlphaNsteps) = 0; 
CC.PVAL_SNR0_125(1:cbvTauNsteps,1:cbvAlphaNsteps) = 0; 
CC.REF_SNR0_125_signal = ...
    AddNoiseTC(CC.REF_signal, 1/125);
for m1Val = 1:(cbvTauNsteps)
	for f1Val = 1:(cbvAlphaNsteps)
        % Extract m1, f1 & n values form structure to matrices with identical
        % arrangement as correlation matrices (to avoid confusion & mistakes)
        CC.m1Values(m1Val,f1Val) = S_BOLD.VARcbv(m1Val,f1Val).CMRO2.amplitude; 
        CC.TmValues(m1Val,f1Val) = S_BOLD.VARcbv(m1Val,f1Val).CMRO2.tau;                    
        CC.f1Values(m1Val,f1Val) = S_BOLD.VARcbv(m1Val,f1Val).CBF.amplitude; 
        CC.TfValues(m1Val,f1Val) = S_BOLD.VARcbv(m1Val,f1Val).CBF.tau;       
        CC.nPrescribed(m1Val,f1Val) = S_BOLD.VARcbv(m1Val,f1Val).n.Prescribed; 
        CC.nActual(m1Val,f1Val) = S_BOLD.VARcbv(m1Val,f1Val).n.Actual;  
        CC.TvValues(m1Val,f1Val) = S_BOLD.VARcbv(m1Val,f1Val).CBV.tau; 
        CC.alphaValues(m1Val,f1Val) = S_BOLD.VARcbv(m1Val,f1Val).CBV.alpha;         
        % Correlation for ideal signal (without noise)
        signal = S_BOLD.VARcbv(m1Val,f1Val).BOLD.rs_bout.Data;
        [CC.RHO_ideal(m1Val,f1Val),CC.PVAL_ideal(m1Val,f1Val)] = ...
                            corr(CC.REF_signal,signal);
        % Correlation for noisy signal (SNR0 = 1000)
        signal_SNR0_1000 = AddNoiseTC(signal, 1/1000);
        [CC.RHO_SNR0_1000(m1Val,f1Val),CC.PVAL_SNR0_1000(m1Val,f1Val)] = ...
                            corr(CC.REF_SNR0_1000_signal, signal_SNR0_1000);     
        % Correlation for noisy signal (SNR0 = 500)
        signal_SNR0_750 = AddNoiseTC(signal, 1/750);
        [CC.RHO_SNR0_750(m1Val,f1Val),CC.PVAL_SNR0_750(m1Val,f1Val)] = ...
                            corr(CC.REF_SNR0_750_signal, signal_SNR0_750);             
        % Correlation for noisy signal(SNR0 = 250)
        signal_SNR0_500 = AddNoiseTC(signal, 1/500);
        [CC.RHO_SNR0_500(m1Val,f1Val),CC.PVAL_SNR0_500(m1Val,f1Val)] = ...
                            corr(CC.REF_SNR0_500_signal, signal_SNR0_500);                  
        % Correlation for noisy signal (SNR0 = 250)
        signal_SNR0_250 = AddNoiseTC(signal, 1/250);
        [CC.RHO_SNR0_250(m1Val,f1Val),CC.PVAL_SNR0_250(m1Val,f1Val)] = ...
                            corr(CC.REF_SNR0_250_signal, signal_SNR0_250); 
        % Correlation for noisy signal (SNR0 = 125)
        signal_SNR0_125 = AddNoiseTC(signal, 1/125);
        [CC.RHO_SNR0_125(m1Val,f1Val),CC.PVAL_SNR0_125(m1Val,f1Val)] = ...
                            corr(CC.REF_SNR0_125_signal, signal_SNR0_125);             
	end % for f1Val
end % for m1Val
end

