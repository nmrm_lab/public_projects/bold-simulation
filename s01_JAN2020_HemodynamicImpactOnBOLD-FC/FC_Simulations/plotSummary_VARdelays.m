function plotSummary_VARdelays(BOLD_Model, fignr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot summary of simulation results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define local variables (for convenience)
m1TauNsteps = BOLD_Model(1,1).SimulationPars.m1TauNsteps;
f1TauNsteps = BOLD_Model(1,1).SimulationPars.f1TauNsteps;
m1TauStepSize = BOLD_Model(1,1).SimulationPars.m1TauStepSize;
f1TauStepSize = BOLD_Model(1,1).SimulationPars.f1TauStepSize;
SNR0 = BOLD_Model(1,1).SimulationPars.SNR0;
totaldur = BOLD_Model(1,1).SimulationPars.totalDur;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot all curves without noise
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(fignr);
colormap('viridis')
set(0,'DefaultAxesColorOrder',viridis(m1TauNsteps))

plotNr = 0;
for f1Val = 1:f1TauNsteps
%     if mod(f1Val,2) == 0
        plotNr = plotNr + 1;
        subplot(4,4,plotNr)    
        for m1Val=1:m1TauNsteps
            plot(BOLD_Model(m1Val,f1Val).BOLD.bout,'LineWidth',2)
            hold on
        end %for m1Val
        if BOLD_Model(1,f1Val).CBF.tau ...
                == BOLD_Model(m1TauNsteps,f1Val).CBF.tau
            title_str = sprintf('T_m = [%3.1f-%3.1f] @T_f = %3.1f',...
                BOLD_Model(1,f1Val).CMRO2.tau, ....
                BOLD_Model(m1TauNsteps,f1Val).CMRO2.tau,...
                BOLD_Model(m1TauNsteps,f1Val).CBF.tau);        
        else
            title_str = sprintf('T_m = [%3.1f-%3.1f] @T_f = [%3.1f-%3.1f]',...
                BOLD_Model(1,f1Val).CMRO2.tau, ....
                BOLD_Model(m1TauNsteps,f1Val).CMRO2.tau,...
                BOLD_Model(1,f1Val).CBF.tau, ...
                BOLD_Model(m1TauNsteps,f1Val).CBF.tau);
        end
        title(title_str);     
        n_text_str = sprintf('n = [%3.1f, %3.1f, %3.1f,...%3.1f]', ...
                BOLD_Model(1, f1Val).n.Prescribed, ...
                BOLD_Model(2, f1Val).n.Prescribed, ...
                BOLD_Model(3, f1Val).n.Prescribed, ...
                BOLD_Model(m1TauNsteps,f1Val).n.Prescribed);
        text(50, 0.97,n_text_str)
        axis([0 totaldur 0.965 1.035])
        ylabel('\Delta S_{BOLD} / S_0')
        xlabel('time [sec]') 
        hold off
%     end %if mod(f1Val,2) == 0
end %for f1Val

% save plot to file
FNout = fullfile(BOLD_Model(1,1).SimulationPars.resultPath,...
        sprintf('VARdelays_%s_ResponseSummary.jpg',...
                    BOLD_Model(1,1).SimulationPars.label));
saveas(gcf,FNout);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot curves with maximum tau_f with noise
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for f1Val = 1:f1TauNsteps
    plotNr = 0;
    figure(fignr+1);
    for m1Val=1:m1TauNsteps
%         if mod(m1Val,2) == 0
            plotNr = plotNr + 1;
            subplot(4,4,plotNr)    
            plot(BOLD_Model(m1Val,f1Val).BOLD.boutNoise,'b','LineWidth',1)
            hold on
            plot(BOLD_Model(m1Val,f1Val).BOLD.bout,'r','LineWidth',2)
            title_str = sprintf('T_m = %3.1f, T_f = %3.1f',...
                    BOLD_Model(m1Val,f1Val).CMRO2.tau, ....
                    BOLD_Model(m1Val,f1Val).CBF.tau);                   
            title(title_str);     
            n_text_str = sprintf('n = %3.1f, SNR_0 = %3.0f',...
                                BOLD_Model(m1Val,f1Val).n.Prescribed,...
                                BOLD_Model(1,1).SimulationPars.SNR0); 
            text(50, 0.97,n_text_str)
            axis([0 totaldur 0.965 1.035])
            ylabel('\Delta S_{BOLD} / S_0')
            xlabel('time [sec]') 
            hold off
%         end %if mod(m1Val,2) == 0
    end %for m1Val

    % save plot to file
    FNout = fullfile(BOLD_Model(1,1).SimulationPars.resultPath,...
            sprintf('VARdelays-Tf%3.1f_%s_ResponseSummaryNoise.jpg',...
                        BOLD_Model(m1Val,f1Val).CBF.tau,...
                        BOLD_Model(1,1).SimulationPars.label));
    saveas(gcf,FNout);
end

end

