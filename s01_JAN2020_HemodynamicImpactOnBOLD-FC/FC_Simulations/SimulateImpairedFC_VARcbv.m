%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 12.07.2019, Christine Preibisch
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function SimulateImpairedFC_VARcbv(BOLD_Model_VARcbv)
% CP 26MAR19
% simulate functional connectivity between 'intrinsic' BOLD responses 
% calculated from simulated neuronal inputs using Balloon model 
% (Buxton et al. MRM, 1998;Buxton et al. NI, 2004; Simon & Buxton, NI 2015)
%
% CP 02JUL19
% modified to allow for a seperate neuronal input signal that serves as a
% reference (should be simulated to electrophysiologically connected to the
% test region with electrophysiological impairment). 
% - NeuronalInput '1' will be used as reference.
% - Reference CBF (f1) und CMRO2 (m1) response  
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set global parameters for simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% minimum of prescribed CBV.tau values
BOLD_Model_VARcbv.SimulationPars.cbvTauMin = 0.0; 
% number of prescribed CBV.tau values % Note: output plots expect 16 steps
BOLD_Model_VARcbv.SimulationPars.cbvTauNsteps = 16; 
% step size for CBV.tau changes
BOLD_Model_VARcbv.SimulationPars.cbvTauStepSize = 2.0;
% minimum of prescribed CBF.alpha values
BOLD_Model_VARcbv.SimulationPars.cbvAlphaMin = 0.025; % TTP = 2*tau
% number of prescribed CBF.tau values % Note: output plots expect 16 steps
BOLD_Model_VARcbv.SimulationPars.cbvAlphaNsteps = 16; 
% step size for CBF changes
BOLD_Model_VARcbv.SimulationPars.cbvAlphaStepSize = 0.025;  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Call simulation of BOLD response for specified range of CMRO2 (-> m1)
% and CBF (-> f1) amplitudes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
S_BOLD_VARcbv = SimulateBOLDsignal_VARcbv(BOLD_Model_VARcbv);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Correlate noisy time courses & plot CC arrays @differenr SNR [1000...125]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CC_VARcbv.varSNR = CorrelateS_BOLD_VARcbv_varSNR(S_BOLD_VARcbv);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Correlate noisy time courses at given SNR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CC_VARcbv.SNR1000matrix = ...
                       CorrelateS_BOLD_VARcbv_SNRmatrix(S_BOLD_VARcbv,1000);
CC_VARcbv.SNR500matrix = ...
                       CorrelateS_BOLD_VARcbv_SNRmatrix(S_BOLD_VARcbv,500);
CC_VARcbv.SNR250matrix = ...
                       CorrelateS_BOLD_VARcbv_SNRmatrix(S_BOLD_VARcbv,250);
CC_VARcbv.SNR125matrix = ...
                       CorrelateS_BOLD_VARcbv_SNRmatrix(S_BOLD_VARcbv,125);
% Set default to be compatible with previous versions plot programs 
CC_VARcbv.SNRmatrix = CC_VARcbv.SNR250matrix;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot CC arrays 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
generateCCplots_VARcbv(S_BOLD_VARcbv, CC_VARcbv) 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% save results to deicated folder
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
save(fullfile(BOLD_Model_VARcbv.SimulationPars.resultPath,...
                                            'SimulationResults.mat'));
end