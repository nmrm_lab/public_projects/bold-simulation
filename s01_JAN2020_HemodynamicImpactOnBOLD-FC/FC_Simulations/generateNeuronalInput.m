function NeuronalInput = generateNeuronalInput(SimulationPars)
% CP26MAR19
% Define neuronal input 
% --> couple simulated gamma-Power activity to block input
%     (for checking behaviour of simulated BOLD signal reponse)

    % --> generate structure on the basis of a block input
    NeuronalInput.block = blockInput(SimulationPars);
    NeuronalInput.label = 'boxcar+avgPow';
             
for nInput = 1:size(SimulationPars.NeuronalInputFiles,1)
    % --> read simulated neuronal signal (average gamma power)
    NeuronalInput.avgPow(nInput) = ...
                        load(SimulationPars.NeuronalInputFiles(nInput,:));
                    
    % determint length of composite signal length
    blockPoints = round(SimulationPars.blockDur/SimulationPars.deltaT);
    npoints = blockPoints + size(NeuronalInput.avgPow(nInput).avgPowNorm,2);
    
    % define time axis
    NeuronalInput.time(nInput,:) = NeuronalInput.block.deltaT:...
    	NeuronalInput.block.deltaT:npoints*NeuronalInput.block.deltaT;
    
    % Couple simulated neuronal input to block input (for checking BOLD model)
    % by generating composite time vector
    compositeTC = ...
        [NeuronalInput.block.block(1:blockPoints) NeuronalInput.avgPow(nInput).avgPowNorm];

    %generate time series for simulation
    NeuronalInput.ts(nInput,:) = timeseries(compositeTC,NeuronalInput.time(nInput,:),...
                                                'Name',NeuronalInput.label);
end % for nInput
end

