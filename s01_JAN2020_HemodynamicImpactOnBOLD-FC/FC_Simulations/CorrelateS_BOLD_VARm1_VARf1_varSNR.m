function CC = CorrelateS_BOLD_VARm1_VARf1_varSNR(S_BOLD)
% CP 28MAR2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Correlate noisy time courses
% SNR of time courses with mean signal '1'; 
% standard deviation corresponds to sigma = 1/SNR0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% convenience definitions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % temporal resolution of simulated BOLD signal curves [sec]
% SampleTime = S_BOLD.VARm1_VARf1(1,1).SimulationPars.SampleTime; 
% % simulated time period
% totalDur = S_BOLD.VARm1_VARf1(1,1).SimulationPars.totalDur; 
% % total duration of initial block stimulus
% blockDur = S_BOLD.VARm1_VARf1(1,1).SimulationPars.blockDur;
% number of prescribed CMRO2 values
m1Nsteps = S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1Nsteps; 
% % step size for CMRO2 changes
% m1StepSize = S_BOLD.VARm1_VARf1(1,1).SimulationPars.m1StepSize;
% number of prescribed CBF values
f1Nsteps= S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1NSteps; 
% % step size for CBF changes
% f1StepSize = S_BOLD.VARm1_VARf1(1,1).SimulationPars.f1StepSize; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialize array for m1, f1, n values
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CC.m1Values(1:m1Nsteps,1:f1Nsteps) = 0; 
CC.f1Values(1:m1Nsteps,1:f1Nsteps) = 0; 
CC.nPrescribed(1:m1Nsteps,1:f1Nsteps) = 0; 
CC.nActual(1:m1Nsteps,1:f1Nsteps) = 0; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%- Initialize arrays for CCs for S_BOLD(VARm1, VARf1) with different noises
%- Prepare Reference signal obtained from separate electrophysiological
%  signal, that is simulated to stem from a connected region
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ideal - no noise
CC.RHO_ideal(1:m1Nsteps,1:f1Nsteps) = 0; 
CC.PVAL_ideal(1:m1Nsteps,1:f1Nsteps) = 0; 
CC.REF_signal = S_BOLD.reference.BOLD.rs_bout.Data;
% irrealistic high SNR
CC.RHO_SNR0_1000(1:m1Nsteps,1:f1Nsteps) = 0;
CC.PVAL_SNR0_1000(1:m1Nsteps,1:f1Nsteps) = 0; 
CC.REF_SNR0_1000_signal = AddNoiseTC(CC.REF_signal, 1/1000);
% irrealistic high SNR
CC.RHO_SNR0_750(1:m1Nsteps,1:f1Nsteps) = 0; 
CC.PVAL_SNR0_750(1:m1Nsteps,1:f1Nsteps) = 0; 
CC.REF_SNR0_750_signal = AddNoiseTC(CC.REF_signal, 1/750);
% high SNR
CC.RHO_SNR0_500(1:m1Nsteps,1:f1Nsteps) = 0; 
CC.PVAL_SNR0_500(1:m1Nsteps,1:f1Nsteps) = 0;
CC.REF_SNR0_500_signal = AddNoiseTC(CC.REF_signal, 1/500);
% common SNR
CC.RHO_SNR0_250(1:m1Nsteps,1:f1Nsteps) = 0; 
CC.PVAL_SNR0_250(1:m1Nsteps,1:f1Nsteps) = 0;
CC.REF_SNR0_250_signal = AddNoiseTC(CC.REF_signal, 1/250);
% low SNR
CC.RHO_SNR0_125(1:m1Nsteps,1:f1Nsteps) = 0; 
CC.PVAL_SNR0_125(1:m1Nsteps,1:f1Nsteps) = 0; 
CC.REF_SNR0_125_signal = AddNoiseTC(CC.REF_signal, 1/125);
for m1Val = 1:(m1Nsteps)
	for f1Val = 1:(f1Nsteps)
        % Extract m1, f1 & n values form structure to matrices with identical
        % arrangement as correlation matrices (to avoid confusion & mistakes)
        CC.m1Values(m1Val,f1Val) = ...
                        S_BOLD.VARm1_VARf1(m1Val,f1Val).CMRO2.amplitude; 
        CC.f1Values(m1Val,f1Val) = ...
                        S_BOLD.VARm1_VARf1(m1Val,f1Val).CBF.amplitude; 
        CC.nPrescribed(m1Val,f1Val) = ...
                        S_BOLD.VARm1_VARf1(m1Val,f1Val).n.Prescribed; 
        CC.nActual(m1Val,f1Val) = ...
                        S_BOLD.VARm1_VARf1(m1Val,f1Val).n.Actual;                    
        % Correlation for ideal signal (without noise)
        signal = S_BOLD.VARm1_VARf1(m1Val,f1Val).BOLD.rs_bout.Data;
        [CC.RHO_ideal(m1Val,f1Val),CC.PVAL_ideal(m1Val,f1Val)] = ...
        	corr(CC.REF_signal,signal);
        % Correlation for noisy signal (SNR0 = 1000)
        signal_SNR0_1000 = AddNoiseTC(signal, 1/1000);
        [CC.RHO_SNR0_1000(m1Val,f1Val),CC.PVAL_SNR0_1000(m1Val,f1Val)] = ...
        	corr(CC.REF_SNR0_1000_signal, signal_SNR0_1000);     
        % Correlation for noisy signal (SNR0 = 750)
        signal_SNR0_750 = AddNoiseTC(signal, 1/750);
        [CC.RHO_SNR0_750(m1Val,f1Val),CC.PVAL_SNR0_750(m1Val,f1Val)] = ...
        	corr(CC.REF_SNR0_750_signal, signal_SNR0_750);             
        % Correlation for noisy signal(SNR0 = 500)
        signal_SNR0_500 = AddNoiseTC(signal, 1/500);
        [CC.RHO_SNR0_500(m1Val,f1Val),CC.PVAL_SNR0_500(m1Val,f1Val)] = ...
        	corr(CC.REF_SNR0_500_signal, signal_SNR0_500);                  
        % Correlation for noisy signal (SNR0 = 250)
        signal_SNR0_250 = AddNoiseTC(signal, 1/250);
        [CC.RHO_SNR0_250(m1Val,f1Val),CC.PVAL_SNR0_250(m1Val,f1Val)] = ...
        	corr(CC.REF_SNR0_250_signal, signal_SNR0_250); 
        % Correlation for noisy signal (SNR0 = 125)
        signal_SNR0_125 = AddNoiseTC(signal, 1/125);
        [CC.RHO_SNR0_125(m1Val,f1Val),CC.PVAL_SNR0_125(m1Val,f1Val)] = ...
        	corr(CC.REF_SNR0_125_signal, signal_SNR0_125);             
	end % for f1Val
end % for m1Val
end

