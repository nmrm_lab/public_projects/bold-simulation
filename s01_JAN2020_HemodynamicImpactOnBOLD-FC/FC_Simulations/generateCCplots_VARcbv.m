function generateCCplots_VARcbv(S_BOLD, CC)
% CP 29MAR19 Plot CC arrays
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% convenience definitions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% temporal resolution of simulated BOLD signal curves [sec]
SampleTime = S_BOLD.VARcbv(1,1).SimulationPars.SampleTime; 
% simulated time period
totalDur = S_BOLD.VARcbv(1,1).SimulationPars.totalDur; 
% total duration of initial block stimulus
blockDur = S_BOLD.VARcbv(1,1).SimulationPars.blockDur;
% number of prescribed CBV temporal delay values
cbvTauNsteps = S_BOLD.VARcbv(1,1).SimulationPars.cbvTauNsteps;
% minimum of prescribed CBV temporal delay values
cbvTauMin = S_BOLD.VARcbv(1,1).SimulationPars.cbvTauMin;
% step size for CBV temporal delay changes
cbvTauStepSize = S_BOLD.VARcbv(1,1).SimulationPars.cbvTauStepSize;
% max CBV temporal delay value
cbvTauMax = cbvTauMin + (cbvTauNsteps-1) * cbvTauStepSize;
% minimum of prescribed alpha values
cbvAlphaMin = S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaMin;
% number of prescribed alpha values
cbvAlphaNsteps = S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaNsteps; 
% step size for alpha changes
cbvAlphaStepSize = S_BOLD.VARcbv(1,1).SimulationPars.cbvAlphaStepSize; 
% max alpha value
cbvAlphaMax = cbvAlphaMin + (cbvAlphaNsteps-1) * cbvAlphaStepSize;

% allow for (ninterval+1) x & y AxisTicks
ninterval = 3;

% SNR matrix size --> sqrt(number of different random noise realizations)
% --> create SmS x SmS patches in CC maps with same SNR but different noise
%     realizations
SmS = S_BOLD.VARcbv(1,1).SimulationPars.SNRmatrixSize;
% Border width between SmS x SmS patches with same SNR
% gap = S_BOLD.VARcbv(1,1).SimulationPars.SNRgapSize;
gap = S_BOLD.VARcbv(1,1).SimulationPars.SNRgapSize;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%   Plot CC arrays created from BOLD TCs with different SNRs    %%%%%%
%%%%%%  (SNR array size = 1)                                         %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(10); 
    % Global settings
    FigureProps.GlobalTitleStr = ...
        sprintf('Simulated FC(T_v, alpha) @%s for different SNR',...
                            S_BOLD.reference.SimulationPars.label);
    FigureProps.cFsize = 1.1; % Fontsize 12 pt 
    FigureProps.NewColorMap = gray;
    FigureProps.TitleStr = ''; % title for subplots
    FigureProps.xAxisTick = 1:round(cbvTauNsteps/ninterval):cbvTauNsteps;
    FigureProps.yAxisTick = 1:round(cbvAlphaNsteps/ninterval):cbvAlphaNsteps;  
subplot(3,4,1)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = cbvTauMin;
    FigureProps.maxVal = cbvTauMax;
    FigureProps.TitleStr = 'CBV temporal delay (T_v)';

	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',cbvTauMin + (count-1)*((cbvTauMax-cbvTauMin)/ninterval))
	end
    FigureProps.xAxisLabel = 'T_v';
	for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = '';
	end
    FigureProps.yAxisLabel = '';
    newdisp_FC(CC.varSNR.TvValues, FigureProps)
subplot(3,4,2)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = cbvAlphaMin;
    FigureProps.maxVal = cbvAlphaMax;
	for count = 1:size(FigureProps.xAxisTick,2)    
        FigureProps.xAxisTickLabel{count} = '';
	end
    FigureProps.xAxisLabel = '';
    yAxTicks = cbvAlphaMax:-round(cbvAlphaNsteps/ninterval):cbvAlphaMin; % axis is inverted!
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
        	sprintf('%1.3f',cbvAlphaMax - (count-1)*((cbvAlphaMax-cbvAlphaMin)/ninterval));
    end
    FigureProps.yAxisLabel = '\alpha';     
    FigureProps.TitleStr = 'CBV response scaling (\alpha)';     
    newdisp_FC(CC.varSNR.alphaValues, FigureProps)  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % set axis for all other plots
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
            sprintf('%1.1f',cbvTauMin + (count-1)*((cbvTauMax-cbvTauMin)/ninterval));
    end
    FigureProps.xAxisLabel = 'T_v';
    yAxTicks = cbvAlphaMax:-round(cbvAlphaNsteps/ninterval):cbvAlphaMin; % axis is inverted!
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.3f',cbvAlphaMax - (count-1)*((cbvAlphaMax-cbvAlphaMin)/ninterval))
    end
    FigureProps.yAxisLabel = '\alpha';      
subplot(3,4,3)
    FigureProps.cLabel = 'n = (f1-1)/(m1-1)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 4;
%     FigureProps.minVal = min(CC.varSNR.nActual(:));
%     FigureProps.maxVal = max(CC.varSNR.nActual(:));
    FigureProps.TitleStr = 'prescribed coupling ratio';     
    newdisp_FC(CC.varSNR.nPrescribed, FigureProps)
subplot(3,4,4)
    FigureProps.cLabel = 'n = (f1-1)/(m1-1)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 4;
%     FigureProps.minVal = min(CC.varSNR.nActual(:));
%     FigureProps.maxVal = max(CC.varSNR.nActual(:));
    FigureProps.TitleStr = 'actual coupling ratio';     
    newdisp_FC(CC.varSNR.nActual, FigureProps)
subplot(3,4,5)
    FigureProps.cLabel = 'CC'; 
%     FigureProps.minVal = 0;
%     FigureProps.maxVal = 0.5;
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_1000(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_1000(:));
    FigureProps.TitleStr = 'CC @SNR_0 = 1000';      
    newdisp_FC(CC.varSNR.RHO_SNR0_1000, FigureProps)    
subplot(3,4,6)
    FigureProps.cLabel = 'CC'; 
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_500(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_500(:));
    FigureProps.TitleStr = 'CC @SNR_0 = 500'; 
    newdisp_FC(CC.varSNR.RHO_SNR0_500, FigureProps)
subplot(3,4,7)
    FigureProps.cLabel = 'CC'; 
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_250(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_250(:));
    FigureProps.TitleStr = 'CC @SNR_0 = 250'; 
    newdisp_FC(CC.varSNR.RHO_SNR0_250, FigureProps)
subplot(3,4,8)
    FigureProps.cLabel = 'CC'; 
    FigureProps.minVal = min(CC.varSNR.RHO_SNR0_125(:));
    FigureProps.maxVal = max(CC.varSNR.RHO_SNR0_125(:));
    FigureProps.TitleStr = 'CC @SNR_0 = 125';     
    newdisp_FC(CC.varSNR.RHO_SNR0_125, FigureProps)
subplot(3,4,9)
    FigureProps.cLabel = 'p value'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;    
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_1000(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_1000(:));
    FigureProps.TitleStr = 'p value @SNR_0 = 1000';      
    newdisp_FC(CC.varSNR.PVAL_SNR0_1000, FigureProps)    
subplot(3,4,10)
    FigureProps.cLabel = 'p value'; 
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_500(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_500(:));
    FigureProps.TitleStr = 'p value @SNR_0 = 500'; 
    newdisp_FC(CC.varSNR.PVAL_SNR0_500, FigureProps)
subplot(3,4,11)
    FigureProps.cLabel = 'p value'; 
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_250(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_250(:));
    FigureProps.TitleStr = 'p value @SNR_0 = 250'; 
    newdisp_FC(CC.varSNR.PVAL_SNR0_250, FigureProps)    
subplot(3,4,12)
    FigureProps.cLabel = 'p value'; 
%     FigureProps.minVal = min(CC.varSNR.PVAL_SNR0_125(:));
%     FigureProps.maxVal = max(CC.varSNR.PVAL_SNR0_125(:));
    FigureProps.TitleStr = 'p value @SNR_0 = 125';     
    newdisp_FC(CC.varSNR.PVAL_SNR0_125, FigureProps)
    
% save plot to file
FNout = fullfile(S_BOLD.reference.SimulationPars.resultPath,...
	sprintf('VAR_Tm-%3.2f-%3.2f_Tf-%3.2f-%3.2f_CC_%s_varSNR.jpg',...
            cbvTauMin,cbvTauMax,cbvAlphaMin,cbvAlphaMax,S_BOLD.reference.SimulationPars.label));
saveas(gcf,FNout);    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%   Plot CC array created from BOLD TCs with same SNRs          %%%%%%
%%%%%%  (SNR array size = BOLD_Model.SimulationPars.SNRmatrixSize)   %%%%%%
%%%%%%  but different random number realizations                     %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(11); 
    % Global settings %%%%% 
    FigureProps.GlobalTitleStr = ...
        sprintf('Simulated FC(T_v, alpha) @SNR=%3.0f & %s; [%3.0fx%3.0f] random noise realizations',...
        S_BOLD.VARcbv(1,1).SimulationPars.SNR0,...
        S_BOLD.reference.SimulationPars.label,SmS,SmS);
    FigureProps.cFsize = 1.1; % Fontsize 12 pt 
    FigureProps.NewColorMap = gray;
    FigureProps.TitleStr = ''; % title for subplots
    FigureProps.xAxisTick = cbvTauMin+(SmS+gap)/2:...
           	(SmS+gap)*round(cbvTauNsteps/ninterval):cbvTauNsteps*(SmS+gap);
    FigureProps.yAxisTick = cbvAlphaMin+(SmS+gap)/2:...
           	(SmS+gap)*round(cbvTauNsteps/ninterval):cbvAlphaNsteps*(SmS+gap); 
subplot(2,3,1)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = cbvTauMin;
    FigureProps.maxVal = cbvTauMax;
    FigureProps.TitleStr = 'CBV temporal delay (T_v)';
    % label xAxisTicks with M1 values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',cbvTauMin + (count-1)*((cbvTauMax-cbvTauMin)/ninterval));
	end
    FigureProps.xAxisLabel = 'T_v';
    % no Ticks and label for y axis
	for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = '';
	end
    FigureProps.yAxisLabel = '';
    newdisp_FC(CC.SNRmatrix.TvValues, FigureProps)
    
subplot(2,3,2)
    FigureProps.cLabel = ''; 
    FigureProps.minVal = cbvAlphaMin;
    FigureProps.maxVal = cbvAlphaMax;
    % no Ticks and label for x axis
	for count = 1:size(FigureProps.xAxisTick,2)    
        FigureProps.xAxisTickLabel{count} = '';
	end
    FigureProps.xAxisLabel = '';
    for count = 1:size(FigureProps.yAxisTick,2)    
        FigureProps.yAxisTickLabel{count} = ...
            sprintf('%1.3f',cbvAlphaMax - (count-1)*((cbvAlphaMax-cbvAlphaMin)/ninterval));
    end
    FigureProps.yAxisLabel = '\alpha';     
    FigureProps.TitleStr = 'CBV response scaling (\alpha)';     
    newdisp_FC(CC.SNRmatrix.alphaValues, FigureProps)       
    
subplot(2,3,3)
    FigureProps.cLabel = 'n = (f1-1)/(m1-1)'; 
    FigureProps.minVal = 0; %min(CC.varSNR.nActual(:));
    FigureProps.maxVal = 4; %max(CC.varSNR.nActual(:));
    FigureProps.TitleStr = 'prescribed coupling ratio (n)'; 
    % label xAxisTicks with M1 values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',cbvTauMin + (count-1)*((cbvTauMax-cbvTauMin)/ninterval));
	end
    FigureProps.xAxisLabel = 'T_v';
    newdisp_FC(CC.SNRmatrix.nPrescribed, FigureProps)

subplot(2,3,4)
    FigureProps.cLabel = 'n = (f1-1)/(m1-1)'; 
    FigureProps.minVal = 0; %min(CC.varSNR.nActual(:));
    FigureProps.maxVal = 4; %max(CC.varSNR.nActual(:));
    FigureProps.TitleStr = 'actual coupling ratio (n)'; 
    % label xAxisTicks with M1 values
	for count = 1:size(FigureProps.xAxisTick,2)
        FigureProps.xAxisTickLabel{count} = ...
        	sprintf('%1.1f',cbvTauMin + (count-1)*((cbvTauMax-cbvTauMin)/ninterval));
	end
    FigureProps.xAxisLabel = 'T_v';
    newdisp_FC(CC.SNRmatrix.nActual, FigureProps)    
    
subplot(2,3,5)
    FigureProps.cLabel = 'CC (T_v, \alpha)';
% 	FigureProps.minVal = 0;
% 	FigureProps.maxVal = 0.5; 
    FigureProps.minVal = min(CC.SNRmatrix.RHO_SNR(:));
    FigureProps.maxVal = max(CC.SNRmatrix.RHO_SNR(:));
    FigureProps.TitleStr = sprintf('CC @SNR = %3.0f',...
                     S_BOLD.VARcbv(1,1).SimulationPars.SNR0);    
    newdisp_FC(CC.SNRmatrix.RHO_SNR, FigureProps)
    
subplot(2,3,6)
    FigureProps.cLabel = 'p value (T_v, \alpha)'; 
    FigureProps.minVal = 0;
    FigureProps.maxVal = 0.05;
%     FigureProps.minVal = min(CC.SNRmatrix.PVAL_SNR(:));
%     FigureProps.maxVal = max(CC.SNRmatrix.PVAL_SNR(:));
    FigureProps.TitleStr = sprintf('p values @SNR = %3.0f',...
                     S_BOLD.VARcbv(1,1).SimulationPars.SNR0);    
    newdisp_FC(CC.SNRmatrix.PVAL_SNR, FigureProps)
        
    
    % save plot to file
    FNout = fullfile(S_BOLD.reference.SimulationPars.resultPath,...
    	sprintf('VAR_Tm-%3.2f-%3.2f_Tf-%3.2f-%3.2f_CC_%s_matrixSNR-%3.0f.jpg',...
        cbvTauMin,cbvTauMax,cbvAlphaMin,cbvAlphaMax,S_BOLD.reference.SimulationPars.label,...
        S_BOLD.VARcbv(1,1).SimulationPars.SNR0));
    saveas(gcf,FNout);    
end

