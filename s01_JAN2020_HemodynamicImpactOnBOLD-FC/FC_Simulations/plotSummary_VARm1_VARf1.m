function plotSummary_VARm1_VARf1(BOLD_Model, fignr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot summary of simulation results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define local variables (for convenience)
m1Nsteps = BOLD_Model(1,1).SimulationPars.m1Nsteps;
f1NSteps = BOLD_Model(1,1).SimulationPars.f1NSteps;
m1StepSize = BOLD_Model(1,1).SimulationPars.m1StepSize;
f1StepSize = BOLD_Model(1,1).SimulationPars.f1StepSize;
SNR0 = BOLD_Model(1,1).SimulationPars.SNR0;
totaldur = BOLD_Model(1,1).SimulationPars.totalDur;

colormap('viridis')
set(0,'DefaultAxesColorOrder',viridis(m1Nsteps))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot all curves without noise
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(fignr);
plotNr = 0;
for f1Val = 1:f1NSteps
%     if mod(f1Val,2) == 0
        plotNr = plotNr + 1;
        subplot(4,4,plotNr)  
        for m1Val=1:m1Nsteps
            plot(BOLD_Model(m1Val,f1Val).BOLD.bout,'LineWidth',2)
            hold on
        end %for m1Val
        if BOLD_Model(1,f1Val).CBF.amplitude ...
                == BOLD_Model(m1Nsteps,f1Val).CBF.amplitude
            title_str = sprintf('m1 = [%3.1f-%3.3f] @f1 = %3.2f',...
                BOLD_Model(1,f1Val).CMRO2.amplitude, ....
                BOLD_Model(m1Nsteps,f1Val).CMRO2.amplitude,...
                BOLD_Model(m1Nsteps,f1Val).CBF.amplitude);        
        else
            title_str = sprintf('m1 = [%3.1f-%3.3f] @f1 = [%3.2f-%3.2f]',...
                BOLD_Model(1,f1Val).CMRO2.amplitude, ....
                BOLD_Model(m1Nsteps,f1Val).CMRO2.amplitude,...
                BOLD_Model(1,f1Val).CBF.amplitude, ...
                BOLD_Model(m1Nsteps,f1Val).CBF.amplitude);
        end
        title(title_str);     
        n_text_str = sprintf('n = [%3.1f, %3.1f, %3.1f,...%3.1f]', ...
        	BOLD_Model(1, f1Val).n.Prescribed, ...
            BOLD_Model(2, f1Val).n.Prescribed, ...
        	BOLD_Model(3, f1Val).n.Prescribed, ...
            BOLD_Model(m1Nsteps,f1Val).n.Prescribed);
        text(50, 0.97,n_text_str)
        axis([0 totaldur 0.965 1.035])
        ylabel('\Delta S_{BOLD} / S_0')
        xlabel('time [sec]') 
        hold off
%     end %if mod(f1Val,2) == 0
end %for f1Val

% save plot to file
FNout = fullfile(BOLD_Model(1,1).SimulationPars.resultPath,...
        sprintf('VAR_m1_VAR_f1_%s_ResponseSummary.jpg',...
                    BOLD_Model(1,1).SimulationPars.label));
saveas(gcf,FNout);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot curves with maximum f1 with noise
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   

for f1Val = 1:f1NSteps
figure(fignr+1);
plotNr = 0;    
    for m1Val=1:m1Nsteps
%         if mod(m1Val,2) == 0
            plotNr = plotNr + 1;
            subplot(4,4,plotNr)    
            plot(BOLD_Model(m1Val,f1Val).BOLD.boutNoise,'b','LineWidth',1)
            hold on
            plot(BOLD_Model(m1Val,f1Val).BOLD.bout,'r','LineWidth',2)
            title_str = sprintf('m1 = %3.3f, n = %3.1f',...
                    BOLD_Model(m1Val,f1Val).CMRO2.amplitude, ....
                    BOLD_Model(m1Val,f1Val).n.Prescribed);                   
            title(title_str);     
            n_text_str = sprintf(' f1 = %3.2f, SNR_0 = %3.0f',...
                                BOLD_Model(m1Val,f1Val).CBF.amplitude,...
                                BOLD_Model(1,1).SimulationPars.SNR0); 
            text(50, 0.97,n_text_str)
            axis([0 totaldur 0.965 1.035])
            ylabel('\Delta S_{BOLD} / S_0')
            xlabel('time [sec]') 
            hold off
%         end %if mod(m1Val,2) == 0
    end %for m1Val

    % save plot to file
    FNout = fullfile(BOLD_Model(1,1).SimulationPars.resultPath,...
            sprintf('VAR_m1_f1-%3.2f_%s_ResponseSummaryNoise.jpg',...
                        BOLD_Model(m1Val,f1Val).CBF.amplitude,...
                        BOLD_Model(1,1).SimulationPars.label));
    saveas(gcf,FNout);
end

end

