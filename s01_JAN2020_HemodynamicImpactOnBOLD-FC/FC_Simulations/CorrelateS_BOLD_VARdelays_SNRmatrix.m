function CC_VARdelays_SNRmatrix = ...
                            CorrelateS_BOLD_VARdelays_SNRmatrix(S_BOLD, SNR0)
% CP 28MAR2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Correlate noisy time courses
% SNR of time courses with mean signal '1'; 
% standard deviation corresponds to sigma = 1/SNR0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% convenience definitions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% number of prescribed CMRO2 values
m1TauNsteps = S_BOLD.VARdelays(1,1).SimulationPars.m1TauNsteps; 
% number of prescribed CBF values
f1TauNsteps = S_BOLD.VARdelays(1,1).SimulationPars.f1TauNsteps;
% SNR matrix size --> sqrt(number of different random noise realizations)
% --> create SmS x SmS patches in CC maps with same SNR but different noise
%     realizations
SmS = S_BOLD.VARdelays(1,1).SimulationPars.SNRmatrixSize;
% Border width between SmS x SmS patches with same SNR
% gap = S_BOLD.VARdelays(1,1).SimulationPars.SNRgapSize;
gap = S_BOLD.VARdelays(1,1).SimulationPars.SNRgapSize;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialize array for m1, f1, n values
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CC_VARdelays_SNRmatrix.m1Values(1:m1TauNsteps*(SmS+gap)+gap,...
                                            1:f1TauNsteps*(SmS+gap)+gap) = 0; 
CC_VARdelays_SNRmatrix.TmValues(1:m1TauNsteps*(SmS+gap)+gap,...
                                            1:f1TauNsteps*(SmS+gap)+gap) = 0;
CC_VARdelays_SNRmatrix.f1Values(1:m1TauNsteps*(SmS+gap)+gap,...
                                            1:f1TauNsteps*(SmS+gap)+gap) = 0; 
CC_VARdelays_SNRmatrix.TfValues(1:m1TauNsteps*(SmS+gap)+gap,...
                                            1:f1TauNsteps*(SmS+gap)+gap) = 0;
CC_VARdelays_SNRmatrix.nPrescribed(1:m1TauNsteps*(SmS+gap)+gap,...
                                            1:f1TauNsteps*(SmS+gap)+gap) = 0; 
CC_VARdelays_SNRmatrix.nActual(1:m1TauNsteps*(SmS+gap)+gap,...
                                            1:f1TauNsteps*(SmS+gap)+gap) = 0; 
                                        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define reference BOLD signal (no vascular impairments, i.e. max. m1 & f1)
% - Reference signal obtained from separate electrophysiological
%   signal, that is simulated to stem from a connected region
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CC_VARdelays_SNRmatrix.REF_SNR_signal = ...
                    AddNoiseTC(S_BOLD.reference.BOLD.rs_bout.Data, 1/SNR0);
%                         1/S_BOLD.VARdelays(1,1).SimulationPars.SNR0);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialize arrays for CCs for S_BOLD(VARm1, VARf1) with noise = 1/SNR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% with noise = 1/SNR                        
CC_VARdelays_SNRmatrix.RHO_SNR(1:m1TauNsteps*(SmS+gap),1:f1TauNsteps*(SmS+gap)) = 0; 
CC_VARdelays_SNRmatrix.PVAL_SNR(1:m1TauNsteps*(SmS+gap),1:f1TauNsteps*(SmS+gap)) = 0;

for m1Val = 1:m1TauNsteps
    for m1SmS = (gap+1):(SmS+gap)
        for f1Val = 1:f1TauNsteps
            for f1SmS = (gap+1):(SmS+gap)
                % Extract m1, f1 & n values form structure to matrices 
                % with identical arrangement as correlation matrices
                % (to avoid confusion & mistakes)       
                m1_index = (m1Val-1)*(SmS+gap)+m1SmS;
                f1_index = (f1Val-1)*(SmS+gap)+f1SmS;
                CC_VARdelays_SNRmatrix.m1Values(m1_index,f1_index) = ...
                        S_BOLD.VARdelays(m1Val,f1Val).CMRO2.amplitude; 
                CC_VARdelays_SNRmatrix.TmValues(m1_index,f1_index) = ...
                    	S_BOLD.VARdelays(m1Val,f1Val).CMRO2.tau;         
                CC_VARdelays_SNRmatrix.f1Values(m1_index,f1_index) = ...
                      	S_BOLD.VARdelays(m1Val,f1Val).CBF.amplitude;
                CC_VARdelays_SNRmatrix.TfValues(m1_index,f1_index) = ...
                      	S_BOLD.VARdelays(m1Val,f1Val).CBF.tau;                                
                CC_VARdelays_SNRmatrix.nPrescribed(m1_index,f1_index) = ...
                      	S_BOLD.VARdelays(m1Val,f1Val).n.Prescribed;  
                CC_VARdelays_SNRmatrix.nActual(m1_index,f1_index) = ...
                      	S_BOLD.VARdelays(m1Val,f1Val).n.Actual;      
                % Correlation for noisy signal(SNR0 = 250)
                signal_SNR = AddNoiseTC(...
                    S_BOLD.VARdelays(m1Val,f1Val).BOLD.rs_bout.Data, 1/SNR0);
%                 	1/S_BOLD.VARdelays(1,1).SimulationPars.SNR0);
                [CC_VARdelays_SNRmatrix.RHO_SNR(m1_index,f1_index),...
                 CC_VARdelays_SNRmatrix.PVAL_SNR(m1_index,f1_index)] = ...
                	corr(CC_VARdelays_SNRmatrix.REF_SNR_signal,...
                    signal_SNR); 
            end % for f1SmS
        end % for f1Val
    end % for m1SmS
end % for m1Val
end

