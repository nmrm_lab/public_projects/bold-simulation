function NeuronalInput = blockInput(TC)
% CP 21FEB2019
% Create block function of amplitude '1'

% Set label
NeuronalInput.label = 'boxcar (length 20 sec)';

% Set default parameters if input is missing
if nargin <1
    NeuronalInput.deltaT = 0.006; %temporal resolution %sec
    NeuronalInput.totalDur = 120; % total duration %sec
else 
    NeuronalInput.totalDur = TC.totalDur; % total duration %sec
    NeuronalInput.deltaT = TC.deltaT ; %temporal resolution [sec]
end
NeuronalInput.topDur = 20; % desired block duration % sec
NeuronalInput.baseDur = 5;   % pre block baseline duration

% Define vector of time points
NeuronalInput.time = 0:NeuronalInput.deltaT:NeuronalInput.totalDur; 

% initialize variable for boxcar function
npoints = size(NeuronalInput.time,2);


% determine start and end of block signal increase
blockStart = round(NeuronalInput.baseDur/NeuronalInput.deltaT + 1);
blockEnd = round((NeuronalInput.baseDur + NeuronalInput.topDur)/NeuronalInput.deltaT);

NeuronalInput.block(1:blockStart) = 0.0;
NeuronalInput.block(blockStart:blockEnd) = 1.0;
% NeuronalInput.block(blockEnd+1:npoints) = 0.0;
NeuronalInput.block(blockEnd+1:floor(npoints/4)) = 0.0;
NeuronalInput.block(ceil(npoints/4):npoints) = 0.3;

NeuronalInput.ts = timeseries(NeuronalInput.block,NeuronalInput.time,...
                                            'Name',NeuronalInput.label);
end