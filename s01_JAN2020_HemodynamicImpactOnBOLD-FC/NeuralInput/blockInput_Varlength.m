function NeuronalInput = blockInput_Varlength(TC,nInput)
% CP 21FEB2019
% Create exponentially decaying input function of initial amplitude '1'



% Set default parameters if input is missing
if nargin <1
    NeuronalInput.deltaT = 0.05; %temporal resolution %sec
    NeuronalInput.totalDur = 60; % total duration %sec
else 
    NeuronalInput.totalDur = TC.totalDur; % total duration %sec
    NeuronalInput.deltaT = TC.deltaT ; %temporal resolution [sec]
end
NeuronalInput.baseDur = 10;   % pre block baseline duration

if nInput == 1
    NeuronalInput.topDur = 1;    % desired block duration % sec
    % Set label
    label = 'block (length 0.2 sec)';
elseif nInput == 2
    NeuronalInput.topDur = 10;    % desired block duration % sec
    % Set label
    label = 'block (length 10 sec)';
elseif nInput == 3
    NeuronalInput.topDur = 30;    % desired block duration % sec
    % Set label
    label = 'block (length 30 sec)';
end

% Define vector of time points
time = 0:NeuronalInput.deltaT:NeuronalInput.totalDur; 

% initialize variable for boxcar function
NeuronalInput.npoints = size(time,2);

% determine start and end of block signal increase
blockStart = round(NeuronalInput.baseDur/NeuronalInput.deltaT + 1);
blockEnd = round((NeuronalInput.baseDur + NeuronalInput.topDur)/NeuronalInput.deltaT);

% block(1:blockStart) = 0.0;
block(:) = 0.0;
% signal = exp(-(blockStart:blockEnd)/50);
% signal = signal/max(signal);
signal = 1;
block(blockStart:blockEnd) = signal;
% block(blockEnd+1:npoints) = 0.0;
block(blockEnd+1:NeuronalInput.npoints) = 0.0;

NeuronalInput = timeseries(block,time,'Name',label);
% figure(100); plot(NeuronalInput)
end