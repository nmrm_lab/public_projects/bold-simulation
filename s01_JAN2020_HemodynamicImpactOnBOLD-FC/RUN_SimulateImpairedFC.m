function RUN_SimulateImpairedFC
% CP 26MAR19
% simulate functional connectivity between 'intrinsic' BOLD responses 
% calculated from simulated neuronal inputs using Balloon model 
% (Buxton et al. MRM, 1998;Buxton et al. NI, 2004; Simon & Buxton, NI 2015)
%
% CP 02JUL19
% modified to allow for a seperate neuronal input signal that serves as a
% reference (should be simulated to electrophysiologically connected to the
% test region with electrophysiological impairment). 
% - NeuronalInput '1' will be used as reference.
% - Reference CBF (f1) und CMRO2 (m1) response  
%
save_path = ...
	'D:\SimulationsAPR2020\Results';
% load_path -> Path where the neural input N(t) is stored.
load_path = ...
    'D:\SimulationsAPR2020\NeuralImput';
addpath(genpath(load_path));
check_save_path(save_path)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set global parameters for simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--------------------------------------------------------------------------
% Set parameters governing BOLD response 
%--------------------------------------------------------------------------
% modeltype - 1: [default]
%                BOLD signal change (in %) according to Balloon model 
%                (Buxton et al. MRM, 1998); updated in (Buxton et al.,2004) 
%                according to (Obata et al., NI, 2004) with coefficients
%                epsilon & r0 according to Table 1 of (Blockley et al. 2009)
%                ---> contains intravascular CBV effects
%           - 2: BOLD signal change (in %) according to Simon & Buxton 
%                (NI, 2015); does not contain direct CBV effects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
BOLD_Model.SimulationPars.modeltype = '1';
% temporal resolution of neuronal input [sec]
% must be derived from number of point and length (typical 120 oder 240 sec)
BOLD_Model.SimulationPars.deltaT = 0.02939794; 
% temporal resolution of simulated BOLD signal curves [sec]
% needs to be entered in Simulink Model Output blocks !!!!
BOLD_Model.SimulationPars.SampleTime = 1.0; 
% simulated time period
BOLD_Model.SimulationPars.totalDur = 400; 
% total duration of initial block stimulus
BOLD_Model.SimulationPars.blockDur = 150;
% SNR of time courses with mean signal '1'; 
% standard deviation of SNR0_300, sigma = 1/SNR0
BOLD_Model.SimulationPars.SNR0 = 250;
% SNR matrix size --> SmS = sqrt(# of different random noise realizations)
% --> create SmS x SmS patches in resulting CC maps with same SNR but 
%     different noise realizations, i.e. different random numbers.
BOLD_Model.SimulationPars.SNRmatrixSize = 16;
% Border width between SmS x SmS patches with same SNR
BOLD_Model.SimulationPars.SNRgapSize = 2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define input neuronal activity (task or intrinsic)
% -> Two neuronal input signals allow to simulate BOLD signals from a 
% (electrophysiologically connected but) intact reference region and 
% a test region with a range of simulated vascular impairments
% CAVE: filenames & file contents need to posses equal length !!!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
BOLD_Model.SimulationPars.NeuronalInputFiles(1,:) = ...
    fullfile(load_path,'simuNeuralData_0.05_amplow_amphigh_23-Sep-2019.mat');
BOLD_Model.SimulationPars.NeuronalInputFiles(2,:) = ...
    fullfile(load_path,'simuNeuralData_0.05_amplow_amphigh_23-Sep-2019.mat');
BOLD_Model.NeuronalInput = generateNeuronalInput(BOLD_Model.SimulationPars); 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set Defaults for Balloon Model (Buxton et al. 2004; Simon&Buxton, 2015)
% --> Model parameters govern the BOLD response via time constants 
% for hemodynamic (CBF/CBF0) -> fin(t)) and metabolic (CMRO2/CMRO20 -> m(t)) 
% response functions (see Table 1 in (Simon & Buxton, 2015)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for nRepetitions = 1:2
    if nRepetitions == 1
        % Use default values for ballon model assuming tight coupling of CBF,  
        % CMRO2, and CBVv dynamics (CMRO2.tau=2s, CBF.tau=2s, CBV.tau=0 s).
        BOLD_Model.SimulationPars.label = 'TightCoupling';
        BOLD_Model = setDefaultValues(BOLD_Model);
    elseif  nRepetitions == 2
        % Use default values for ballon model assuming slower CBV response
        BOLD_Model.SimulationPars.label = 'SlowerCBV';          
        BOLD_Model = setDefaultValues(BOLD_Model);
        % transit time for blood through venous compartment
        BOLD_Model.CBV.tau0 = 2.0; % 2sec [0.75 sec - 2.5 sec]
        % characteristic time constant for CBF [variable]                     
        BOLD_Model.CBV.tau = 20.0; 
    end % if nRepetitions

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% 12.07.2019, Christine Preibisch
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Matlab functions to perform simulations of the impact of impaired  
    % physiology on functional connectivity (FC) via simulation of BOLD signal
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % 1) Influence of variable CMRO2 (m1) and CBF (f1) amplitudes on FC is 
    %    simulated by
    %         - SimulateImpairedFC_VARm1_VARf1(BOLD_Model)
    %    By default, simulations can be performed for 'ThightCoupling' or 
    %    'SlowerCBV'.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    BOLD_Model.SimulationPars.resultPath = ...
        fullfile(save_path,'VARm1_VARf1',BOLD_Model.SimulationPars.label); 
    SimulateImpairedFC_VARm1_VARf1(BOLD_Model)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % 2) Influence of delayed CBF (CBF.tau) or CMRO2 (CMRO2.tau) response  
    %    is simulated by
    %         - SimulateImpairedFC_VARdelays(BOLD_Model)
    %    By default, simulations can be performed for 'ThightCoupling' or 
    %    'SlowerCBV'.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    BOLD_Model.SimulationPars.resultPath = ...
        fullfile(save_path,'VARdelays',BOLD_Model.SimulationPars.label);
    SimulateImpairedFC_VARdelays(BOLD_Model)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % 3) Influence of veriable CBV response (CBV.tau, CBV.alpha) is 
    %    simulated by
    %         - SimulateImpairedFC_VARcbv(BOLD_Model)
    %    By default, simulations can be performed for a reference signal  
    %    with 'ThightCoupling' or 'SlowerCBV'.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    BOLD_Model.SimulationPars.resultPath = ...
        fullfile(save_path,'VARcbv',BOLD_Model.SimulationPars.label);
    SimulateImpairedFC_VARcbv(BOLD_Model)
end % for nRepetitions
end

