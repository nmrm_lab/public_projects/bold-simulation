function NeuronalInput = generateNeuronalInput_VARlength(SimulationPars)
% CP11FEB20
% Define neuronal input 

% --> generate structure on the basis of a block input

      
for nInput = 1:SimulationPars.nInputs 
    
    NeuronalInput.ts(nInput,:) = blockInput_Varlength(SimulationPars,nInput);

end % for nInput   

end

