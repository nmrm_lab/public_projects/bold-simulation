# CP 07FEB2020 (inventory & clean up)
#
# Folder 's01_JAN2020_HemodanymicImpactOnBOLD-FC' contains code 
# used to simulated BOLD-TCs and BOLD-FCas well as for generating Figures
#
#	'Modelling the impact of neurovascular coupling impairments on   
# 		BOLD-based functional connectivity at rest'                     
# 	Mario E. Archila-Mel�ndez, Christian Sorg, Christine Preibisch; 
# 		submitted to NeuroImage (January 2020)                          
#
##################################################################################
# The code resides in the following subfolders 
#   
# 	- DynamicBOLDModel_SB2015: contains the core code (implement in simulink)
#                                  to perform simulations of BOLD-TCs based on
#                                  - 'neuronal input curves'
#                                  - allows for independent CMRO2 & CBF response
#
#	- FC_simulations: contains code to perform BOLD-FC calculations 
#
#	- NeuralInputs: contains prescribed 'neuronal' input functions    
#
#	- GenerateFigures: contains code to BOLD-TC and BOLD-FC plots 
#
#	- Colormap: contains additional custom colormaps from different sources
#
#	- PlotResults_TEMPLAT: contains an empty data structure that is expected 
#                              by the simulation program
#       
##################################################################################
